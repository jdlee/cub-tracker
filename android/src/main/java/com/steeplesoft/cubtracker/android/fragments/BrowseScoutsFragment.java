package com.steeplesoft.cubtracker.android.fragments;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.activities.ScoutFormActivity;
import com.steeplesoft.cubtracker.android.activities.ViewScoutActivity;
import com.steeplesoft.cubtracker.android.adapter.ExpandableListAdapter;
import com.steeplesoft.cubtracker.android.adapter.util.ExpandListChild;
import com.steeplesoft.cubtracker.android.adapter.util.ExpandListGroup;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Scout;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BrowseScoutsFragment extends ExpandableListFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LayoutInflater li = LayoutInflater.from(getActivity());
        return li.inflate(R.layout.list_expandable, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        ExpandableListView expandableList = (ExpandableListView) getView().findViewById(android.R.id.list);
        expandableList.setTag("meetings");
        updateList();

        expandableList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View view, int groupPosition, int childPosition,
                                        long id) {
                TextView tv = (TextView) view.findViewById(R.id.tvChild);
                Scout scout = (Scout) tv.getTag();
                Intent intent = new Intent(view.getContext(), ViewScoutActivity.class);
                intent.putExtra(IntentConstants.SCOUT_ID, scout.getId());
                startActivity(intent);
                return false;
            }
        });
        expandableList.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
                if (ExpandableListView.getPackedPositionType(((ExpandableListView.ExpandableListContextMenuInfo) menuInfo).packedPosition) ==
                        ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                    menu.setHeaderTitle("Scout Management");
                    menu.add(0, Constants.CONTEXTMENU_EDIT_SCOUT, 1, "Edit Scout");
                    menu.add(0, Constants.CONTEXTMENU_DELETE_SCOUT, 1, "Delete Scout");
                }
            }
        });
    }

    @Override
    public void onResume() {
        updateList();
        super.onResume();
    }

    public void updateList() {
        setListAdapter(new ExpandableListAdapter(getActivity(), getGroupList()));
    }

    protected List<ExpandListGroup> getGroupList() {
        List<ExpandListGroup> list = new ArrayList<ExpandListGroup>();
        Cursor cursor = DatabaseAccess.getInstance().getCursorForRegisteredRanks();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            long id = cursor.getLong(0);
            String name = cursor.getString(1);
            list.add(new ExpandListGroupRanks(id, name));
            cursor.moveToNext();
        }
        cursor.close();

        return list;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) item.getMenuInfo();
        int groupPos = 0, childPos = 0;
        int type = ExpandableListView.getPackedPositionType(info.packedPosition);
        if (type == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
            groupPos = ExpandableListView.getPackedPositionGroup(info.packedPosition);
            childPos = ExpandableListView.getPackedPositionChild(info.packedPosition);
        }

//        final ExpandListGroupRanks group = (ExpandListGroupRanks) expandableAdapter.getGroup(groupPos);
        final ExpandListChildScouts child = (ExpandListChildScouts) mAdapter.getChild(groupPos, childPos);
        final Scout scout = (Scout) child.getTag();

        switch (item.getItemId()) {
            case Constants.CONTEXTMENU_EDIT_SCOUT:
                Intent intent = new Intent(getActivity(), ScoutFormActivity.class);
                intent.putExtra(IntentConstants.SCOUT_ID, scout.getId());
                startActivityForResult(intent, Constants.ACTIVITY_EDIT_SCOUT);
                break;
            case Constants.CONTEXTMENU_DELETE_SCOUT:
                Util.showConfirmationDialog(getActivity(), "Are you sure you want to delete " + scout.getName(),
                        new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == DialogInterface.BUTTON_POSITIVE) {
                                    try {
                                        DatabaseAccess.getInstance().deleteScout(scout.getId());
                                        updateList();
                                    } catch (SQLException e) {
                                        Util.showAlertDialog(getActivity(), "Error!", "Unable to delete scout.");
                                    }
                                }
                            }
                        });

                break;
        }
        return true;
    }

    private static class ExpandListGroupRanks implements ExpandListGroup {
        protected long id;
        protected String name;
        protected List<ExpandListChild> items;

        public ExpandListGroupRanks(long id, String name) {
            super();
            this.id = id;
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public void setName(String name) {
        }

        @Override
        public List<ExpandListChild> getItems() {
            if (items == null) {
                items = new ArrayList<ExpandListChild>();
                DatabaseAccess db = DatabaseAccess.getInstance();
                Cursor cursor = db.getCursorForAllScoutsInRank(id);
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        Scout scout = db.getScout(cursor.getLong(0));
                        if (scout != null) {
                            items.add(new ExpandListChildScouts(scout));
                        }
                        cursor.moveToNext();
                    }
                }
                cursor.close();
            }
            return items;
        }

        @Override
        public void setItems(List<ExpandListChild> items) {
            this.items = items;
        }
    }

    private static class ExpandListChildScouts implements ExpandListChild {
        private Scout scout;

        public ExpandListChildScouts(Scout scout) {
            super();
            this.scout = scout;
        }

        @Override
        public long getId() {
            return scout.getId();
        }

        @Override
        public String getName() {
            return scout.getName();
        }

        @Override
        public void setName(String Name) {

        }

        @Override
        public Object getTag() {
            return scout;
        }

        @Override
        public void setTag(Object Tag) {
        }
    }
}