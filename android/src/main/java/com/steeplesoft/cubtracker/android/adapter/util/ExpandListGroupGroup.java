package com.steeplesoft.cubtracker.android.adapter.util;

import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Group;
import com.steeplesoft.cubtracker.android.model.Part;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ExpandListGroupGroup implements ExpandListGroup {
    protected List<ExpandListChild> items;
    protected Group group;

    public ExpandListGroupGroup(Group group) {
        this.group = group;
    }

    @Override
    public String getName() {
        String name = group.getName();
        String notes = group.getNotes();
        int qtyReq = group.getQuantityRequired();

        return ((name != null) ? name + " - " : "") + ((notes != null) ? notes : "")
                + ((qtyReq > 0) ? "Do " + qtyReq + " of the following" : "Do all of the following");
    }

    @Override
    public void setName(String name) {
    }

    @Override
    public List<ExpandListChild> getItems() {
        if (items == null) {
            try {
                items = new ArrayList<ExpandListChild>();
                final DatabaseAccess db = DatabaseAccess.getInstance();
                for (Part part :  db.getPartsForGroup(group.getId())) {
                    items.add(new ExpandListChildPart(part));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return items;
    }

    @Override
    public void setItems(List<ExpandListChild> items) {
        this.items = items;
    }

}
