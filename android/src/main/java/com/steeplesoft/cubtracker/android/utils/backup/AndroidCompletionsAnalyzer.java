package com.steeplesoft.cubtracker.android.utils.backup;

import android.database.Cursor;

import com.steeplesoft.cubtracker.CompletionsAnalyzer;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.CompletionCheck;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jdlee on 8/4/14.
 */
public class AndroidCompletionsAnalyzer extends CompletionsAnalyzer {
    protected DatabaseAccess da = DatabaseAccess.getInstance();

    @Override
    protected List<CompletionCheck> getCompletionCheck(long[] scoutIds) {
        StringBuilder scouts = arrayToString(scoutIds);

        String sql = getItemCompletionQuery(scouts.toString());
        List<CompletionCheck> list = new ArrayList<CompletionCheck>();
        try {
            Cursor c = da.executeSelectStatement(sql);
            int scoutIdx = c.getColumnIndex("scoutId");
            int itemIdx = c.getColumnIndex("itemId");
            int groupIdx = c.getColumnIndex("groupId");
            int completedIdx = c.getColumnIndex("partsCompleted");
            int requiredIdx = c.getColumnIndex("qtyRequired");
            int countIdx = c.getColumnIndex("partCount");
            while (!c.isAfterLast()) {
                CompletionCheck cc = new CompletionCheck();
                cc.setId(1);
                cc.setScoutId(c.getInt(scoutIdx));
                cc.setGroupId(c.getInt(groupIdx));
                cc.setItemId(c.getInt(itemIdx));
                cc.setPartCount(c.getInt(countIdx));
                cc.setPartsCompleted(c.getInt(completedIdx));
                cc.setQtyRequired(c.getInt(requiredIdx));

                list.add(cc);
                c.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    protected void persistItemCompletion(Long scoutId, Long itemId) {
        da.createItemCompletion(itemId, scoutId, new Date());
    }
}
