package com.steeplesoft.cubtracker.android.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.ReportHolder;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Scout;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ViewScoutActivity extends Activity {
    private Scout scout;
    private Long scoutId;
    private List<String> contactIds = new ArrayList<String>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(Constants.LOG_TAG, "Creating activity: " + getClass().getSimpleName());

        setContentView(R.layout.view_scout);
        scoutId = (savedInstanceState == null) ? getIntent().getLongExtra(IntentConstants.SCOUT_ID, -1) :
                savedInstanceState.getLong(IntentConstants.SCOUT_ID);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadScout();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(IntentConstants.SCOUT_ID, scoutId);
    }

    protected void loadScout() {
        scout = DatabaseAccess.getInstance().getScout(scoutId);
        if (scout != null) {
            ((TextView) findViewById(R.id.scoutName)).setText(scout.getName());
            ((TextView) findViewById(R.id.scoutDen)).setText(scout.getDen().getName());
            ((TextView) findViewById(R.id.scoutEmail)).setText(scout.getEmailAddress());
            ((TextView) findViewById(R.id.scoutPhone)).setText(scout.getPhoneNumber());
            ListView listView = (ListView) findViewById(android.R.id.list);
            final String contacts = scout.getContactIds();
            if (contacts != null) {
                contactIds = Arrays.asList(contacts.split(","));
                findViewById(R.id.contactsRow).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.contactsRow).setVisibility(View.GONE);
//                ((TextView) findViewById(R.id.labelScoutContacts)).setVisibility(View.GONE);
            }
            String[] contactNames = Util.getContactNames(contacts);
            Util.handleEmptyList(this, contactNames.length == 0);

            listView.setAdapter(new ArrayAdapter<String>(this, R.layout.plain_list_item, contactNames));
            listView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    final Map<String, String> contact = Util.getContact(Long.parseLong(contactIds.get(position)));
                    Uri uri = Uri.withAppendedPath(Contacts.CONTENT_URI,
                            contact.get(ContactsContract.CommonDataKinds.Identity._ID));
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }

            });

            showImage();
            /*
            List<String> completions = new ArrayList<String>();
            for (ItemCompletion completion : DatabaseAccess.getInstance().getItemCompletionsForScout(scoutId)) {
                completions.add(completion.getItem().getDescription() + "(" + Util.formatDate(completion.getTimestamp()) + ")");
            }
            completions.addAll(DatabaseAccess.getInstance().getPartCompletionsForScout(scoutId));

            TextView et = (TextView) findViewById(R.id.editText);
            StringBuilder sb = new StringBuilder();
            for (String c: completions) {
                sb.append(c).append("\n");
                sb.append(c).append("\n");
                sb.append(c).append("\n");
                sb.append(c).append("\n");
            }
            et.setText(sb.toString());
            */

            /*
            ListView completionLV = (ListView) findViewById(R.id.completedList);
            completionLV.setAdapter(new ArrayAdapter<String>(this, R.layout.list_item, completions));            ;
            completionLV.setEmptyView(findViewById(R.id.completedListEmpty));
            */

            ReportHolder holder = new ReportHolder();
            holder.setSelectedScouts(new long[] { scoutId });
            holder.setReportAll(true);
            holder.setShowRequiredAdventures(true);
            holder.setShowElectiveAdventures(true);
//            holder.setShowPinsLoops(true);
            holder.setShowWebelosBadges(true);

            if (!holder.getReport().isEmpty()) {
                findViewById(R.id.labelScoutCompleted).setVisibility(View.VISIBLE);
                Util.setText(findViewById(R.id.editText), holder.getReport().trim());
            } else {
                findViewById(R.id.labelScoutCompleted).setVisibility(View.GONE);
            }
//            DatabaseAccess.getInstance().getI
        }
    }

    @SuppressWarnings("deprecation")
    protected void showImage() {
        byte[] photo = DatabaseAccess.getInstance().getScoutPhoto(scoutId);
        if (photo == null) {
            findViewById(R.id.photoRow).setVisibility(View.GONE);
        } else {
            final Bitmap bitmap = BitmapFactory.decodeByteArray(photo, 0, photo.length);
            if (bitmap != null) {

                final ImageView imageView = (ImageView) findViewById(R.id.scoutPhoto);
                View parent = (View) imageView.getParent();
                View grandParent = (View) parent.getParent();
                View label = findViewById(R.id.labelScoutPhoto);
                Display display = getWindowManager().getDefaultDisplay();
                parent.measure(display.getWidth(), display.getHeight());
                grandParent.measure(display.getWidth(), display.getHeight());

                int imageWidth = bitmap.getWidth();
                int imageHeight = bitmap.getHeight();

                int maxWidth = grandParent.getMeasuredWidth() - label.getMeasuredWidth() - 5;
                int maxHeight = grandParent.getMeasuredHeight() - imageView.getTop() - 5;

                float xScale = ((float) maxWidth) / imageWidth;
                float yScale = ((float) maxHeight) / imageHeight;
                float scale = (display.getWidth() <= display.getHeight()) ? xScale : yScale;

                Util.scaleImage(bitmap, imageView, scale);
                findViewById(R.id.photoRow).setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.view_scout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final Context context = this;
        switch (item.getItemId()) {
            case R.id.menu_edit_scout:
                Intent intent = new Intent(this, ScoutFormActivity.class);
                intent.putExtra(IntentConstants.SCOUT_ID, scoutId);
                startActivityForResult(intent, Constants.ACTIVITY_EDIT_SCOUT);
//                Intent intent = new Intent(this, ScoutFormActivity.class);
//                startActivityForResult(intent, Constants.ACTIVITY_ADD_SCOUT);
                break;
            case R.id.menu_delete_scout:
                Util.showConfirmationDialog(this, "Are you sure you want to delete this scout?",
                    new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == DialogInterface.BUTTON_POSITIVE) {
                                try {
                                    DatabaseAccess.getInstance().deleteScout(scoutId);
                                    finish();
                                } catch (SQLException e) {
                                    Util.showAlertDialog(context, "Error!", "Unable to delete scout.");
                                }
                            }
                        }
                    });

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.ACTIVITY_EDIT_SCOUT) {
            if (resultCode == RESULT_OK) {
                scoutId = data.getLongExtra("scoutId", -1);
                loadScout();
            }
        }
    }

}
