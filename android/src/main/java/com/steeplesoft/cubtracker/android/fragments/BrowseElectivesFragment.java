package com.steeplesoft.cubtracker.android.fragments;

import android.os.Bundle;

public class BrowseElectivesFragment extends BrowseAchievementsFragment {
    public BrowseElectivesFragment() {
        super();
        itemType = "E";
        beginningRank = 1;
        endingRank = 3;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        mList.setTag("electives");
    }
}