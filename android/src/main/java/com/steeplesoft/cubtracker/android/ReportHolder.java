package com.steeplesoft.cubtracker.android;

import android.database.Cursor;
import android.util.Log;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by jdlee on 9/20/13.
 */
public class ReportHolder {
    private String report;
    private StringBuilder attachment;
    private Boolean showRemaining = Boolean.FALSE;
    private Boolean showDescription = Boolean.FALSE;
    private Boolean reportAll = Boolean.TRUE;
    private long[] selectedScouts = new long[]{};
    private Boolean showRequiredAdventures = Boolean.TRUE;
    private Boolean showElectiveAdventures = Boolean.TRUE;
    private Boolean showWebelosBadges = Boolean.FALSE;
    private Date startDate;
    private Date endDate;

    public String getReport() {
        if (report == null) {
            generateReports();
        }
        return report;
    }

    public String getAttachment() {
        if (attachment == null) {
            generateReports();
        }
        return attachment.toString();
    }

    public void markRecordsReported() {
        String query = "UPDATE completions SET reported = date('now')  where _id in (select completions._id ";
        StringBuilder from = new StringBuilder("from groups, parts, items, scouts, ranks, dens");
        String where = buildWhere(from);
        query = query + " " + from.toString() + " " + where + ")";
        DatabaseAccess.getInstance().execSql(query);
    }

    private void generateReports() {
        StringBuilder sb = new StringBuilder();
        attachment = new StringBuilder("name|rank|title|type|item number|part number|completion date|description\n");
        String colSep = "|";
        String query = "";
        if (showRemaining) {
            query = Util.readFileAsset(R.raw.items_remaining);
        } else {
            String completed = Util.readFileAsset(R.raw.items_completed);
            completed = completed.substring(0, completed.indexOf("ORDER BY")-1);
            query = completed +
                    "\nUNION\n" +
                    Util.readFileAsset(R.raw.parts_completed);
        };
        Cursor cursor = buildCursor(query);
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            int colName = cursor.getColumnIndex("name");
            int colRank = cursor.getColumnIndex("rank");
            int colTitle = cursor.getColumnIndex("title");
            int colType = cursor.getColumnIndex("type");
            int colInum = cursor.getColumnIndex("inum");
            int colPnum = cursor.getColumnIndex("pnum");
            int colCompleted = cursor.getColumnIndex("completed");
            String prevName = "";

            while (!cursor.isAfterLast()) {
                String name = cursor.getString(colName);
                String rank = cursor.getString(colRank);
                if (!prevName.equals(name)) {
                    sb.append("\n").append(name).append(" (")
                            .append(rank)
                            .append(")\n");
                }

                sb.append("    ");
                final String type = cursor.getString(colType);
                String typeName = Util.getItemType(type);
                String inum = cursor.getString(colInum);
                String title = cursor.getString(colTitle);
                attachment.append(name)
                        .append(colSep).append(rank)
                        .append(colSep).append(title)
                        .append(colSep).append(typeName)
                        .append(colSep).append(inum != null ? inum : "");
                String pnum = null;
                if (!showRemaining) {
                    pnum = cursor.getString(colPnum);
                    attachment.append(colSep).append(pnum);
                }

                if (type.equals(Constants.TYPE_BADGE)) {
                    sb.append(title);
//                            .append(" part #")
//                            .append(pnum);
                } else {
                    final boolean loopOrPin = Util.isBeltLoop(type) || Util.isPin(type);
                    sb.append(loopOrPin ? title : rank).append(" ").append(typeName)
                            .append(" ")
                            .append(loopOrPin ? "" : inum);
                    if ((pnum != null) && (pnum.length() > 0)) {
                        sb.append(loopOrPin ? "part " : "-")
                                .append(pnum);
                    }
                    if (!loopOrPin) {
                        sb.append(" (")
                                .append(title)
                                .append(") ");
                    }
                }

                if (!showRemaining) {
                    String completed = Util.getDateString(cursor.getString(colCompleted));
                    attachment.append(colSep).append(completed);
                    sb.append("on ").append(completed);
                }
                sb.append("\n");
                attachment.append("\n");
                prevName = name;
                cursor.moveToNext();
            }

            cursor.close();
        }
        this.report = sb.toString();
    }

    private Cursor buildCursor(String query) {
        if (selectedScouts.length > 0) {
            query = query.replaceAll("--1.*1--",
                    "AND scouts._id in (" + implode(selectedScouts, ",") + ")");
        }

        if (showRemaining) {
            StringBuilder typeString = new StringBuilder();
            String sep = "";
            if (showRequiredAdventures) {
                typeString.append("'").append(Constants.TYPE_ADVENTURE_REQ).append("'");
                sep = ",";
            }
            if (showElectiveAdventures) {
                typeString.append(sep)
                        .append("'")
                        .append(Constants.TYPE_ADVENTURE_ELECTIVE)
                        .append("'");
                sep = ",";
            }
            if (showWebelosBadges) {
                typeString.append(sep)
                        .append("'")
                        .append(Constants.TYPE_BADGE)
                        .append("'");
                sep = ",";
            }
            if (typeString.length() > 0) {
                query = query.replaceAll("--4.*4--",
                        " AND items.type in (" + typeString.toString() + ")");
            }
        } else {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            if (startDate != null) {
                query = query.replaceAll("--2.*2--",
                        "AND item_completions.completed >= '" + sdf.format(startDate) + " 00:00:00'");
            }
            if (endDate != null) {
                query = query.replaceAll("--3.*3--",
                        "AND item_completions.completed <= '" + sdf.format(endDate) + " 23:59:59'");
            }
            if (reportAll) {
                query = query.replace("AND item_completions.reported IS NULL", "");
            }
        }

        return DatabaseAccess.getInstance().executeSelectStatement(query);
    }

    private String buildWhere(StringBuilder from) {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        StringBuilder where = new StringBuilder("WHERE scouts.den_id = dens._id " +
                " AND (items.rank_id = -1 or items.rank_id = dens.rank_id) AND dens.rank_id = ranks._id ");

        if (selectedScouts.length > 0) {
            where.append(" AND scouts._id in (")
                    .append(implode(selectedScouts, ","))
                    .append(") ");
        }

        if (!showRemaining) {
            from.append(", item_completions");
            where.append("AND item_completions.scout_id = scouts._id AND item_completions.item_id = items._id");
            if ((startDate != null) || (endDate != null)) {
                if (startDate != null) {
                    where.append(" AND item_completions.completed >= '")
                            .append( sdf.format(startDate))
                            .append(" 00:00:00'");
                }
                if (endDate != null) {
                    where.append(" AND item_completions.completed <= '")
                            .append( sdf.format(endDate))
                            .append(" 23:59:59'");
                }
            }
        } else {
            where.append(" AND items._id not in (select item_id from item_completions where scout_id = scouts._id)");
        }

        if (showRemaining) {
            StringBuilder typeString = new StringBuilder();
            String sep = "";
            if (showRequiredAdventures) {
                typeString.append("'").append(Constants.TYPE_ADVENTURE_REQ).append("'");
                sep = ",";
            }
            if (showElectiveAdventures) {
                typeString.append(sep)
                        .append("'")
                        .append(Constants.TYPE_ADVENTURE_ELECTIVE)
                        .append("'");
                sep = ",";
            }
            if (showWebelosBadges) {
                typeString.append(sep)
                        .append("'")
                        .append(Constants.TYPE_BADGE)
                        .append("'");
                sep = ",";
            }
            if (!showRemaining) {
                where.append(" AND item_completions.reported is null");
            }
            if (typeString.length() > 0) {
                where.append(" AND items.type in (")
                        .append(typeString.toString())
                        .append(")");
            }
        }
        return where.toString();
    }

    // TODO: Move this to Util
    protected String implode(long[] array, String delim) {
        StringBuilder sb = new StringBuilder();
        String sep = "";
        for (long item : array) {
            String text = "" + item;
            text = text.trim();
            if (!text.isEmpty()) {
                sb.append(sep).append(item);
                sep = delim;
            }
        }

        return sb.toString();
    }


    public Boolean getShowRemaining() {
        return showRemaining;
    }

    public void setShowRemaining(Boolean showRemaining) {
        this.showRemaining = showRemaining;
    }

    public Boolean getShowDescription() {
        return showDescription;
    }

    public void setShowDescription(Boolean showDescription) {
        this.showDescription = showDescription;
    }

    public long[] getSelectedScouts() {
        return selectedScouts;
    }

    public void setSelectedScouts(long[] selectedScouts) {
        this.selectedScouts = selectedScouts;
    }

    public Boolean getReportAll() {
        return reportAll;
    }

    public void setReportAll(Boolean reportAll) {
        this.reportAll = reportAll;
    }

    public Boolean getShowRequiredAdventures() {
        return showRequiredAdventures;
    }

    public void setShowRequiredAdventures(Boolean showAdventures) {
        this.showRequiredAdventures = showAdventures;
    }

    public Boolean getShowElectiveAdventures() {
        return showElectiveAdventures;
    }

    public void setShowElectiveAdventures(Boolean showElectiveAdventures) {
        this.showElectiveAdventures = showElectiveAdventures;
    }

    public Boolean getShowWebelosBadges() {
        return showWebelosBadges;
    }

    public void setShowWebelosBadges(Boolean showWebelosBadges) {
        this.showWebelosBadges = showWebelosBadges;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
