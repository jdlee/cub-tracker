package com.steeplesoft.cubtracker.android;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.steeplesoft.cubtracker.android.db.DatabaseAccess;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DatabaseLoadThread extends Thread {
    DatabaseAccess databaseAccess;
    private Handler progressBarHandler;
    private ProgressDialog progressBar;
    private int progressBarStatus;
    private Context context;

    public DatabaseLoadThread(Context context, ProgressDialog progressBar, Handler progressBarHandler) {
        this.context = context;
        this.progressBar = progressBar;
        this.progressBarHandler = progressBarHandler;
        databaseAccess = DatabaseAccess.getInstance();
    }

    public void run() {
        // This is not currently used, but I don't want to get rid of it yet. :)
        /*
        try {
            JSONArray items = loadArray(R.raw.items);
            DatabaseAccess db = DatabaseAccess.getInstance();

            setMessageAndMax("Installing items data", items.length());

            for (int i = 0; i < items.length(); i++) {
                JSONObject o = (JSONObject) items.get(i);
                Rank rank = null;
                if (!o.isNull("rank")) {
                    rank = new Rank();
                    rank.setId(o.getLong("rank"));
                }
                db.createItem(o.getLong("_id"),
                        getString("type", o),
                        rank,
                        getString("number", o),
                        getString("title", o),
                        getString("notes", o),
                        null);
                update();
            }
            items = null;

            JSONArray groups = loadArray(R.raw.groups);
            setMessageAndMax("Installing group data", groups.length());
            for (int i = 0; i < groups.length(); i++) {
                final JSONObject o = (JSONObject) groups.get(i);
                Item item = new Item();
                item.setId(o.getLong("item"));

                db.createGroup(o.getInt("_id"),
                        item,
                        getString("name", o),
                        getString("number", o),
                        o.getInt("qtyReq"),
                        getString("notes", o));
                update();
            }
            groups = null;
            JSONArray parts = loadArray(R.raw.parts);
            setMessageAndMax("Installing parts data", parts.length());
            for (int i = 0; i < parts.length(); i++) {
                final JSONObject o = (JSONObject) parts.get(i);

                Group group = new Group();
                group.setId(o.getLong("group_id"));

                db.createPart(o.getInt("_id"),
                        group,
                        getString("number", o),
                        getString("description", o),
                        getString("notes", o));
                update();
            }
            parts = null;
            databaseAccess.markDatabaseUpgraded();
        } catch (Exception e) {
            Log.e(Constants.LOG_TAG, e.getLocalizedMessage());
        }

        // Update the progress bar
        update();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        progressBar.dismiss();
        */
    }

    protected String getString(String key, JSONObject o) {
        try {
            return o.getString(key);
        } catch (Exception e) {
            return null;
        }
    }

    protected void setMessageAndMax(final String message, final int max) {
        progressBarHandler.post(new Runnable() {
            public void run() {
                progressBar.setMessage(message);
                progressBar.setMax(max);
                progressBar.setProgress(0);
                progressBarStatus = 0;
            }
        });
    }

    protected void update() {
        progressBarHandler.post(new Runnable() {
            public void run() {
                progressBar.setProgress(++progressBarStatus);
            }
        });

    }

    protected JSONArray loadArray(int id) {
        try {
            return new JSONArray(readFile(id));
        } catch (JSONException e) {
            Log.e(Constants.LOG_TAG, e.getLocalizedMessage());
        }

        return new JSONArray();
    }

    protected String readFile(int id) {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(id)));
            String line = br.readLine();
            while (line != null) {
                line = line.trim();
                if (!"".equals(line)) {
                    sb.append(line);
                }
                line = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                }
            }
        }
        return sb.toString();
    }

}
