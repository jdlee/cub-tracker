package com.steeplesoft.cubtracker.android.activities;

import android.app.DatePickerDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ListView;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.SelectableListActivity;
import com.steeplesoft.cubtracker.android.adapter.SelectableListViewAdapter;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Scout;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class SelectScoutActivity extends ListActivity implements SelectableListActivity {
    private long rankId;
    private long itemId;
    private long partId;
    protected SelectableListViewAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(Constants.LOG_TAG, "Creating activity: " + getClass().getSimpleName());

//        setContentView(R.layout.select_scout);

        if (savedInstanceState != null) {
            rankId = savedInstanceState.getLong(IntentConstants.RANK_ID, -1);
            itemId = savedInstanceState.getLong(IntentConstants.ITEM_ID, -1);
            partId = savedInstanceState.getLong(IntentConstants.PART_ID, -1);
        } else {
            rankId = getIntent().getLongExtra(IntentConstants.RANK_ID, -1);
            itemId = getIntent().getLongExtra(IntentConstants.ITEM_ID, -1);
            partId = getIntent().getLongExtra(IntentConstants.PART_ID, -1);
        }
        init();
    }

    @Override
    public SelectableListViewAdapter getSelectableAdapter() {
        return adapter;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    protected void init() {
        getListView().addFooterView(getLayoutInflater().inflate(R.layout.ok_cancel_buttons, null));
//        final Activity activity = this;
        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        Object obj = getLastNonConfigurationInstance();
        if (obj != null) {
            adapter = (SelectableListViewAdapter) obj;
        } else {
            List<Scout> scoutsForRank = rankId == 0 ?
                    DatabaseAccess.getInstance().getScouts() : // get all scouts for Bobcat achievement
                    DatabaseAccess.getInstance().getScoutsForRank(rankId, true); // Get rank-appropriate scouts
            List<SelectableListViewAdapter.ListItem> listItems = new ArrayList<SelectableListViewAdapter.ListItem>();
            for (Scout s : scoutsForRank) {
                listItems.add(new SelectableListViewAdapter.ListItem(s.getId(), s.getName(), s, false));
            }
            adapter = new SelectableListViewAdapter(listItems,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            findViewById(R.id.button_ok).setEnabled(adapter.getSelected().length > 0);
                        }
                    });
            if (itemId != -1) {
                adapter.setSelected(DatabaseAccess.getInstance().getScoutsWhoCompletedItem(partId));
            } else if (partId != -1) {
                adapter.setSelected(DatabaseAccess.getInstance().getScoutsWhoCompletedPart(partId));
            }
        }
        setListAdapter(adapter);

        findViewById(R.id.button_ok).setEnabled(adapter.getSelected().length > 0);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putSerializable(IntentConstants.RANK_ID, rankId);
        savedInstanceState.putLong(IntentConstants.ITEM_ID, itemId);
        savedInstanceState.putLong(IntentConstants.PART_ID, partId);
    }

    public void onButtonPressed(View view) {
        switch (view.getId()) {
            case  R.id.button_ok:
                final Calendar c = Calendar.getInstance();
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        okClicked(year, month, day);
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
                break;
            case R.id.button_cancel:
                Intent resultIntent = new Intent();
                setResult(RESULT_CANCELED, resultIntent);
                finish();
                break;
        }
    }

    protected void okClicked(int year, int month, int day) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(IntentConstants.DATE, new Date(new GregorianCalendar(year, month, day).getTimeInMillis()));
        resultIntent.putExtra(IntentConstants.SELECTED,
                ((SelectableListViewAdapter)getListAdapter()).getSelected());
        setResult(RESULT_OK, resultIntent);
        finish();
    }
}
