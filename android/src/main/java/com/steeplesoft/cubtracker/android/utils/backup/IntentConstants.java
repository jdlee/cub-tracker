package com.steeplesoft.cubtracker.android.utils.backup;

/**
 * Created by jdlee on 8/7/14.
 */
public class IntentConstants {
    public static final String DEN_ID = "denId";
    public static final String ITEM_ID = "itemId";
    public static final String MEETING_ID = "meetingId";
    public static final String PART_ID = "partId";
    public static final String RANK_ID = "rank_id";
    public static final String SCOUT_ID = "scoutId";

    public static final String ITEM = "item";
    public static final String PARTS = "parts";
    public static final String DATE = "date";
    public static final String RANK = "rank";
    public static final String PART = "part";
    public static final String DEN = "den";

    public static final String DATA = "data";
    public static final String ID = "id";
    public static final String IDS = "ids";
    public static final String MEETING_DATE = "meeting_date";
    public static final String MEETING_NOTES = "meeting_notes";
    public static final String NAME = "name";
    public static final String NAMES = "names";
    public static final String SELECTED = "selected";
    public static final String SELECTED_RANK = "selected_rank";
    public static final String SEARCH_TEXT = "search_text";

    public static final String END_DATE = "endDate";
    public static final String REPORT_ALL = "reportAll";
//    public static final String SHOW_ACHIEVEMENTS = "showAchievements";
//    public static final String SHOW_ELECTIVES = "showElectives";
    public static final String SHOW_ADVENTURES_REQ = "showAdventuresReq";
    public static final String SHOW_ADVENTURES_ELECT = "showAdventuresElect";
//    public static final String SHOW_PINS_LOOPS = "showPinsLoops";
    public static final String SHOW_REMAINING = "showRemaining";
    public static final String SHOW_WEBELOS_BADGES = "showWebelosBadges";
    public static final String START_DATE = "startDate";

    public static final String REQUIRED = "required";
    public static final String RANK_NAME = "rankName";
}
