package com.steeplesoft.cubtracker.android.activities;

import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Item;
import com.steeplesoft.cubtracker.android.model.ItemCompletion;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;
import com.steeplesoft.cubtracker.android.utils.backup.NavUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jdlee on 7/30/14.
 */
public class ViewItemActivity extends /*ActionBar*/ListActivity {
    private Long itemId;
    private Item item;

//    public ViewItemActivity() {
//        super(R.layout.view_item);
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(Constants.LOG_TAG, "Creating activity: " + getClass().getSimpleName());
        setContentView(R.layout.view_item);

        getListView().setChoiceMode(ListView.CHOICE_MODE_NONE);
        itemId = (savedInstanceState == null) ?
                getIntent().getLongExtra(IntentConstants.ITEM_ID, -1) :
                savedInstanceState.getLong(IntentConstants.ITEM_ID);
        getListView().setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.setHeaderTitle("Completion Management");
                menu.add(0, Constants.CONTEXTMENU_DELETE_COMPLETION, 1, "Delete Completion");
            }
        });

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadItem();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(IntentConstants.ITEM_ID, itemId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.view_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            NavUtils.navigateUpTo(this, new Intent(this, MainActivity.class));
            return true;
        }

        Intent intent;
        switch (menuItem.getItemId()) {
            case R.id.view_details:
                intent = new Intent(this, BrowsePartsActivity.class);
                intent.putExtra(IntentConstants.ITEM, item);
                startActivityForResult(intent, Constants.ACTIVITY_VIEW_DETAILS);
                break;
            case R.id.rec_complete:
                intent = new Intent(this, SelectScoutActivity.class);
                if (item.getRank() != null) {
                    intent.putExtra(IntentConstants.RANK_ID, item.getRank().getId());
                }
                intent.putExtra(IntentConstants.ITEM_ID, item.getId());
                startActivityForResult(intent, Constants.ACTIVITY_RECORD_COMPLETION);
                break;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.ACTIVITY_RECORD_COMPLETION) {
            if (resultCode == RESULT_OK) {
                long[] selected = data.getLongArrayExtra(IntentConstants.SELECTED);
                Date date = (Date)data.getSerializableExtra(IntentConstants.DATE);
                if (selected != null) {
                    for (long scoutId : selected) {
                        DatabaseAccess.getInstance().createItemCompletion(itemId, scoutId, date);
                    }
                }
                loadItem();
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final Context context = this;
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        ListView list = (ListView)findViewById(android.R.id.list);
        final ItemCompletionWrapper c = (ItemCompletionWrapper) list.getAdapter().getItem(info.position);
//        final long id = c.getLong(c.getColumnIndex("_id"));
        switch (item.getItemId()) {
            case Constants.CONTEXTMENU_DELETE_COMPLETION:
                Util.showConfirmationDialog(this, "Are you sure you want to delete this completion?",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == DialogInterface.BUTTON_POSITIVE) {
                                    try {
                                        DatabaseAccess.getInstance().deleteItemCompletion(c.ic.getId());
                                        loadItem();
                                    } catch (SQLException e) {
                                        Util.showAlertDialog(context, "Error!", "Unable to delete the record.");
                                    }
                                }
                            }

                        });
                break;
        }
        return true;
    }


    protected void loadItem() {
        DatabaseAccess dba = DatabaseAccess.getInstance();
        item = dba.getItem(itemId);
        if (item != null) {
            Util.setText(findViewById(R.id.itemTitle), item.getTitle());
            Util.setText(findViewById(R.id.itemNumber),item.getDescription());
            if (item.getNotes() != null && !item.getNotes().isEmpty()) {
                findViewById(R.id.itemNotesLabel).setVisibility(View.VISIBLE);
                Util.setText(findViewById(R.id.itemNotes), item.getNotes().trim());
            } else {
                findViewById(R.id.itemNotesLabel).setVisibility(View.GONE);
                findViewById(R.id.itemNotes).setVisibility(View.GONE);
            }

            ImageView imageView = (ImageView) findViewById(R.id.icon);

            String mDrawableName = item.getImage();
            if (mDrawableName != null && !mDrawableName.isEmpty()) {
                imageView.setImageResource(getResources().getIdentifier(mDrawableName, "drawable", getPackageName()));
            }

            List<ItemCompletionWrapper> completed = new ArrayList<ItemCompletionWrapper>();
            for (ItemCompletion ic : dba.getCompletionsForItem(item)) {
                completed.add(new ItemCompletionWrapper(ic));
            }
            createListAdapter(completed);
        }
    }

    private void createListAdapter(List<ItemCompletionWrapper> scoutList) {
        if (scoutList.isEmpty()) {
            findViewById(android.R.id.empty).setVisibility(View.VISIBLE);
            getListView().setVisibility(View.GONE);
        }
//        Collections.sort(scoutList);
        ArrayAdapter adapter = new ArrayAdapter<ItemCompletionWrapper>(this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                scoutList.toArray(new ItemCompletionWrapper[]{}));
        setListAdapter(adapter);
    }

    private static class ItemCompletionWrapper {
        ItemCompletion ic;

        private ItemCompletionWrapper(ItemCompletion ic) {
            this.ic = ic;
        }

        public String toString() {
            return ic.getScout().getName() + " (completed " + Util.formatDate(ic.getTimestamp()) + ")";
        }
    }
}
