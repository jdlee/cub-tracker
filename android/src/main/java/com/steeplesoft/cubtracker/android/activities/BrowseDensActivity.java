package com.steeplesoft.cubtracker.android.activities;

import com.steeplesoft.cubtracker.android.fragments.BrowseDensFragment;

/**
 * Created by jdlee on 8/1/14.
 */
public class BrowseDensActivity extends DrawerLayoutActivity {
    public BrowseDensActivity() {
        super(BrowseDensFragment.class);
    }
}
