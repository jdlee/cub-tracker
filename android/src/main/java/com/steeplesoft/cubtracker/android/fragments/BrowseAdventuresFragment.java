package com.steeplesoft.cubtracker.android.fragments;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.CubTracker;
import com.steeplesoft.cubtracker.android.activities.ViewItemActivity;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Item;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;
import com.steeplesoft.cubtracker.android.utils.backup.RankEnum;

import java.sql.SQLException;

/**
 */
public class BrowseAdventuresFragment extends ListFragment {
    private Item[] items;
    private int rankId;
    private String type;

    public BrowseAdventuresFragment() {
    }

    @Override
    public void onStart() {
        super.onStart();
        Intent intent = getActivity().getIntent();
        boolean required = intent.getBooleanExtra(IntentConstants.REQUIRED, true);

        rankId = intent.getIntExtra(IntentConstants.RANK_ID, -1);
        type = required ?
                Constants.TYPE_ADVENTURE_REQ : Constants.TYPE_ADVENTURE_ELECTIVE;
        getActivity().setTitle("Browse " +
                (required ? " Required " : " Elective ") +
                intent.getStringExtra(IntentConstants.RANK_NAME) + " Adventures");

        if (type.equals(Constants.TYPE_ADVENTURE_ELECTIVE) && rankId == RankEnum.ARROW_OF_LIGHT.rank) {
            rankId = RankEnum.WEBELOS.rank; // Webelos and AOL share electives
        }

        setHasOptionsMenu(false);
        updateList();

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(view.getContext(), ViewItemActivity.class);
                intent.putExtra(IntentConstants.ITEM_ID, items[position].getId());
                startActivity(intent);
            }

        });
    }

    public void updateList() {
        try {
            items = DatabaseAccess.getInstance().getItemsForRankAndType(rankId, type).toArray(new Item[0]);
            ItemArrayAdapter adapter = new ItemArrayAdapter(getActivity(), items);
            setListAdapter(adapter);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private class ItemArrayAdapter extends ArrayAdapter<Item> {
        private final Context context;
        private final Item[] values;

        public ItemArrayAdapter(Context context, Item[] values) {
            super(context, -1, items);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View rowView = inflater.inflate(R.layout.list_item_with_image, parent, false);
            final TextView textView = (TextView) rowView.findViewById(R.id.label);
            final ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
            textView.setText(values[position].getTitle());

            String mDrawableName = values[position].getImage();
            if (mDrawableName != null || !mDrawableName.isEmpty()) {
                imageView.setImageResource(getResources().getIdentifier(mDrawableName, "drawable", context.getPackageName()));
            }

            ViewTreeObserver viewTreeObserver = textView.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
//                        textView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                        Log.d(Constants.LOG_TAG, "TextView w/h: " + textView.getWidth() + "/" +
//                                textView.getHeight());
                        imageView.getLayoutParams().height = textView.getHeight();
                        imageView.requestLayout();
                    }
                });
            }

            return rowView;
        }
    }
}
