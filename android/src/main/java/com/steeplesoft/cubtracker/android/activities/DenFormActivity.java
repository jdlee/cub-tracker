package com.steeplesoft.cubtracker.android.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.adapter.SelectableListViewAdapter;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.model.Rank;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jdlee on 8/5/14.
 */
public class DenFormActivity extends Activity {
    private long denId;
    private Den den;
    private long selectedRank;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(Constants.LOG_TAG, "Creating activity: " + getClass().getSimpleName());
        setContentView(R.layout.activity_add_den);
        denId = getIntent().getLongExtra(IntentConstants.DEN_ID, -1);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (denId > -1) {
            den = DatabaseAccess.getInstance().getDen(denId);

            setTitle("Edit Den"); // i18n
            Util.setText(findViewById(R.id.denName), den.getName());
        }

        final View.OnClickListener buttonPressed = new View.OnClickListener() {
            public void onClick(View v) {
                onButtonPressed(v);
            }
        };

        findViewById(R.id.saveButton).setOnClickListener(buttonPressed);
        findViewById(R.id.cancelButton).setOnClickListener(buttonPressed);

        prepareSpinner();
    }

    protected void prepareSpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        DatabaseAccess db = DatabaseAccess.getInstance();

//            @SuppressWarnings("deprecation")
        /*
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.list_item, db.getCursorForAllRanks(),
                new String[] { "name" }, new int[] { android.R.id.text1 });
        */
        List<SelectableListViewAdapter.ListItem> ranks = new ArrayList<SelectableListViewAdapter.ListItem>();
        for (Rank r : db.getRanks(1,7)) {
            ranks.add(new SelectableListViewAdapter.ListItem(r.getId(), r.getName(), r, false));
        }
        ArrayAdapter adapter = new ArrayAdapter<SelectableListViewAdapter.ListItem>(this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                ranks.toArray(new SelectableListViewAdapter.ListItem[]{}));
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
//                Cursor cursor = (Cursor) parent.getItemAtPosition(pos);
//                selectedRank = cursor.getLong(cursor.getColumnIndex("_id"));
                SelectableListViewAdapter.ListItem listItem = ((SelectableListViewAdapter.ListItem)parent.getItemAtPosition(pos));
                selectedRank = ((Rank)listItem.tag).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                //
            }
        });

        if (den != null) {
            for (int i = 0; i < spinner.getCount(); i++) {
                SelectableListViewAdapter.ListItem listItem = ((SelectableListViewAdapter.ListItem)spinner.getItemAtPosition(i));
                Rank r = (Rank)listItem.tag;
                if (den.getRank().getId() == r.getId()) {
                    spinner.setSelection(i);
                }
            }
        }
    }

    public void onButtonPressed(View view) {
        switch (view.getId()) {
            case R.id.saveButton:
                if (den == null) {
                    DatabaseAccess.getInstance().createDen(Util.getText(findViewById(R.id.denName)),
                            selectedRank);
                } else {
                    DatabaseAccess.getInstance().updateDen(den.getId(),
                            Util.getText(findViewById(R.id.denName)), selectedRank);
                }
                finish();
                break;
            case R.id.cancelButton:
                finish();
                break;
        }
    }

}
