package com.steeplesoft.cubtracker.android.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.CubTracker;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.adapter.ExpandableListAdapter;
import com.steeplesoft.cubtracker.android.adapter.util.ExpandListGroup;
import com.steeplesoft.cubtracker.android.model.Scout;

import java.util.List;

/**
 * Created by jdlee on 6/1/15.
 */
public class ScoutStatusExpandableListAdapter extends ExpandableListAdapter {
    public ScoutStatusExpandableListAdapter(Context context, List<ExpandListGroup> groups) {
        super(context, groups);
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View view, ViewGroup parent) {
        ExpandListGroup group = (ExpandListGroup) getGroup(groupPosition);
        if (view == null) {
            LayoutInflater inf = (LayoutInflater) CubTracker.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inf.inflate(R.layout.expandlist_group_den, null);
        }
        TextView tv = (TextView) view.findViewById(R.id.denName);
        tv.setTextColor(CubTracker.getAppContext().getResources().getColor(R.color.defaultText));
        tv.setText(group.getName());
        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View view, ViewGroup parent) {
        ExpandListChildScout child = (ExpandListChildScout) getChild(groupPosition, childPosition);
        Scout scout = (Scout) child.getTag();

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) CubTracker.getAppContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.expandlist_scout_status, null);
        }
        view.setTag(scout);
        ((TextView)view.findViewById(R.id.scoutName)).setTextColor(CubTracker.getAppContext().getResources().getColor(R.color.defaultText));
        Util.setText(view.findViewById(R.id.scoutName), child.getName());
        ProgressBar pb = (ProgressBar) view.findViewById(R.id.scoutProgress);
        pb.setProgress(Math.round((int) (child.getPercentComplete() * 100)));
        return view;
    }
}
