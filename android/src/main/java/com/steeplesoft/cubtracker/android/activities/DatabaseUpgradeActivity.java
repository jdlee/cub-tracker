package com.steeplesoft.cubtracker.android.activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Util;

public class DatabaseUpgradeActivity extends Activity implements DatabaseUpgradeFragment.UpgradeDatabaseCallbacks {
    private static final String TAG_TASK_FRAGMENT = "task_fragment";
    private DatabaseUpgradeFragment upgradeFragment;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database_upgrade);

        Util.setText(findViewById(R.id.headerText), "Welcome to Cub Tracker " + Util.getVersionString());
        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        FragmentManager fm = getFragmentManager();
        upgradeFragment = (DatabaseUpgradeFragment) fm.findFragmentByTag(TAG_TASK_FRAGMENT);

        // If the Fragment is non-null, then it is currently being
        // retained across a configuration change.
        if (upgradeFragment == null) {
            upgradeFragment = new DatabaseUpgradeFragment();
            fm.beginTransaction().add(upgradeFragment, TAG_TASK_FRAGMENT).commit();
        }
    }

    @Override
    public void setProgressBarMax(int max) {
        progressBar.setMax(max);
    }

    @Override
    public void resetProgressBar() {
        progressBar.setProgress(0);
    }

    @Override
    public void onPreExecute() {

    }

    @Override
    public void onProgressUpdate(int percent) {
        progressBar.setProgress(percent);
    }

    @Override
    public void onCancelled() {

    }

    @Override
    public void onPostExecute() {
        finish();
    }
}

