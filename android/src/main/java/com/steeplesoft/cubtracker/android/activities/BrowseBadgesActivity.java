package com.steeplesoft.cubtracker.android.activities;

import com.steeplesoft.cubtracker.android.fragments.BrowseBadgesFragment;

/**
 * Created by jdlee on 8/1/14.
 */
public class BrowseBadgesActivity extends DrawerLayoutActivity {
    public BrowseBadgesActivity() {
        super(BrowseBadgesFragment.class);
    }
}
