package com.steeplesoft.cubtracker.android.activities;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;
import com.steeplesoft.cubtracker.android.utils.backup.RankEnum;

/**
 * Created by jdlee on 7/30/14.
 */
public abstract class DrawerLayoutActivity extends Activity {
    private static final String HOME = "Home";
    private static final String ACHIEVEMENTS = "Achievements";
    private static final String ELECTIVES = "Electives";
    private static final String PINS_AND_LOOPS = "Pins and Loops";
    private static final String WEBELOS_BADGES = "Webelos Badges";
    private static final String MEETINGS = "Meetings";
    private static final String DENS = "Dens";
    private static final String SCOUTS = "Scouts";
    private static final String REPORTS = "Reports";
    private static final String BACKUPS = "Backups";

    protected final String[] navLabels = new String[] {
            HOME,
            ACHIEVEMENTS,
            ELECTIVES,
            PINS_AND_LOOPS,
            WEBELOS_BADGES,
            MEETINGS,
            DENS,
            REPORTS,
            BACKUPS
//            SCOUTS
    };
    protected DrawerLayout mDrawerLayout;
    protected ListView mDrawerList;
    protected ActionBarDrawerToggle mDrawerToggle;
    protected CharSequence mDrawerTitle;
    protected CharSequence mTitle;
    protected Class<? extends Fragment> initialFragment;
    private DatabaseAccess db;

    protected DrawerLayoutActivity(Class<? extends Fragment> initialFragment) {
        this.initialFragment = initialFragment;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(Constants.LOG_TAG, "Creating activity: " + getClass().getSimpleName());
        super.onCreate(savedInstanceState);
        db = DatabaseAccess.getInstance();
        setContentView(R.layout.activity_main);
        setupDrawer();

        try {
            getFragmentManager().beginTransaction()
                    .add(R.id.content_frame, initialFragment.newInstance(), "initial")
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

    }
    // endregion

    protected void setupDrawer() {
        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                R.string.navigation_drawer_open,R.string.navigation_drawer_close) {
            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);

                if (newState == 2) { // Find a constant?
                    addNavItems();
                }
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (mDrawerToggle != null) {
            mDrawerToggle.syncState();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ((mDrawerToggle != null) && (mDrawerToggle.onOptionsItemSelected(item))) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addNavItems() {
        LinearLayout ll = (LinearLayout) findViewById(R.id.navigation_drawer);
        ll.removeAllViews();

        addHeaderItem(ll, "Home", MainActivity.class);
        addSeparator(ll);
        addHeaderItem(ll, "Required Adventures", null);
        adventureSubItem(ll, RankEnum.TIGER, true);
        adventureSubItem(ll, RankEnum.WOLF, true);
        adventureSubItem(ll, RankEnum.BEAR, true);
        adventureSubItem(ll, RankEnum.WEBELOS, true);
        adventureSubItem(ll, RankEnum.ARROW_OF_LIGHT, true);
        addHeaderItem(ll, "Elective Adventures", null);
        adventureSubItem(ll, RankEnum.TIGER, false);
        adventureSubItem(ll, RankEnum.WOLF, false);
        adventureSubItem(ll, RankEnum.BEAR, false);
        adventureSubItem(ll, RankEnum.WEBELOS, false);
        adventureSubItem(ll, RankEnum.ARROW_OF_LIGHT, false);
        if (db.hasScout(RankEnum.WEB2)) {
            addHeaderItem(ll, "Webelos Badges", BrowseBadgesActivity.class);
        }
        addSeparator(ll);
        addHeaderItem(ll, "Meetings", BrowseMeetingsActivity.class);
        addHeaderItem(ll, "Dens", BrowseDensActivity.class);
        addSeparator(ll);
        addHeaderItem(ll, "Reports", GenerateReportActivity.class);
        addHeaderItem(ll, "Backups", BackupRestoreActivity.class);
    }


    /*
    <TextView xmlns:android="http://schemas.android.com/apk/res/android"
    android:id="@android:id/text1"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:textAppearance="?android:attr/textAppearanceListItemSmall"
    android:gravity="center_vertical"
    android:paddingLeft="16dp"
    android:paddingRight="16dp"
    android:textColor="#fff"
    android:background="?android:attr/activatedBackgroundIndicator"
    android:minHeight="?android:attr/listPreferredItemHeightSmall"/>
     */
    private void addHeaderItem(ViewGroup container, final String label, final Class<?> activity) {
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView item = new TextView(this);
        item.setTextAppearance(this, android.R.style.TextAppearance_DeviceDefault_Small);
        item.setGravity(Gravity.CENTER_VERTICAL);
        item.setPadding(16,0, 16, 0);
        item.setTextColor(Color.BLACK);
        item.setLayoutParams(lp);
        item.setText(label);
        item.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
        item.setTypeface(null, Typeface.BOLD);
        container.addView(item);

        final Activity parent = this;

        if (activity != null) {
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!parent.getClass().equals(activity)){
                        startActivity(new Intent(parent, activity));
                    }
                }
            });
        }
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        mDrawerLayout.closeDrawer(Gravity.START);
    }

    private void adventureSubItem(ViewGroup container, final RankEnum rank, final boolean required) {
        if (db.hasScout(rank)) {
            TextView item = createSubItemTextView(rank.name);
            item.setTag("adv_" + rank.name.toLowerCase() + "_" + required);
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mDrawerLayout.closeDrawer(Gravity.START);
                    Intent intent = new Intent(view.getContext(), BrowseAdventuresActivity.class);
                    intent.putExtra(IntentConstants.RANK_ID, rank.rank);
                    intent.putExtra(IntentConstants.RANK_NAME, rank.name);
                    intent.putExtra(IntentConstants.REQUIRED, required);

                    startActivity(intent);
                }
            });
            container.addView(item);
        }
    }

    private TextView createSubItemTextView(String label) {
        TextView item = new TextView(this);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.WRAP_CONTENT);
        item.setPadding(66,0, 16, 0);
        item.setTextAppearance(this, android.R.style.TextAppearance_DeviceDefault_Small);
        item.setGravity(Gravity.CENTER_VERTICAL);
        item.setTextColor(Color.BLACK);
        item.setLayoutParams(lp);
        item.setText(label);
        item.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
//        item.setPadding(50,0,0,0);
        return item;
    }

    private void addSeparator(ViewGroup container) {
        container.addView(getLayoutInflater().inflate(R.layout.navdrawer_separator, container, false));
    }

    private void selectItem(int position) {
        String label = navLabels[position];

        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerList);

        Class<?> target = null;

        if (HOME.equals(label))                  { target = MainActivity.class;
        } else if (WEBELOS_BADGES.equals(label)) { target = BrowseBadgesActivity.class;
        } else if (MEETINGS.equals(label))       { target = BrowseMeetingsActivity.class;
        } else if (DENS.equals(label))           { target = BrowseDensActivity.class;
        } else if (SCOUTS.equals(label))         { target = BrowseScoutsActivity.class;
        } else if (REPORTS.equals(label))        { target = GenerateReportActivity.class;
        } else if (BACKUPS.equals(label))        { target = BackupRestoreActivity.class;
        }

        if (target != null) {
//            if (!getClass().equals(target)){
                startActivity(new Intent(this, target));
//            }
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            selectItem(position);
        }
    }
}
