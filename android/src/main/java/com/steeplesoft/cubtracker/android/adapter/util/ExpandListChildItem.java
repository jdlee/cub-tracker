package com.steeplesoft.cubtracker.android.adapter.util;

import com.steeplesoft.cubtracker.android.model.Item;

public class ExpandListChildItem implements ExpandListChild {
    protected Item item;

    public ExpandListChildItem(Item item) {
        super();
        this.item = item;
    }

    @Override
    public String getName() {
        String name = (item.getNumber() != null) ?
                (item.getNumber() + " - " + item.getTitle()) :
                item.getTitle();
        return name;
    }

    @Override
    public void setName(String name) {
    }

    @Override
    public Object getTag() {
        return item;
    }

    @Override
    public void setTag(Object tag) {
    }

    @Override
    public long getId() {
        return item.getId();
    }

}
