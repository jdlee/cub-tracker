package com.steeplesoft.cubtracker.android.utils.backup;

import android.util.Log;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.Util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Vector;

/**
 * Created by jdlee on 2/12/14.
 */
public class ScpBackup implements BackupMethod {
    private JSch jsch = new JSch();
    private String host;
    private String user;
    private String pass;
//    private UploadHandlerThread mThread = null;
    private final BackupHandlerThread ht = new BackupHandlerThread();

    public ScpBackup(String host, String user, String pass) {
        this.host = host;
        this.user = user;
        this.pass = pass;
        JSch.setConfig("StrictHostKeyChecking", "no");
    }

    public List<String> getFileList() {
        final List<String> files = new ArrayList<String>();
        synchronized (ht) {
            ht.execute(new Runnable() {
                @Override
                public void run() {
                    Channel channel = null;
                    ChannelSftp c = null;
                    Session session = null;

                    try {
                        session = getSession();

                        channel = session.openChannel("sftp");
                        channel.connect();
                        c = (ChannelSftp) channel;
                        Vector v = c.ls(Constants.BACKUP_DIR);
                        for (int i = 0; i < v.size(); i++) {
                            ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry)v.get(i);
                            if (entry.getFilename().contains("cubtracker")) {
                                files.add(entry.getFilename());
                            }
                        }

                    } catch (Exception e) {
                        Util.displayMessage("Database restoration failed: " + e.getLocalizedMessage());
                    }

                    if (channel != null) {
                        c.disconnect();
                    }

                    if (session != null) {
                        session.disconnect();
                    }

                    synchronized (ht) {
                        Log.i(Constants.LOG_TAG, "Notifying");
                        ht.notify();
                        Log.i(Constants.LOG_TAG, "Notified");
                    }
                }
            }, true);
        }
        return files;
    }

    public void pruneBackups(int count) {
        List<String> files = getFileList();
        if (files.size() > count) {
            Collections.sort(files);
            final List<String> toDelete = files.subList(0, files.size() - count);
            synchronized (ht) {
                ht.execute(new Runnable() {
                    @Override
                    public void run() {
                        Channel channel = null;
                        ChannelSftp c = null;
                        Session session = null;

                        try {
                            session = getSession();

                            channel = session.openChannel("sftp");
                            channel.connect();
                            c = (ChannelSftp) channel;
                            Vector v = c.ls(Constants.BACKUP_DIR);
                            c.cd(Constants.BACKUP_DIR);
                            for (String file : toDelete) {
                                c.rm(file);
                            }

                        } catch (Exception e) {
                            Util.displayMessage("Database restoration failed: " + e.getLocalizedMessage());
                        }

                        if (channel != null) {
                            c.disconnect();
                        }

                        if (session != null) {
                            session.disconnect();
                        }

                        synchronized (ht) {
                            Log.i(Constants.LOG_TAG, "Notifying");
                            ht.notify();
                            Log.i(Constants.LOG_TAG, "Notified");
                        }
                    }
                }, true);
            }
        }
    }

    public boolean upload(String file) {
        final ResultHolder holder = new ResultHolder();

        synchronized (ht) {
            ht.execute(new Runnable() {
                @Override
                public void run() {
                    Channel channel = null;
                    ChannelSftp c = null;
                    Session session = null;

                    try {
                        session = getSession();

                        channel = session.openChannel("sftp");
                        channel.connect();
                        c = (ChannelSftp) channel;
                        try {
                            c.mkdir(Constants.BACKUP_DIR);
                        } catch (Exception e) {

                        }
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd.HH.mm");
                        String remoteFile = "cubtracker-" + sdf.format(new Date()) +".db";
                        c.put(Constants.LOCAL_PATH, Constants.BACKUP_DIR + "/" + remoteFile);
                        Util.displayMessage("Backup completed.");
                        holder.result = true;
                    } catch (Exception e) {
                        Util.displayMessage("Backup failed: " + e.getLocalizedMessage());
                        holder.result = false;
                    }

                    if (channel != null) {
                        c.disconnect();
                    }

                    if (session != null) {
                        session.disconnect();
                    }

                    synchronized (ht) {
                        ht.notify();
                    }
                }
            }, true);
        }

        return holder.result;
    }

    public boolean download(final String file) {
        final ResultHolder holder = new ResultHolder();

        synchronized (ht) {
            ht.execute(new Runnable() {
                @Override
                public void run() {
                    Channel channel = null;
                    ChannelSftp c = null;
                    Session session = null;

                    try {
                        session = getSession();

                        channel = session.openChannel("sftp");
                        channel.connect();
                        c = (ChannelSftp) channel;
                        c.get(Constants.BACKUP_DIR + "/" + file, Constants.LOCAL_PATH);
                        Util.displayMessage("Database restoration completed. Restarting application.");
                        holder.result = true;
                    } catch (Exception e) {
                        Util.displayMessage("Database restoration failed: " + e.getLocalizedMessage());
                        holder.result = false;
                    }

                    if (channel != null) {
                        c.disconnect();
                    }

                    if (session != null) {
                        session.disconnect();
                    }

                    synchronized (ht) {
                        ht.notify();
                    }
                }
            }, true);
        }

        return holder.result;
    }

    private Session getSession() throws JSchException {
        Session session = jsch.getSession(user, host, 22);
        session.setPassword(pass);
        session.connect();

        return session;
    }

}
