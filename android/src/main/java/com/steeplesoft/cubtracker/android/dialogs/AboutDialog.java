package com.steeplesoft.cubtracker.android.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Util;

public class AboutDialog extends Dialog {
    public AboutDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
        setTitle("About");

        ((TextView)findViewById(R.id.aboutTitle)).setText("Cub Tracker v. " + Util.getVersionString());
    }
}