package com.steeplesoft.cubtracker.android.activities;

import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.model.Scout;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;
import com.steeplesoft.cubtracker.android.utils.backup.NavUtils;
import com.steeplesoft.cubtracker.android.utils.backup.RankEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jdlee on 8/7/14.
 */
public class ViewDenActivity extends ListActivity {
    private long denId;
    private Den den;
    private Scout[] scouts;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(Constants.LOG_TAG, "Creating activity: " + getClass().getSimpleName());

        setContentView(R.layout.view_den);

        final ListView listView = getListView();
        listView.setChoiceMode(ListView.CHOICE_MODE_NONE);
        denId = (savedInstanceState == null) ?
                getIntent().getLongExtra(IntentConstants.DEN_ID, -1) :
                savedInstanceState.getLong(IntentConstants.DEN_ID);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Scout scout = scouts[position];
                Intent intent = new Intent(view.getContext(), ViewScoutActivity.class);
                intent.putExtra(IntentConstants.SCOUT_ID, scout.getId());
                startActivity(intent);
            }
        });

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadDen();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(IntentConstants.DEN_ID, denId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.view_den, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            NavUtils.navigateUpTo(this, new Intent(this, MainActivity.class));
            return true;
        }

        Intent intent;
        switch (menuItem.getItemId()) {
            case R.id.menu_edit:
                intent = new Intent(this, DenFormActivity.class);
                intent.putExtra(IntentConstants.DEN_ID, den.getId());
                startActivityForResult(intent, Constants.ACTIVITY_VIEW_DETAILS);
                break;
            case R.id.menu_promote:
                promoteDen();
                break;

            case R.id.menu_delete:
                if (DatabaseAccess.getInstance().getScoutsForDen(den.getId()).isEmpty()) {
                    final Context context = this;
                    Util.showConfirmationDialog(this, "Are you sure you want to delete this den?",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == DialogInterface.BUTTON_POSITIVE) {
                                        DatabaseAccess.getInstance().deleteDen(den.getId());
                                        finish();
                                    }
                                }
                            });

                } else {
                    Util.showAlertDialog(this,
                            "Error",
                            "You can not delete a den that has scouts. Please reassign or delete the scouts first.");
                }
                break;
            case R.id.menu_add_scout:
                intent = new Intent(this, ScoutFormActivity.class);
                intent.putExtra(IntentConstants.DEN, den);
                startActivityForResult(intent, Constants.ACTIVITY_ADD_SCOUT);
                break;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.ACTIVITY_ADD_SCOUT) {
            if (resultCode == RESULT_OK) {
                loadDen();
            }
        }
    }

    protected void loadDen() {
        DatabaseAccess dba = DatabaseAccess.getInstance();
        den = dba.getDen(denId);
        if (den != null) {
            Util.setText(findViewById(R.id.denName), den.getName());
            Util.setText(findViewById(R.id.denRank), den.getRank().getName());

            List<Scout> scoutsForDen = dba.getScoutsForDen(den.getId());
            scouts = scoutsForDen.toArray(new Scout[]{});
            createListAdapter(scoutsForDen);
        }
    }

    private void createListAdapter(List<Scout> scoutList) {
        Util.handleEmptyList(this, scoutList == null || scoutList.isEmpty());
//            findViewById(android.R.id.empty).setVisibility(View.VISIBLE);
//            getListView().setVisibility(View.GONE);
//        }
        List<String> list = new ArrayList<String>();
        for (Scout s : scoutList) {
            list.add(s.getName());
        }
        setListAdapter(new ArrayAdapter<String>(this, R.layout.list_item, list));
    }

    protected void promoteDen() {

        int rank = (int) den.getRank().getId();
        switch (rank) {
            case Constants.RANK_WEB1: rank = Constants.RANK_ARROW_OF_LIGHT; break;
            case Constants.RANK_WEB2: rank = Constants.RANK_BOY_SCOUTS; break;
            default: rank++;
        }

        final long oldRank = rank;
        final long newRank = rank;
        Util.showConfirmationDialog(this, "Are you sure you want to promote this den?",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                DatabaseAccess.getInstance().updateDen(den.getId(), den.getName(), newRank);
                                if (newRank == Constants.RANK_BOY_SCOUTS) {
                                    Util.displayMessage("This den has been promoted to Boy Scouts.");
                                    finish();
                                } else if (oldRank == Constants.RANK_WEB1) {
                                    Util.displayMessage("This 'Webelos 1 (Old Program)' has been updated to Arrow of Light (new program)."
                                            + " If this is incorrect, please manually select 'Webelos 2 (Old Program)'");
                                }

                                loadDen();
                                break;
                        }
                    }
                });
    }
}
