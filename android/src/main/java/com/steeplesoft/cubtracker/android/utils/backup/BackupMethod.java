package com.steeplesoft.cubtracker.android.utils.backup;

import java.util.List;

/**
 * Created by jdlee on 2/13/14.
 */
public interface BackupMethod {
    List<String> getFileList();
    void pruneBackups(int count);
    boolean upload(String file);
    boolean download(String file);
}
