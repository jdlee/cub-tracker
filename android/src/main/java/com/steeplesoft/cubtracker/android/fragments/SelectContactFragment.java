package com.steeplesoft.cubtracker.android.fragments;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.HeaderViewListAdapter;
import android.widget.ListView;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.adapter.SelectableListViewAdapter;
import com.steeplesoft.cubtracker.android.adapter.SelectableListViewAdapter.ListItem;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.util.ArrayList;
import java.util.List;

public class SelectContactFragment extends ListFragment {
    private SelectableListViewAdapter mAdapter;
//    private String mCurFilter;
    private long[] selected = new long[]{};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LayoutInflater li = (LayoutInflater) LayoutInflater.from(getActivity());
        return li.inflate(R.layout.select_contacts, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final View headerButtons = getActivity().getLayoutInflater().inflate(R.layout.ok_cancel_buttons, null);
        final View footerButtons = getActivity().getLayoutInflater().inflate(R.layout.ok_cancel_buttons, null);
        getListView().addHeaderView(headerButtons);
        getListView().addFooterView(footerButtons);

        final OnClickListener buttonPressed = new OnClickListener() {
            public void onClick(View v) {
                onButtonPressed(v);
            }
        };
        headerButtons.findViewById(R.id.button_ok).setOnClickListener(buttonPressed);
        headerButtons.findViewById(R.id.button_cancel).setOnClickListener(buttonPressed);
        footerButtons.findViewById(R.id.button_ok).setOnClickListener(buttonPressed);
        footerButtons.findViewById(R.id.button_cancel).setOnClickListener(buttonPressed);
//            mAdapter = new SelectableListViewCursorAdapter(getActivity(), R.layout.list_item, buildCursor(),
//                    Contacts._ID, new String[] { Contacts.DISPLAY_NAME }, new int[] { android.R.id.text1 });
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            mAdapter = new SelectableListViewAdapter(getItems());

            mAdapter.setSelected(selected);
            setListAdapter(mAdapter);


//            getActivity().findViewById(R.id.button_ok).setOnClickListener(buttonPressed);
//            getActivity().findViewById(R.id.button_cancel).setOnClickListener(buttonPressed);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setSelectedContacts(String selected) {
        if (selected != null) {
            String[] ids = selected.split(",");
            this.selected = new long[ids.length];
            int index = 0;
            for (String id : ids) {
                this.selected[index++] = Long.parseLong(id);
            }
        }
    }

    public void onButtonPressed(View view) {
        switch (view.getId()) {
            case R.id.button_ok:
                HeaderViewListAdapter hvla = (HeaderViewListAdapter)getListView().getAdapter();
                SelectableListViewAdapter adapter = (SelectableListViewAdapter) hvla.getWrappedAdapter();
                long[] selected = adapter.getSelected();

                Intent intent = new Intent();
                String[] names = Util.getContactNames(selected);
                intent.putExtra(IntentConstants.NAMES, names);
                intent.putExtra(IntentConstants.IDS, selected);
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
                break;
            case R.id.button_cancel:
                Intent resultIntent = new Intent();
                getActivity().setResult(Activity.RESULT_CANCELED, resultIntent);
                getActivity().finish();
                break;
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // Insert desired behavior here.
        Log.i("FragmentComplexList", "Item clicked: " + id);
        CursorWrapper item = (CursorWrapper) l.getAdapter().getItem(position);
        String name = item.getString(1);
        Intent intent = new Intent();
        intent.putExtra(IntentConstants.NAME, name); // TODO: constant
        intent.putExtra(IntentConstants.ID, (int) id);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    @SuppressWarnings("deprecation")
    protected List<ListItem> getItems() {
        List<ListItem> items = new ArrayList<ListItem>();
        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";

        Cursor managedCursor = getActivity().managedQuery(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                CONTACTS_SUMMARY_PROJECTION,
                ContactsContract.CommonDataKinds.Identity.IS_PRIMARY + "=1",
                null,
                sortOrder);
        managedCursor.moveToFirst();
        while (!managedCursor.isAfterLast()) {
            items.add(new ListItem(managedCursor.getLong(0),
                    managedCursor.getString(3),
                    managedCursor.getLong(0),
                    false));
            managedCursor.moveToNext();
        }
        managedCursor.close();
        return items;
    }

    // These are the Contacts rows that we will retrieve.
    private static final String[] CONTACTS_SUMMARY_PROJECTION = new String[] {
        ContactsContract.CommonDataKinds.Identity.LOOKUP_KEY,
        ContactsContract.CommonDataKinds.Identity.IDENTITY,
        ContactsContract.CommonDataKinds.Identity.IS_PRIMARY,
        ContactsContract.CommonDataKinds.Identity.DISPLAY_NAME,
        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
    };
}