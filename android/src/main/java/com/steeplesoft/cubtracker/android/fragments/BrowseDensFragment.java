package com.steeplesoft.cubtracker.android.fragments;

import android.app.Activity;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.activities.DenFormActivity;
import com.steeplesoft.cubtracker.android.activities.ViewDenActivity;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.sql.SQLException;

/**
 * Created by jdlee on 8/1/14.
 */
public class BrowseDensFragment extends ListFragment {
    private Den[] dens;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        updateList();

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(view.getContext(), ViewDenActivity.class);
                intent.putExtra(IntentConstants.DEN_ID, dens[position].getId());
                startActivity(intent);
            }

        });

        getListView().setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.setHeaderTitle("Den Management");
                menu.add(0, Constants.CONTEXTMENU_EDIT_DEN, 1, "Edit Den");
                menu.add(0, Constants.CONTEXTMENU_DELETE_DEN, 1, "Delete Den");
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.activity_dens, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add_den:
                startActivityForResult(new Intent(getActivity(), DenFormActivity.class), Constants.ACTIVITY_ADD_DEN);
                break;
            case R.id.menu_promote:
                promoteDens();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.ACTIVITY_ADD_DEN) {
            if (resultCode == Activity.RESULT_OK) {
                updateList();
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final Den den = (Den) getListAdapter().getItem(info.position);
        switch (item.getItemId()) {
            case Constants.CONTEXTMENU_EDIT_DEN:
                Intent intent = new Intent(getActivity(), DenFormActivity.class);
                intent.putExtra(IntentConstants.DEN_ID, den.getId());
                startActivityForResult(intent, Constants.ACTIVITY_EDIT_SCOUT);
                break;
            case Constants.CONTEXTMENU_DELETE_DEN:
                if (DatabaseAccess.getInstance().isDenEmpty(den.getId())) {
                    Util.showConfirmationDialog(getActivity(), "Are you sure you want to delete " + den.getName(),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == DialogInterface.BUTTON_POSITIVE) {
                                        DatabaseAccess.getInstance().deleteDen(den.getId());
                                        updateList();
                                    }
                                }
                            });
                } else {
                    Util.showAlertDialog(getActivity(), "Error", "Can not delete the den. There are scouts assigned to it.");
                }
                break;
        }
        return true;
    }

    public void updateList() {
        dens = DatabaseAccess.getInstance().getDens().toArray(new Den[]{});
        ArrayAdapter<Den> adapter = new ArrayAdapter<Den>(getActivity(), R.layout.list_item, dens);
        setListAdapter(adapter);
    }

    protected void promoteDens() {
        Util.showConfirmationDialog(getActivity(), "Are you sure you want to promote all the dens?",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                try {
                                    DatabaseAccess.getInstance().promoteDens();
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                                break;
                        }
                    }
                });
    }
}
