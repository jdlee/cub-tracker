package com.steeplesoft.cubtracker.android.activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.SelectableListActivity;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.adapter.SelectableListViewAdapter;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.model.Meeting;
import com.steeplesoft.cubtracker.android.model.Scout;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class MeetingFormActivity extends ListActivity implements SelectableListActivity {
    private Den selectedDen;
    private Meeting meeting = new Meeting();
    private long meetingId;

    private SelectableListViewAdapter selListViewAdapter;
    private static final int DIALOG_MEETING_DATE = 1;
    private boolean restored = false;

    // State vars
    private long[] selected = null;
    private String meetingDate;
    private String meetingNotes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(Constants.LOG_TAG, "Creating activity: " + getClass().getSimpleName());

        final ListView listView = getListView();
        meetingId = getIntent().getLongExtra(IntentConstants.MEETING_ID, -1);
        if (meetingId > -1) {
            meeting = DatabaseAccess.getInstance().getMeeting(meetingId);
            selectedDen = meeting.getDen();
        }

        listView.addHeaderView(getLayoutInflater().inflate(R.layout.meeting_form_header, null));
        listView.addFooterView(getLayoutInflater().inflate(R.layout.ok_cancel_buttons, null));
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        selListViewAdapter = new SelectableListViewAdapter(new ArrayList<SelectableListViewAdapter.ListItem>());
        setListAdapter(selListViewAdapter);

        ((Button) findViewById(R.id.meetingDate)).setOnClickListener(new OnClickListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                showDialog(DIALOG_MEETING_DATE);
            }

        });

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public SelectableListViewAdapter getSelectableAdapter() {
        return selListViewAdapter;
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        restored = true;
        meetingId = state.getLong(IntentConstants.MEETING_ID, -1);
        selected = state.getLongArray(IntentConstants.SELECTED);
        meetingDate = state.getString(IntentConstants.MEETING_DATE);
        meetingNotes = state.getString(IntentConstants.MEETING_NOTES);
        selectedDen = (Den)state.getSerializable(IntentConstants.DEN);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLongArray(IntentConstants.SELECTED, selListViewAdapter.getSelected());
        outState.putString(IntentConstants.MEETING_DATE, ((Button) findViewById(R.id.meetingDate)).getText().toString());
        outState.putString(IntentConstants.MEETING_NOTES, Util.getText(findViewById(R.id.meetingNotes)));
        outState.putLong(IntentConstants.MEETING_ID, meetingId);
        long denId = ((Spinner) findViewById(R.id.spinner)).getSelectedItemId();
        outState.putSerializable(IntentConstants.DEN, DatabaseAccess.getInstance().getDen(denId));
    }

    @Override
    protected void onStart() {
        super.onStart();
        setupUserInterface();
    }

    protected void setupUserInterface() {
        final Button button = (Button) findViewById(R.id.meetingDate);

        if (meetingId > -1) {
            meeting = DatabaseAccess.getInstance().getMeeting(meetingId);
            selectedDen = meeting.getDen();
            setTitle("Edit Meeting"); // i18n
        }

        prepareSpinner();
        setupScoutList();

        if (restored) {
            selListViewAdapter.setSelected(selected);
            button.setText(meetingDate);
            Util.setText(findViewById(R.id.meetingNotes), meetingNotes);
        } else if (meeting != null) {
            selListViewAdapter.setSelected(DatabaseAccess.getInstance().getAttendees(meeting.getId()));
            if (meeting.getNotes() != null) {
                Util.setText(findViewById(R.id.meetingNotes), meeting.getNotes());
            }
            button.setText(Util.formatDate(meeting.getMeetingDate()));
        }
    }

    private void setupScoutList() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        SpinnerAdapter adapter = spinner.getAdapter();
        if (selectedDen == null && adapter.getCount() > 0) {
            selectedDen = (Den) adapter.getItem(0);
        }

        if (selectedDen != null) {
            List<SelectableListViewAdapter.ListItem> items = new ArrayList<SelectableListViewAdapter.ListItem>();
            for (Scout s : DatabaseAccess.getInstance().getScoutsForDen(selectedDen.getId())) {
                items.add(new SelectableListViewAdapter.ListItem(s.getId(), s.getName(), s, false));
            }
            selListViewAdapter = new SelectableListViewAdapter(items);
            selListViewAdapter.setSelected(DatabaseAccess.getInstance().getAttendees(meetingId));
            setListAdapter(selListViewAdapter);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        switch (id) {
            case DIALOG_MEETING_DATE:
                return new DatePickerDialog(this, getDatePickerListener(R.id.meetingDate), year, month, day);
        }
        return null;
    }

    // TODO: make a util method
    private DatePickerDialog.OnDateSetListener getDatePickerListener(final int id) {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                Button button = (Button) findViewById(id);
                Calendar c = new GregorianCalendar(selectedYear, selectedMonth, selectedDay);
                button.setTag(new Date(c.getTimeInMillis()));
                button.setText(Util.formatDate(c.getTime()));
//                        new StringBuilder().append(selectedMonth + 1).append("-").append(selectedDay).append("-")
//                        .append(selectedYear).append(" "));

                // set selected date into date picker also
                view.init(selectedYear, selectedMonth, selectedDay, null);
                meeting.setMeetingDate(new Date(new GregorianCalendar(selectedYear, selectedMonth, selectedDay).getTimeInMillis()));
            }
        };

        return listener;
    }

    @SuppressLint("SimpleDateFormat")

    protected void prepareSpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner);

        List<SelectableListViewAdapter.ListItem> listItems = new ArrayList<SelectableListViewAdapter.ListItem>();
        for (Den d : DatabaseAccess.getInstance().getDens()) {
            listItems.add(new SelectableListViewAdapter.ListItem(d.getId(), d.getName(), d, false));
        }
//        final SelectableListViewAdapter adapter = new SelectableListViewAdapter(listItems);
        final ArrayAdapter<Den> adapter = new ArrayAdapter<Den>(this, R.layout.list_item, DatabaseAccess.getInstance().getDens());
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                selectedDen = (Den) parent.getItemAtPosition(pos);
                setupScoutList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                //
            }
        });

        if (meeting != null) {
            Den den = meeting.getDen();
            for (int i = 0; i < listItems.size(); i++) {
                SelectableListViewAdapter.ListItem li = listItems.get(i);
                if (li.tag.equals(den)) {
                    spinner.setSelection(i, true);
                }
            }
        }
    }

    public void onButtonPressed(View view) {
        switch (view.getId()) {
            case R.id.button_ok:
                try {
                    if (selectedDen != null) {
                        if (meeting.getId() == 0) {
                            DatabaseAccess.getInstance().createMeeting(selectedDen,
                                    meeting.getMeetingDate(),
                                    Util.getText(findViewById(R.id.meetingNotes)),
                                    selListViewAdapter.getSelected());
                        } else {
                            DatabaseAccess.getInstance().updateMeeting(meeting.getId(), selectedDen,
                                    meeting.getMeetingDate(),
                                    Util.getText(findViewById(R.id.meetingNotes)),
                                    selListViewAdapter.getSelected());
                        }

                        Intent intent = new Intent();
                        intent.putExtra(IntentConstants.MEETING_ID, meeting.getId());
                        setResult(RESULT_OK, intent);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
        }
        finish();
    }
}
