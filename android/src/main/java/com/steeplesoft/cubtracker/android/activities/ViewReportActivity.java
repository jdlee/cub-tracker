package com.steeplesoft.cubtracker.android.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.SpannableString;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.ReportHolder;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class ViewReportActivity extends Activity {
    private static final int ACTIVITY_EMAIL = 1;
    private Boolean showRemaining = Boolean.FALSE;
    private Boolean reportAll;
    private ReportHolder report;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(Constants.LOG_TAG, "Creating activity: " + getClass().getSimpleName());

        setContentView(R.layout.report);

        reportAll = getIntent().getBooleanExtra(IntentConstants.REPORT_ALL, Boolean.FALSE);
        showRemaining = getIntent().getBooleanExtra(IntentConstants.SHOW_REMAINING, Boolean.FALSE);

        report = new ReportHolder();
        report.setShowRemaining(showRemaining);
        report.setReportAll(reportAll);
        report.setSelectedScouts(getIntent().getLongArrayExtra(IntentConstants.SELECTED));
        report.setStartDate((Date)getIntent().getExtras().get(IntentConstants.START_DATE));
        report.setEndDate((Date)getIntent().getExtras().get(IntentConstants.END_DATE));
        report.setShowRequiredAdventures(getIntent().getBooleanExtra(IntentConstants.SHOW_ADVENTURES_REQ, Boolean.TRUE));
        report.setShowElectiveAdventures(getIntent().getBooleanExtra(IntentConstants.SHOW_ADVENTURES_ELECT, Boolean.TRUE));
        report.setShowWebelosBadges(getIntent().getBooleanExtra(IntentConstants.SHOW_WEBELOS_BADGES, Boolean.FALSE));

        if (report.getReport() == null || report.getReport().isEmpty()) {
            Util.displayMessage("There are no items to report.", Toast.LENGTH_LONG);
            finish();
        }

        TextView reportView = (TextView)findViewById(R.id.reportText);
        reportView.setMovementMethod(new ScrollingMovementMethod());
        Util.setText(reportView, report.getReport());

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_view_report, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_send_report:
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            sendEmail();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                        }
                        dialog.dismiss();
                    }
                };

                if (!showRemaining && !reportAll) {
                    final String message = "Are you sure? Emailing the report will mark all items as reported.";
                    Util.showConfirmationDialog(this, message, dialogClickListener);
                } else {
                    sendEmail();
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void sendEmail() {
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("application/octet-stream");
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Cub Tracker Activity Report");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, new SpannableString(report.getReport()));
        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/temp");
        if (!dir.exists()) {
            Log.d(Constants.LOG_TAG, "Creating " + dir.getAbsolutePath());
            dir.mkdirs();
        }
        final File file = new File(dir, "activityReport.csv");
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            bw.write(report.getAttachment());
            file.deleteOnExit();
            Log.d(Constants.LOG_TAG, "Deleting file on exit");
            bw.close();
        } catch (IOException e) {
            Log.e(Constants.LOG_TAG, e.getMessage());
        }
        Log.d(Constants.LOG_TAG, "Emailing report file " + file.getAbsolutePath());
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        startActivityForResult(Intent.createChooser(emailIntent, "Email:"), ACTIVITY_EMAIL);

        if (!showRemaining && !reportAll) {
            report.markRecordsReported();
            finish();
        }
        finish();
    }
}
