package com.steeplesoft.cubtracker.android.activities;

import com.steeplesoft.cubtracker.android.fragments.BrowsePinsAndLoopsFragment;

/**
 * Created by jdlee on 8/1/14.
 */
public class BrowsePinsAndLoopsActivity extends ExpandableListActivity {
    public BrowsePinsAndLoopsActivity() {
        super(BrowsePinsAndLoopsFragment.class);
    }
}
