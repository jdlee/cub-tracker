package com.steeplesoft.cubtracker.android.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.CubTracker;
import com.steeplesoft.cubtracker.android.Util;

public class NewVersionDialog extends Dialog {
    private String latestVersion;
    public NewVersionDialog(Context context, String latestVersion) {
        super(context);
        this.latestVersion = latestVersion;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_version);
        setTitle("New Version");
        String text = "A new version of Cub Tracker (" + latestVersion + ") has been released. " +
                "You are running version " + Util.getVersionString() + ". " +
                "Press 'OK' to go to the Play Store?";
        ((TextView)findViewById(R.id.newVersion)).setText(text);

        ((Button) findViewById(R.id.button_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleButton(false);
            }
        });

        ((Button) findViewById(R.id.button_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleButton(true);
            }
        });
    }

    protected void handleButton(boolean okPressed) {
        boolean dontShow = ((CheckBox)findViewById(R.id.dontShow)).isChecked();
        Util.setStringPref(Constants.PREF_LAST_VERSION_CHECKED, latestVersion);
        Util.setBooleanPref(Constants.PREF_DONT_SHOW_VERSION_CHECK, dontShow);
        if (okPressed) {
            final String appName = "com.steeplesoft.cubtracker";
            try {
                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+appName));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                CubTracker.getAppContext().startActivity(intent);
            } catch (android.content.ActivityNotFoundException anfe) {
                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id="+appName));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                CubTracker.getAppContext().startActivity(intent);
            }
        }
        dismiss();
    }
}
