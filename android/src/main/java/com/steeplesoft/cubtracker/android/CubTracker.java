package com.steeplesoft.cubtracker.android;

import android.app.Application;
import android.content.Context;
import android.os.Build;

import com.steeplesoft.cubtracker.R;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpPostSender;

import java.util.Locale;

@ReportsCrashes(formKey = "dEE0ZWM0bS1JWlhjRGNDY2pvcFhPM2c6MQ",
        socketTimeout = 120000,
        mode = ReportingInteractionMode.DIALOG,
        resDialogText = R.string.crash_dialog_text,
        resDialogIcon = android.R.drawable.ic_dialog_info, //optional. default is a warning sign
        resDialogTitle = R.string.crash_dialog_title, // optional. default is your application name
        resDialogCommentPrompt = R.string.crash_dialog_comment_prompt, // optional. when defined, adds a user text field input with this text resource as a label
        resDialogOkToast = R.string.crash_dialog_ok_toast // optional. displays a Toast message when the user accepts to send a report.
)
public class CubTracker extends Application {
    private static Context context;

    public void onCreate() {
        super.onCreate();
        CubTracker.context = getApplicationContext();
        final String buildModel = Build.MODEL;
        if ((buildModel != null) && !buildModel.toLowerCase(Locale.US).contains("sdk")) {
            try {
                ACRA.init(this);
                ACRA.getErrorReporter().setReportSender(new HttpPostSender("http://cubtracker.com/error_reporting__.php", null));
            } catch (Exception e) {
                // Just in case
            }
        }
    }

    public static Context getAppContext() {
        return CubTracker.context;
    }

}
