package com.steeplesoft.cubtracker.android.activities;

import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

import com.inscription.WhatsNewDialog;
import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.VersionCheckAsyncTask;
import com.steeplesoft.cubtracker.android.db.DatabaseHelper;
import com.steeplesoft.cubtracker.android.dialogs.AboutDialog;
import com.steeplesoft.cubtracker.android.dialogs.SearchDialog;
import com.steeplesoft.cubtracker.android.fragments.HomeFragment;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.util.Locale;

public class MainActivity extends DrawerLayoutActivity {
    private static boolean checked = false;
    private DatabaseHelper databaseHelper;

    public MainActivity() {
        super(HomeFragment.class);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseHelper = Util.initHelper(this);
        if (databaseHelper.needsValidation()) {
            startActivityForResult(new Intent(this, DatabaseUpgradeActivity.class), Constants.ACTIVITY_UPGRADE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!checked) {
            new VersionCheckAsyncTask(this).execute();
            checked = true;
        }
//        if ((Build.MODEL != null) && !Build.MODEL.toLowerCase(Locale.US).contains("sdk"))
        {
//            ChangeLogDialog dialog = new ChangeLogDialog(this);
//            dialog.show();
            WhatsNewDialog dialog = new WhatsNewDialog(this);
            dialog.show();
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_SEARCH) {
            performSearch();
        }
        return super.onKeyUp(keyCode, event);
    }

    private void performSearch() {
        new SearchDialog(this, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                showSearchResults((SearchDialog) dialog);
            }

        }).show();
    }

    protected void showSearchResults(SearchDialog dialog) {
        Intent intent = new Intent(this, SearchResultsActivity.class);
        intent.putExtra(IntentConstants.SEARCH_TEXT, dialog.getSearchText());
        intent.putExtra(IntentConstants.RANK_ID, dialog.getSelectedRank());
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add_den:
                startActivityForResult(new Intent(this, DenFormActivity.class), Constants.ACTIVITY_ADD_DEN);
                break;
            case R.id.menu_add_scout:
                startActivityForResult(new Intent(this, ScoutFormActivity.class), Constants.ACTIVITY_ADD_SCOUT);
                break;
            case R.id.menu_add_meeting:
                startActivityForResult(new Intent(this, MeetingFormActivity.class), Constants.ACTIVITY_ADD_MEETING);
                break;
            case R.id.menu_search:
                performSearch();
                break;
            case R.id.menu_about:
                new AboutDialog(this).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.ACTIVITY_ADD_SCOUT) {
            if (resultCode == RESULT_OK) {
            }
        } else if (requestCode == Constants.ACTIVITY_UPGRADE) {
            Fragment f = getFragmentManager().findFragmentByTag("initial");
            f.onStart();
        }
    }
}
