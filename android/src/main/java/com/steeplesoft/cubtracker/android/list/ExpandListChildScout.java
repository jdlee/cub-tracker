package com.steeplesoft.cubtracker.android.list;

import com.steeplesoft.cubtracker.android.adapter.util.ExpandListChild;
import com.steeplesoft.cubtracker.android.model.Scout;

/**
 * Created by jdlee on 6/1/15.
 */
public class ExpandListChildScout implements ExpandListChild {
    private Scout scout;
    private double percentComplete;

    public ExpandListChildScout(Scout s, double percentComplete) {
        this.scout = s;
        this.percentComplete = percentComplete;
    }

    @Override
    public long getId() {
        return scout.getId();
    }

    @Override
    public String getName() {
        return scout.getName();
    }

    @Override
    public void setName(String Name) {

    }

    @Override
    public Object getTag() {
        return scout;
    }

    @Override
    public void setTag(Object Tag) {

    }

    public double getPercentComplete() {
        return percentComplete;
    }
}
