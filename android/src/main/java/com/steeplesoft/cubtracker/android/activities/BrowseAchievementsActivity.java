package com.steeplesoft.cubtracker.android.activities;

import com.steeplesoft.cubtracker.android.fragments.BrowseAchievementsFragment;

public class BrowseAchievementsActivity extends ExpandableListActivity {
    public BrowseAchievementsActivity() {
        super(BrowseAchievementsFragment.class);
    }
}
