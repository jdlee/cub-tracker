package com.steeplesoft.cubtracker.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.activities.ViewItemActivity;
import com.steeplesoft.cubtracker.android.adapter.ExpandableListAdapter;
import com.steeplesoft.cubtracker.android.adapter.util.ExpandListGroup;
import com.steeplesoft.cubtracker.android.adapter.util.ExpandListGroupPinLoop;
import com.steeplesoft.cubtracker.android.model.Item;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.util.ArrayList;
import java.util.List;

public class BrowsePinsAndLoopsFragment extends ExpandableListFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LayoutInflater li = (LayoutInflater) LayoutInflater.from(getActivity());
        return li.inflate(R.layout.list_expandable, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        mList = (ExpandableListView) getView().findViewById(android.R.id.list);
        mList.setTag("pinsAndLoops");
        setListAdapter(new ExpandableListAdapter(getActivity(), getGroupList()));
//        mList.setAdapter(mAdapter);
        mList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View view, int groupPosition, int childPosition,
                    long id) {
                TextView tv = (TextView) view.findViewById(R.id.tvChild);
                Item item = (Item) tv.getTag();
                Intent intent = new Intent(view.getContext(), ViewItemActivity.class);
                intent.putExtra(IntentConstants.ITEM_ID, item.getId());
                startActivity(intent);
                return false;
            }
        });
    }

    protected List<ExpandListGroup> getGroupList() {
        List<ExpandListGroup> list = new ArrayList<ExpandListGroup>();
        list.add(new ExpandListGroupPinLoop("L", "Belt Loops"));
        list.add(new ExpandListGroupPinLoop("P", "Pins"));

        return list;
    }
}
