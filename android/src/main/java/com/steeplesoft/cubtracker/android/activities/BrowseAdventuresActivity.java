package com.steeplesoft.cubtracker.android.activities;

import com.steeplesoft.cubtracker.android.fragments.BrowseAdventuresFragment;

public class BrowseAdventuresActivity extends DrawerLayoutActivity { //BaseListActivity {
    public BrowseAdventuresActivity() {
        super(BrowseAdventuresFragment.class);
    }
}
