package com.steeplesoft.cubtracker.android.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.steeplesoft.cubtracker.R;

public class ConfirmWithDontShowDialog extends AlertDialog {
    private OnClickListener callback;
    private Dialog dialog;
    private String message;

    public ConfirmWithDontShowDialog(Context context, String message, OnClickListener onClickListener) {
        super(context, R.style.AppTheme);
        this.callback = onClickListener;
        this.message = message;
//        FrameLayout fl = (FrameLayout) findViewById(android.R.id.custom);
//        addContentView(getLayoutInflater().inflate(R.layout.dont_show, null),
//                new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        dialog = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Confirm...");
        setMessage(message);

        ((Button) findViewById(android.R.id.button1)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
            }
        });
        ((Button) findViewById(android.R.id.button2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onClick(dialog, DialogInterface.BUTTON_NEGATIVE);
            }
        });
    }
}
