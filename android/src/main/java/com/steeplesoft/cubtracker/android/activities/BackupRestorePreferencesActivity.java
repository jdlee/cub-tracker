package com.steeplesoft.cubtracker.android.activities;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.util.Log;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;

/**
 * Created by jdlee on 2/12/14.
 */
public class BackupRestorePreferencesActivity extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(Constants.LOG_TAG, "Creating activity: " + getClass().getSimpleName());
        addPreferencesFromResource(R.layout.backupprefs);
    }
}
