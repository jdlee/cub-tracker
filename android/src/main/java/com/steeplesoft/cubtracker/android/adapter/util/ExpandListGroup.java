package com.steeplesoft.cubtracker.android.adapter.util;

import java.util.List;

public interface ExpandListGroup {
    public String getName();

    public void setName(String name);

    public List<ExpandListChild> getItems();

    public void setItems(List<ExpandListChild> items);
}
