package com.steeplesoft.cubtracker.android.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;
import android.util.Log;

import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.CubTracker;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.activities.DatabaseUpgradeFragment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DatabaseHelper extends SQLiteOpenHelper {
    private int oldVersion;
    private int newVersion;

    public DatabaseHelper(Context context) {
        super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(Constants.LOG_TAG, db.getPath());
        Log.i(Constants.LOG_TAG, "Creating database");
        validateDatabase(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //
    }

    //    @Override
    public void onUpgrade1(SQLiteDatabase db, int oldVersion, int newVersion) {
        int dbVersion = DatabaseAccess.getInstance().getDatabaseVersion();
        if (dbVersion == 0) {
            dbVersion = oldVersion;
        }
        if (dbVersion < Constants.VERSION_2_0_4) {
            upgradeDatabase(db, "2_0_4");
        }
        if (dbVersion < Constants.VERSION_2_2) {
            upgradeDatabase(db, "2_2");
        }
        if (dbVersion < Constants.VERSION_2_4) {
		    upgradeDatabase(db, "2_4");
        }
        if (dbVersion < Constants.VERSION_2_7) {
            upgradeDatabase(db, "2_7");
            updateGroupCounts(db);
            updatePartCounts(db);
        }
        if (dbVersion < Constants.VERSION_3_0) {
            upgradeDatabase(db, "3_0");
            performV3Upgrade(db);
        }
        if (dbVersion < Constants.VERSION_4_0) {
            try {
                dropColumn(db,
                        "CREATE TABLE items (_id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT, rank_id INTEGER, number TEXT, title TEXT, notes TEXT, image TEXT, group_count int, FOREIGN KEY (rank_id) REFERENCES ranks(_id));",
                        "items",
                        new String[] {"image"});
            } catch (SQLException e) {
                e.printStackTrace();
            }

            upgradeDatabase(db, "4_0");
            updateGroupCounts(db);
            updatePartCounts(db);
        }
        db.execSQL("UPDATE settings SET setting_value = " + Constants.DATABASE_VERSION + " WHERE setting_key = 'db_version'");
    }

    public boolean needsValidation() {
        try {
            File db = new File (Constants.DATABASE_PATH + "/" + Constants.DATABASE_NAME);
            if (!db.exists()) {
                copyDatabase();
            }

//            return DatabaseUpgradeFragment.createTask().hasTasks();
            return DatabaseAccess.getInstance().getDatabaseVersion() != Constants.DATABASE_VERSION;
        } catch (Exception e) {
            Log.e(Constants.LOG_TAG, e.getMessage());
        }
        return false;
    }

    public void validateDatabase(SQLiteDatabase db) {
    }

	private void upgradeDatabase(SQLiteDatabase db, String version) {
		try {
			final InputStream is = CubTracker.getAppContext().getAssets().open("db" + version + ".sql");
			final BufferedReader br = new BufferedReader(new InputStreamReader(
					is));
            String line = br.readLine();
			while (line != null) {
				if (!"".equals(line.trim()) && !line.trim().startsWith("#") && !line.trim().startsWith("--")) {
                    Log.d(Constants.LOG_TAG, line);
					db.execSQL(line);
				}
				line = br.readLine();
			}
			try {
				br.close();
				is.close(); // unnecessary?
			} catch (Exception e) {
			}
		} catch (IOException ioe) {
			Log.d(Constants.LOG_TAG, ioe.getMessage());
		}
	}

    private void performV3Upgrade(SQLiteDatabase db) {
        final String sql = "INSERT INTO scouts (name, rank_id, email_address, phone_number, contacts, photo) VALUES (?,?,?,?,?,?);";
        final String createDenSql = "INSERT INTO dens (name, rank_id) VALUES (?,?);";

        Map<Integer, Long> dens = new HashMap<Integer, Long>();
        Cursor c = db.rawQuery("SELECT _id, name, rank_id, email_address, phone_number, contacts FROM scouts ORDER by rank_id", null);
        c.moveToFirst();
        int rankIdx = c.getColumnIndex("rank_id");
        int idIdx = c.getColumnIndex("_id");
        while (!c.isAfterLast()) {
            int rank = c.getInt(rankIdx);
            long id = c.getLong(idIdx);
            Long denId = dens.get(rank);
            if (denId == null) {
                db.execSQL(createDenSql, new Object[] { "Den " + rank, rank });

                Cursor cursor = db.rawQuery("SELECT last_insert_rowid()", null);
                cursor.moveToFirst();
                denId = cursor.getLong(0);
                cursor.close();
                dens.put(rank, denId);
                db.execSQL("UPDATE meetings SET den_id = ? WHERE rank_id = ?", new Object[] { denId, rank });
            }
            db.execSQL("UPDATE scouts SET den_id = ? WHERE _id = ?", new Object[] { denId, id });
            c.moveToNext();
        }
        c.close();
//        db.execSQL("ALTER TABLE scouts DROP COLUMN rank_id");
        try {
            dropColumn(db,
                    "CREATE TABLE scouts (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, den_id INTEGER REFERENCES dens(_id), contacts TEXT, photo BLOB, email_address text, phone_number text)",
                    "scouts",
                    new String[] {"rank_id"});
//            dropColumn(db,
//                    "CREATE TABLE meetings (_id INTEGER PRIMARY KEY AUTOINCREMENT, den_id INTEGER REFERENCES dens(_id), meeting_date DATETIME, notes TEXT, reported DATETIME);",
//                    "meetings",
//                    new String[] {"rank_id"});
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void copyDatabase() throws IOException {
        final InputStream is = CubTracker.getAppContext().getAssets().open(Constants.DATABASE_NAME);
        // Path to the just created empty db
        final String outFileName = Constants.DATABASE_PATH + "/" + Constants.DATABASE_NAME;

        // Open the empty db as the output stream
        new File(Constants.DATABASE_PATH).mkdirs();
        final OutputStream os = new FileOutputStream(outFileName);

        // transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[16384];
        int length;
        while ((length = is.read(buffer)) > 0) {
            os.write(buffer, 0, length);
        }

        // Close the streams
        os.flush();
        os.close();
        is.close();
        Util.setIntPref("DB_VERSION", Constants.DATABASE_VERSION);
    }

    protected void updateGroupCounts(SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("select items._id, (select count(*) FROM groups WHERE item_id = items._id) as gc FROM items where gc > 1", null);
        SQLiteStatement ps = db.compileStatement("UPDATE items SET group_count = ? WHERE _id = ?");
        cursor.moveToFirst();
        int idx_gc = cursor.getColumnIndex("gc");
        int idx_id = cursor.getColumnIndex("_id");
        while (!cursor.isAfterLast()) {
            ps.bindLong(1, cursor.getLong(idx_gc));
            ps.bindLong(2, cursor.getLong(idx_id));
            ps.execute();
            cursor.moveToNext();
        }
        ps.close();
        cursor.close();
    }

    protected void updatePartCounts(SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("select groups._id, (select count(*) FROM parts WHERE group_id = groups._id) as pc FROM groups where pc > 1", null);
        SQLiteStatement ps = db.compileStatement("UPDATE groups SET part_count = ? WHERE _id = ?");
        cursor.moveToFirst();
        int idx_pc = cursor.getColumnIndex("pc");
        int idx_id = cursor.getColumnIndex("_id");
        while (!cursor.isAfterLast()) {
            ps.bindLong(1, cursor.getLong(idx_pc));
            ps.bindLong(2, cursor.getLong(idx_id));
            ps.execute();
            cursor.moveToNext();
        }
        ps.close();
        cursor.close();
    }

    /**
     * Close the database connections and clear any cached DAOs.
     */
    @Override
    public void close() {
        super.close();
    }

    protected void createTables(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS ranks (_id INTEGER PRIMARY KEY, name TEXT NOT NULL);");
        db.execSQL("CREATE TABLE IF NOT EXISTS scouts (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, rank_id INTEGER, contacts TEXT, photo BLOB, FOREIGN KEY (rank_id) REFERENCES ranks(_id))");
        db.execSQL("CREATE TABLE IF NOT EXISTS items (_id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT, rank_id INTEGER, number TEXT, title TEXT, notes TEXT, image BLOB, FOREIGN KEY (rank_id) REFERENCES ranks(_id))");
        db.execSQL("CREATE TABLE IF NOT EXISTS groups (_id INTEGER PRIMARY KEY AUTOINCREMENT, item_id INTEGER NOT NULL, name TEXT, number TEXT, qty_required INTEGER , notes TEXT, FOREIGN KEY (item_id) REFERENCES items(_id))");
        db.execSQL("CREATE TABLE IF NOT EXISTS parts (_id INTEGER PRIMARY KEY AUTOINCREMENT, group_id INTEGER NOT NULL, number TEXT, description TEXT, notes TEXT, FOREIGN KEY (group_id) REFERENCES groups(_id))");
        db.execSQL("CREATE TABLE IF NOT EXISTS completions (_id INTEGER PRIMARY KEY AUTOINCREMENT, scout_id INTEGER NOT NULL, part_id INTEGER NOT NULL, timestamp DATETIME, reported DATETIME, FOREIGN KEY (scout_id) REFERENCES scouts(_id), FOREIGN KEY (part_id) REFERENCES parts(_id))");
    }

    private void dropColumn(SQLiteDatabase db,
//                            ConnectionSource connectionSource,
                            String createTableCmd,
                            String tableName,
                            String[] colsToRemove) throws java.sql.SQLException {

        List<String> updatedTableColumns = getTableColumns(db, tableName);
        // Remove the columns we don't want anymore from the table's list of columns
        updatedTableColumns.removeAll(Arrays.asList(colsToRemove));

        String columnsSeperated = TextUtils.join(",", updatedTableColumns);

        db.execSQL("ALTER TABLE " + tableName + " RENAME TO " + tableName + "_old;");

        // Creating the table on its new format (no redundant columns)
        db.execSQL(createTableCmd);

        // Populating the table with the data
        db.execSQL("INSERT INTO " + tableName + "(" + columnsSeperated + ") SELECT "
                + columnsSeperated + " FROM " + tableName + "_old;");
        db.execSQL("DROP TABLE " + tableName + "_old;");
    }

    public List<String> getTableColumns(SQLiteDatabase db, String tableName) {
        List<String> columns = new ArrayList<String>();
        String cmd = "pragma table_info(" + tableName + ");";
        Cursor cur = db.rawQuery(cmd, null);

        while (cur.moveToNext()) {
            columns.add(cur.getString(cur.getColumnIndex("name")));
        }
        cur.close();

        return columns;
    }
}
