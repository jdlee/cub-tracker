package com.steeplesoft.cubtracker.android.adapter;

import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.steeplesoft.cubtracker.android.Constants;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SuppressWarnings("deprecation")
public class SelectableListViewAdapter extends BaseAdapter {
    private static final int FONT_SIZE_GROUP_ITEM = 20;
    private static final int FONT_SIZE_GROUP_HEADER = 26;
    private List<ListItem> items = new ArrayList<ListItem>();
    private Set<Long> selected = new HashSet<Long>();
    private View.OnLongClickListener longClickListener;
    private View.OnClickListener onClickCallback;

    public SelectableListViewAdapter(List<ListItem> items) {
        this(items, null);
    }

    public SelectableListViewAdapter(List<ListItem> items, View.OnClickListener callback) {
        super();
        this.items = items;
        onClickCallback = callback;
    }

    public SelectableListViewAdapter(Cursor cursor, String id, String name) {
        this(cursor, id, name, null);
    }

    public SelectableListViewAdapter(Cursor cursor, String id, String name, View.OnClickListener callback) {
        onClickCallback = callback;
        cursor.moveToFirst();
        // Don't attempt anything on the cursor if there's no data
        if (!cursor.isAfterLast()) {
            int idCol = cursor.getColumnIndex(id);
            int nameCol = cursor.getColumnIndex(name);
            while (!cursor.isAfterLast()) {
                items.add(new ListItem(cursor.getLong(idCol), cursor.getString(nameCol), null, false));
                cursor.moveToNext();
            }
        }
        cursor.close();
    }

    public View.OnLongClickListener getLongClickListener() {
        return longClickListener;
    }

    public void setLongClickListener(View.OnLongClickListener longClickListener) {
        this.longClickListener = longClickListener;
    }

    public long[] getSelected() {
        long[] array = new long[selected.size()];
        int index = 0;
        for (long l : selected) {
            array[index++] = l;
        }
        return array;
    }

    public void setSelected(long[] selected) {
        this.selected.clear();
        for (long l : selected) {
            this.selected.add(l);
        }
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position).tag;
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        final ListItem listItem = items.get(position);
        if (convertView == null) {
            TextView tv = new TextView(parent.getContext()); //inflater.inflate(layoutId, null);
            tv.setOnClickListener(new View.OnClickListener() {
                public void onClick(final View v) {
                    final Long id = (Long)v.getTag();
                    boolean isSelected = selected.contains(id);
                    if (isSelected) {
                        selected.remove(id);
                        v.setBackgroundColor(Constants.COLOR_DEFAULT);
//                        v.setBackgroundDrawable(null);
                    } else {
                        selected.add(id);
                        v.setBackgroundColor(Constants.COLOR_SELECTED);
                    }

                    if (onClickCallback != null) {
                        onClickCallback.onClick(v);
                    }
                }
            });
            if (longClickListener != null) {
                tv.setOnLongClickListener(longClickListener);
            }
            tv.setPadding(0, 5, 0, 5);
            convertView = tv;
        }

        ((TextView)convertView).setText(listItem.name);
        convertView.setTag(listItem.id);
        convertView.setBackgroundColor(selected.contains(listItem.id) ? Constants.COLOR_SELECTED : Color.WHITE);

        if (listItem.groupHeader) {
            ((TextView)convertView).setTextSize(FONT_SIZE_GROUP_HEADER);
            convertView.setEnabled(false);
            ((TextView)convertView).setTypeface(null, Typeface.BOLD_ITALIC);
        } else {
            ((TextView)convertView).setTextSize(FONT_SIZE_GROUP_ITEM);
            convertView.setEnabled(true);
            ((TextView)convertView).setTypeface(null, Typeface.NORMAL);
        }
        return convertView;
    }

    public static class ListItem {
        public long id;
        public String name;
        public boolean groupHeader;
        public Object tag;

        public ListItem(long id, String name, Object tag, boolean groupHeader) {
            this.id = id;
            this.name = name;
            this.tag = tag;
            this.groupHeader = groupHeader;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
