package com.steeplesoft.cubtracker.android.adapter;

import android.util.SparseArray;
import android.view.View;

public class ViewHolder {
    private SparseArray<View> views = new SparseArray<View>();
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public View getView(Integer id) {
        return views.get(id);
    }

    public void addView(Integer id, View view) {
        views.put(id, view);
    }
}