package com.steeplesoft.cubtracker.android.utils.backup;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import com.steeplesoft.cubtracker.android.Constants;

/**
* Created by jdlee on 2/13/14.
*/
class BackupHandlerThread extends HandlerThread {
    protected static final String LOCAL_PATH = Constants.DATABASE_PATH + "/" + Constants.DATABASE_NAME;
    protected Handler handler = null;

    public BackupHandlerThread() {
        super("BackupHandlerThread");
        start();
        handler = new Handler(getLooper());
    }

    public void execute(final Runnable r, final boolean wait) {
        handler.post(r);
        if (wait) {
            try {
                Log.i(Constants.LOG_TAG, "Waiting...");
                wait();
                Log.i(Constants.LOG_TAG, "Finished waiting...");
            } catch (InterruptedException e) {
                Log.w(Constants.LOG_TAG, "wait was interrupted");
            }
        }
    }
}
