package com.steeplesoft.cubtracker.android.utils.backup;

import com.steeplesoft.cubtracker.android.model.ItemCompletion;
import com.steeplesoft.cubtracker.android.model.Scout;

import java.util.List;

/**
 * Created by jdlee on 8/21/14.
 */
public class RankCheck {
    final private Scout scout;
    final List<ItemCompletion> itemsCompleted;
    private double percentComplete;

    public RankCheck(Scout scout, List<ItemCompletion> itemsCompleted) {
        this.scout = scout;
        this.itemsCompleted = itemsCompleted;

        analyze();
    }

    public double getPercentComplete() {
        return percentComplete;
    }

    private void analyze() {
        int rank = (int)scout.getDen().getRank().getId();
        int required = 0;
        int elective = 0;

        for (ItemCompletion ic : itemsCompleted) {
            String type = ic.getItem().getType();
            switch (type) {
                case "a" :
                case "B":
                    required++;
                    break;
                case "e":
                    elective++;
            }
        }

        // All ranks require 7 completions:
        // * Tiger - Bear: 6 required + 1 elective
        // Webelos:        5 required + 2 elective
        // Arrow of Light: 4 required + 3 elective
        // Webelos 2     : 7 badges

//        // TODO: This test is dumb. It would allow 5 required + 2 elective. Make it smarter
//        percentComplete = itemsCompleted.size() / 7.0;

        if ((rank == RankEnum.TIGER.rank) ||
                (rank == RankEnum.WOLF.rank) ||
                (rank == RankEnum.BEAR.rank)) {
            percentComplete = (required +  ((elective >= 1) ? 1 : elective)) / 7.0;
        } else if (rank == RankEnum.WEBELOS.rank) {
            percentComplete = (required +  ((elective >= 2) ? 2 : elective)) / 7.0;
        } else if (rank == RankEnum.ARROW_OF_LIGHT.rank) {
            percentComplete = (required +  ((elective >= 3) ? 3 : elective)) / 7.0;
        } else if (rank == RankEnum.WEB2.rank) {
            percentComplete = required / 7.0;

        }
        /*
        if ((rank == RankEnum.TIGER.rank) ||
                (rank == RankEnum.WOLF.rank) ||
                (rank == RankEnum.BEAR.rank) ||
                (rank == RankEnum.WEBELOS.rank) ||
                (rank == RankEnum.ARROW_OF_LIGHT.rank)) {
            percentComplete = itemsCompleted.size() / 7.0;
        } else if (rank == RankEnum.WEB2.rank) {
            percentComplete = itemsCompleted.size() / 7.0;
        }
        */
        /*
        if (rank == RankEnum.TIGER.rank) {
            percentComplete = itemsCompleted.size() / 5.0;
        } else if (rank == RankEnum.WOLF.rank) {
            percentComplete = itemsCompleted.size() / 12.0;
        } else if (rank == RankEnum.BEAR.rank) {
            analyzeBear();
        } else if (rank == RankEnum.WEB1.rank) {
            percentComplete = itemsCompleted.size() / 8.0;
        } else if (rank == RankEnum.WEB2.rank) {
            percentComplete = itemsCompleted.size() / 7.0;
        }*/
    }

    private void analyzeBear() {
        int group1 = 0;
        int group2 = 0;
        int group3 = 0;
        int group4 = 0;

        for (ItemCompletion ic : itemsCompleted) {
            int itemId = Integer.parseInt(ic.getItem().getNumber());
            if (itemId >= 1 && itemId <= 2) {
                group1++;
            } else if (itemId >= 3 && itemId <= 7) {
                    group2++;
            } else if (itemId >= 8 && itemId <= 13) {
                    group3++;
            } else if (itemId >= 14 && itemId <= 24) {
                    group4++;
            }
        }

        percentComplete = ((group1 / 1.0) + (group2 / 3.0) + (group3 / 4.0) + (group4 / 4.0)) / 4.0;
    }
}
