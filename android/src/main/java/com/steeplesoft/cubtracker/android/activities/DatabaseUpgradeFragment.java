package com.steeplesoft.cubtracker.android.activities;

import android.app.Activity;
import android.app.Fragment;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;

import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.CubTracker;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This Fragment manages a single background task and retains
 * itself across configuration changes.
 */
public class DatabaseUpgradeFragment extends Fragment {

    /**
     * Callback interface through which the fragment will report the
     * task's progress and results back to the Activity.
     */
    interface UpgradeDatabaseCallbacks {
        void setProgressBarMax(int max);
        void resetProgressBar();
        void onPreExecute();
        void onProgressUpdate(int percent);
        void onCancelled();
        void onPostExecute();
    }

    private UpgradeDatabaseCallbacks mCallbacks;
    private UpgradeTask task;

    /**
     * Hold a reference to the parent Activity so we can report the
     * task's current progress and results. The Android framework
     * will pass us a reference to the newly created Activity after
     * each configuration change.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (UpgradeDatabaseCallbacks) activity;
    }

    /**
     * This method will only be called once when the retained
     * Fragment is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Retain this fragment across configuration changes.
        setRetainInstance(true);

        // Create and execute the background task.
        try {
            createTask();
            task.execute();
            task = null;
        } catch (java.sql.SQLException e) {
            Log.d(Constants.LOG_TAG, e.getMessage());
        }
    }

    public UpgradeTask createTask() throws java.sql.SQLException {
        if (task == null) {
            task = new UpgradeTask();
            final SQLiteDatabase db = DatabaseAccess.getInstance().getWritableDatabase();
            final int dbVersion = DatabaseAccess.getInstance().getDatabaseVersion();

            if (dbVersion < Constants.VERSION_2_0_4) {
                task.addPart(new UpgradePart("2_0_4"));
            }
            if (dbVersion < Constants.VERSION_2_2) {
                task.addPart(new UpgradePart("2_2"));
            }
            if (dbVersion < Constants.VERSION_2_4) {
                task.addPart(new UpgradePart("2_4"));
            }
            if (dbVersion < Constants.VERSION_2_7) {
                UpgradePart part = new UpgradePart("2_7");
                part.additionalSteps.add(new Runnable() {
                    @Override
                    public void run() {
                        updateGroupCounts(db);
                        updatePartCounts(db);
                    }
                });
                task.addPart(part);
            }
            if (dbVersion < Constants.VERSION_3_0) {
                UpgradePart part = new UpgradePart("3_0");
                part.additionalSteps.add(new Runnable() {
                    @Override
                    public void run() {
                        performV3Upgrade(db);
                    }
                });

                task.addPart(part);
            }
            if (dbVersion < Constants.VERSION_4_0) {
                try {
                    dropColumn(db,
                            "CREATE TABLE items (_id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT, rank_id INTEGER, number TEXT, title TEXT, notes TEXT, image TEXT, group_count int, FOREIGN KEY (rank_id) REFERENCES ranks(_id));",
                            "items",
                            new String[]{"image"});
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                UpgradePart part = new UpgradePart("4_0");
                part.additionalSteps.add(new Runnable() {
                    @Override
                    public void run() {
                        updateGroupCounts(db);
                        updatePartCounts(db);
                    }
                });
                task.addPart(part);
            }
            if (dbVersion < Constants.VERSION_4_0_1) {
                task.addPart(new UpgradePart("4_0_1"));
            }

            if (!task.hasTasks()) {
                DatabaseAccess.getInstance().getWritableDatabase().execSQL("UPDATE settings SET setting_value = " +
                        Constants.DATABASE_VERSION + " WHERE setting_key = 'db_version'");
            }
        }

        return task;
    }

    private static void performV3Upgrade(SQLiteDatabase db) {
        final String sql = "INSERT INTO scouts (name, rank_id, email_address, phone_number, contacts, photo) VALUES (?,?,?,?,?,?);";
        final String createDenSql = "INSERT INTO dens (name, rank_id) VALUES (?,?);";

        Map<Integer, Long> dens = new HashMap<Integer, Long>();
        Cursor c = db.rawQuery("SELECT _id, name, rank_id, email_address, phone_number, contacts FROM scouts ORDER by rank_id", null);
        c.moveToFirst();
        int rankIdx = c.getColumnIndex("rank_id");
        int idIdx = c.getColumnIndex("_id");
        while (!c.isAfterLast()) {
            int rank = c.getInt(rankIdx);
            long id = c.getLong(idIdx);
            Long denId = dens.get(rank);
            if (denId == null) {
                db.execSQL(createDenSql, new Object[] { "Den " + rank, rank });

                Cursor cursor = db.rawQuery("SELECT last_insert_rowid()", null);
                cursor.moveToFirst();
                denId = cursor.getLong(0);
                cursor.close();
                dens.put(rank, denId);
                db.execSQL("UPDATE meetings SET den_id = ? WHERE rank_id = ?", new Object[] { denId, rank });
            }
            db.execSQL("UPDATE scouts SET den_id = ? WHERE _id = ?", new Object[] { denId, id });
            c.moveToNext();
        }
        c.close();
        try {
            dropColumn(db,
                    "CREATE TABLE scouts (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, den_id INTEGER REFERENCES dens(_id), contacts TEXT, photo BLOB, email_address text, phone_number text)",
                    "scouts",
                    new String[] {"rank_id"});
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    private static void dropColumn(SQLiteDatabase db,
                            String createTableCmd,
                            String tableName,
                            String[] colsToRemove) throws java.sql.SQLException {

        List<String> updatedTableColumns = getTableColumns(db, tableName);
        // Remove the columns we don't want anymore from the table's list of columns
        updatedTableColumns.removeAll(Arrays.asList(colsToRemove));

        String columnsSeperated = TextUtils.join(",", updatedTableColumns);

        db.execSQL("ALTER TABLE " + tableName + " RENAME TO " + tableName + "_old;");

        // Creating the table on its new format (no redundant columns)
        db.execSQL(createTableCmd);

        // Populating the table with the data
        db.execSQL("INSERT INTO " + tableName + "(" + columnsSeperated + ") SELECT "
                + columnsSeperated + " FROM " + tableName + "_old;");
        db.execSQL("DROP TABLE " + tableName + "_old;");
    }

    public static List<String> getTableColumns(SQLiteDatabase db, String tableName) {
        List<String> columns = new ArrayList<String>();
        String cmd = "pragma table_info(" + tableName + ");";
        Cursor cur = db.rawQuery(cmd, null);

        while (cur.moveToNext()) {
            columns.add(cur.getString(cur.getColumnIndex("name")));
        }
        cur.close();

        return columns;
    }

    protected static void updateGroupCounts(SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("select items._id, (select count(*) FROM groups WHERE item_id = items._id) as gc FROM items where gc > 1", null);
        SQLiteStatement ps = db.compileStatement("UPDATE items SET group_count = ? WHERE _id = ?");
        cursor.moveToFirst();
        int idx_gc = cursor.getColumnIndex("gc");
        int idx_id = cursor.getColumnIndex("_id");
        while (!cursor.isAfterLast()) {
            ps.bindLong(1, cursor.getLong(idx_gc));
            ps.bindLong(2, cursor.getLong(idx_id));
            ps.execute();
            cursor.moveToNext();
        }
        ps.close();
        cursor.close();
    }

    protected static void updatePartCounts(SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("select groups._id, (select count(*) FROM parts WHERE group_id = groups._id) as pc FROM groups where pc > 1", null);
        SQLiteStatement ps = db.compileStatement("UPDATE groups SET part_count = ? WHERE _id = ?");
        cursor.moveToFirst();
        int idx_pc = cursor.getColumnIndex("pc");
        int idx_id = cursor.getColumnIndex("_id");
        while (!cursor.isAfterLast()) {
            ps.bindLong(1, cursor.getLong(idx_pc));
            ps.bindLong(2, cursor.getLong(idx_id));
            ps.execute();
            cursor.moveToNext();
        }
        ps.close();
        cursor.close();
    }


    /**
     * Set the callback to null so we don't accidentally leak the
     * Activity instance.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    private static class UpgradePart {
        String version;
        List<Runnable> additionalSteps = new ArrayList<Runnable>();

        public UpgradePart(String version) {
            this.version = version;
        }
    }

    public class UpgradeTask extends AsyncTask<Void, Integer, Void> {
        private List<UpgradePart> parts = new ArrayList<>();

        public void addPart(UpgradePart part) {
            parts.add(part);
        }

        public boolean hasTasks() {
            return !parts.isEmpty();
        }

        @Override
        protected void onPreExecute() {
            if (mCallbacks != null) {
                mCallbacks.onPreExecute();
            }
        }

        /**
         * Note that we do NOT call the callback object's methods
         * directly from the background thread, as this could result
         * in a race condition.
         */
        @Override
        protected Void doInBackground(Void... ignore) {
            for (UpgradePart part : parts) {
                Log.d(Constants.LOG_TAG, "Processing upgrade for " + part.version);
                if (mCallbacks != null) {
                    mCallbacks.resetProgressBar();

                    try {
                        final List<String> lines = new ArrayList<String>();
                        final SQLiteDatabase db = DatabaseAccess.getInstance().getWritableDatabase();
                        final InputStream is = CubTracker.getAppContext().getAssets().open("db" + part.version + ".sql");
                        final BufferedReader br = new BufferedReader(new InputStreamReader(is));
                        String line = br.readLine();
                        while (line != null) {
                            if (!"".equals(line.trim()) && !line.trim().startsWith("#") && !line.trim().startsWith("--")) {
                                Log.d(Constants.LOG_TAG, line);
                                lines.add(line);
                            }
                            line = br.readLine();
                        }
                        try {
                            br.close();
                            is.close(); // unnecessary?
                        } catch (Exception e) {
                        }

                        int count = 0;
                        int total = lines.size();
                        mCallbacks.setProgressBarMax(total);

                        for (String sql : lines) {
                            count++;
                            db.execSQL(sql);
                            publishProgress(count);
                        }
                        for (Runnable runnable : part.additionalSteps) {
                            runnable.run();
                        }
                    } catch (IOException ioe) {
                        Log.d(Constants.LOG_TAG, ioe.getMessage());
                    }

                    DatabaseAccess.getInstance().getWritableDatabase().execSQL("UPDATE settings SET setting_value = " +
                            Constants.DATABASE_VERSION + " WHERE setting_key = 'db_version'");
                }
            }
            try {
                Util.displayMessage("The database has been upgraded.");
            } catch (Exception e) {

            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... percent) {
            if (mCallbacks != null) {
                mCallbacks.onProgressUpdate(percent[0]);
            }
        }

        @Override
        protected void onCancelled() {
            if (mCallbacks != null) {
                mCallbacks.onCancelled();
            }
        }

        @Override
        protected void onPostExecute(Void ignore) {
            if (mCallbacks != null) {
                mCallbacks.onPostExecute();
            }
        }
    }
}
