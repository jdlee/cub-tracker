package com.steeplesoft.cubtracker.android.activities;

import android.app.Fragment;
import android.os.Bundle;

import com.steeplesoft.cubtracker.android.fragments.ExpandableListFragment;

/**
 * Created by jdlee on 8/1/14.
 */
public abstract class ExpandableListActivity extends DrawerLayoutActivity {
//    private static Map<String, long[]> expandedMap = new HashMap<String, long[]>();

    protected ExpandableListActivity(Class<? extends Fragment> initialFragment) {
        super(initialFragment);
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
//        expandedMap.put(getClass().getSimpleName(), getExpandableListFragment().getState());
//        bundle.putLongArray("expanded", expanded);
        super.onSaveInstanceState(bundle);
    }

    @Override
    protected void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
//        long[] expanded = expandedMap.get(getClass().getSimpleName());
//        if (expanded != null) {
//            getExpandableListFragment().restoreExpandedState(expanded);
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        long[] expanded = expandedMap.get(getClass().getSimpleName());
//        if (expanded != null) {
//            getExpandableListFragment().restoreExpandedState(expanded);
//        }
    }

    @Override
    protected void onStop() {
        super.onStop();
//        expandedMap.put(getClass().getSimpleName(), getExpandableListFragment().getState());
    }

    protected ExpandableListFragment getExpandableListFragment() {
        return (ExpandableListFragment)getFragmentManager().findFragmentByTag("initial");
    }
}
