package com.steeplesoft.cubtracker.android.fragments;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.activities.ViewItemActivity;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Item;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.sql.SQLException;

public class BrowseBadgesFragment extends ListFragment {
    private Item[] items;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            items = DatabaseAccess.getInstance().getItemsForType("B").toArray(new Item[]{});
            ArrayAdapter<Item> adapter = new ArrayAdapter<Item>(getActivity(), R.layout.list_item, items);

            setListAdapter(adapter);
            getListView().setTag("badges");
            getListView().setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(view.getContext(), ViewItemActivity.class);
                    intent.putExtra(IntentConstants.ITEM_ID, items[position].getId());
                    startActivity(intent);
                }

            });
        } catch (SQLException e) {
            Log.e(Constants.LOG_TAG, e.getLocalizedMessage());
        }
    }
}
