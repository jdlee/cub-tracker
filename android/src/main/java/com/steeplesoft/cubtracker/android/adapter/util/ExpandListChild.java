package com.steeplesoft.cubtracker.android.adapter.util;

public interface ExpandListChild {
    public long getId();

    public String getName();

    public void setName(String Name);

    public Object getTag();

    public void setTag(Object Tag);
}
