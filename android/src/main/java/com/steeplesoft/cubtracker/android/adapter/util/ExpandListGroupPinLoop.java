package com.steeplesoft.cubtracker.android.adapter.util;

import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Item;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ExpandListGroupPinLoop implements ExpandListGroup {
    private String type;
    private String name;
    private List<ExpandListChild> items;

    public ExpandListGroupPinLoop(String type, String name) {
        this.type = type;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public List<ExpandListChild> getItems() {
        if (items == null) {
            try {
                items = new ArrayList<ExpandListChild>();
                final DatabaseAccess db = DatabaseAccess.getInstance();
                for (Item item : db.getItemsForType(type)) {
                    items.add(new ExpandListChildItem(item));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return items;
    }

    @Override
    public void setItems(List<ExpandListChild> items) {
        this.items = items;
    }
}
