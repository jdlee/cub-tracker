package com.steeplesoft.cubtracker.android.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.provider.MediaStore;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.adapter.SelectableListViewAdapter;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.model.Rank;
import com.steeplesoft.cubtracker.android.model.Scout;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressLint("InlinedApi")
public class ScoutFormActivity extends Activity {
    private static final int REQUEST_CODE_TAKE_PICTURE = 100;
    private static final int CONTACT_PICKER_RESULT = 1001;
    private Bitmap photo;
    private Set<String> contactIds = new HashSet<String>();
//    private long selectedRank;
    private Scout scout;
    private Long scoutId;
    private Den den;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(Constants.LOG_TAG, "Creating activity: " + getClass().getSimpleName());

        setContentView(R.layout.activity_add_scout);
        scoutId = getIntent().getLongExtra(IntentConstants.SCOUT_ID, -1);
        den = (Den)getIntent().getSerializableExtra(IntentConstants.DEN);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (scoutId > -1) {
            scout = DatabaseAccess.getInstance().getScout(scoutId);
            if ((den == null) && (scout != null)) {
                den = scout.getDen();
            }
            final String contacts = scout.getContactIds();
            if (contacts != null) {
                contactIds = new HashSet<String>(Arrays.asList(contacts.split(",")));
            }
            EditText et = (EditText) findViewById(R.id.scoutName);
            et.setText(scout.getName());
            setTitle("Edit Scout"); // i18n
            Util.setText(findViewById(R.id.scoutEmail), scout.getEmailAddress());
            Util.setText(findViewById(R.id.scoutPhone), scout.getPhoneNumber());
        }

        preparePhotoButton();
        prepareContactsButton();
        prepareSpinner();
    }

    protected void prepareContactsButton() {
        findViewById(R.id.linkedContact).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,  Contacts.CONTENT_URI);
                startActivityForResult(contactPickerIntent, CONTACT_PICKER_RESULT);
            }
        });

        final OnClickListener buttonPressed = new OnClickListener() {
            public void onClick(View v) {
                onButtonPressed(v);
            }
        };

        findViewById(R.id.saveButton).setOnClickListener(buttonPressed);
        findViewById(R.id.cancelButton).setOnClickListener(buttonPressed);

        buildContactList();
    }

    protected void buildContactList() {
        LinearLayout linearLayout =  (LinearLayout)findViewById(R.id.contactsContainer);
        linearLayout.removeAllViews();

        if (contactIds != null) {
            for (String id : contactIds) {
                addContactToList(linearLayout, id);
            }
        }

    }

    /**
     * @param id
     * @return
     * @throws NumberFormatException
     */
    protected void addContactToList(LinearLayout parent, final String id) throws NumberFormatException {
        if (!id.isEmpty()) {
            long lid = Long.parseLong(id);
            Map<String, String> contact = Util.getContact(lid);

            LinearLayout layout = new LinearLayout(this);
            layout.setId(1000000000 + (int)lid); // There must be a better way :P
            layout.setOrientation(LinearLayout.HORIZONTAL);

            TextView valueTV = new TextView(this);
            valueTV.setText(contact.get(ContactsContract.CommonDataKinds.Identity.DISPLAY_NAME));
            valueTV.setTag(contact.get(ContactsContract.CommonDataKinds.Identity._ID));
            valueTV.setId((int) lid);
            valueTV.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            valueTV.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

            ImageView imageView = new ImageView(this);
            imageView.setImageResource(R.drawable.redx);
            imageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    Util.showConfirmationDialog(view.getContext(), "Are you sure you wish to delete this contact?",
                            new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == DialogInterface.BUTTON_POSITIVE) {
                                contactIds.remove(id);
                                buildContactList();
                            }
                        }
                    });
                }
            });

            layout.addView(imageView);
            layout.addView(valueTV);
            parent.addView(layout);
        }
    }

    protected void preparePhotoButton() {
        ImageView scoutImage = (ImageView)findViewById(R.id.scoutPhoto);
        if (scout != null) {
            if (scout.getPhoto() != null) {
                final Bitmap bitmap = BitmapFactory.decodeByteArray(scout.getPhoto(), 0, scout.getPhoto().length);
                this.photo = bitmap;
                Util.scaleImage(bitmap, scoutImage, 250.0f / bitmap.getHeight());
            }
        }
        scoutImage.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Intent pickIntent = new Intent();
                pickIntent.setType("image/*");
                pickIntent.setAction(Intent.ACTION_GET_CONTENT);

                Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                String pickTitle = "Select or Take a New Picture"; // Or get from strings.xml
                Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] { takePhotoIntent });

                startActivityForResult(chooserIntent, REQUEST_CODE_TAKE_PICTURE);
            }
        });
    }

    protected void prepareSpinner() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        DatabaseAccess db = DatabaseAccess.getInstance();
        final Button saveButton = (Button) findViewById(R.id.saveButton);

        List<SelectableListViewAdapter.ListItem> listItems = new ArrayList<SelectableListViewAdapter.ListItem>();
        for (Den d : db.getDens()) {
            listItems.add(new SelectableListViewAdapter.ListItem(d.getId(), d.getName(), d, false));
        }
//        final SelectableListViewAdapter adapter = new SelectableListViewAdapter(listItems);
        final ArrayAdapter adapter = new ArrayAdapter<SelectableListViewAdapter.ListItem>(this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                listItems.toArray(new SelectableListViewAdapter.ListItem[]{}));

        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                SelectableListViewAdapter.ListItem listItem = ((SelectableListViewAdapter.ListItem)parent.getItemAtPosition(pos));
                den = (Den)listItem.tag;
                saveButton.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                //
            }
        });

        if (den != null) {
            saveButton.setEnabled(true);
            for (int i = 0; i < listItems.size(); i++) {
                SelectableListViewAdapter.ListItem li = listItems.get(i);
                if (li.tag.equals(den)) {
                    spinner.setSelection(i, true);
                }
            }
        } else if (scout != null) {
            saveButton.setEnabled(true);
            spinner.setSelection((int)scout.getDen().getId(), true);
        } else {
            saveButton.setEnabled(false);
        }
    }

    public void onButtonPressed(View view) {
        switch (view.getId()) {
            case R.id.saveButton:
                String contacts = null;
                if ((contactIds != null) && (!contactIds.isEmpty())) {
                    StringBuilder sb = new StringBuilder();
                    String sep = "";
                    for (String contact : contactIds) {
                        sb.append(sep).append(contact);
                        sep = ",";
                    }
                    contacts = sb.toString();
                }

                try {
                    final DatabaseAccess db = DatabaseAccess.getInstance();
                    if (scout == null) {
                        db.createScout(Util.getText(findViewById(R.id.scoutName)),
                                den,
                                Util.getText(findViewById(R.id.scoutEmail)),
                                Util.getText(findViewById(R.id.scoutPhone)),
                                getImageBytes(),
                                contacts);
                    } else {
                        db.updateScout(scout.getId(), Util.getText(findViewById(R.id.scoutName)),
                                den,
                                Util.getText(findViewById(R.id.scoutEmail)),
                                Util.getText(findViewById(R.id.scoutPhone)),
                                getImageBytes(), contacts);
                    }
                    Intent intent = new Intent();
                    intent.putExtra(IntentConstants.SCOUT_ID, scoutId);
                    setResult(RESULT_OK, intent);
                    finish();
                } catch (OutOfMemoryError oom) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(R.string.oom_message).setTitle(R.string.error_dialog_title);

                    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                break;
            case R.id.cancelButton:
                finish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode) {
            case REQUEST_CODE_TAKE_PICTURE :
                if (resultCode == RESULT_OK) {
                    ImageView iv = (ImageView) findViewById(R.id.scoutPhoto);
                    Uri uri = intent.getData();
                    if (uri != null) {
                        try {
                            photo = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri));
                        } catch (OutOfMemoryError oom) {
                            Util.displayMessage("The image was too large to display. Please take or select a smaller one",
                                    Toast.LENGTH_LONG);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    } else {
                        photo = (Bitmap) intent.getExtras().get(IntentConstants.DATA);
                    }
                    if (photo != null) {
                        iv.setImageBitmap(photo);
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    // User cancelled the image capture
                } else {
                    // Image capture failed, advise user
                }
                break;
            case CONTACT_PICKER_RESULT:
                if (resultCode == RESULT_OK) {
                    Cursor cursor = null;
                    try {

                        Uri result = intent.getData();
                        String id = result.getLastPathSegment();
                        addContactToList((LinearLayout)findViewById(R.id.contactsContainer), id);
                        contactIds.add(id);
                        cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                CONTACTS_SUMMARY_PROJECTION,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                                new String[] { id },
                                null);
                        if (cursor.moveToFirst()) {
                            StringBuilder sb = new StringBuilder();
                            for (int i = 0; i< cursor.getColumnCount(); i++) {
                                sb.append(cursor.getString(i)).append("\n");
                            }
                        }
                    } catch (Exception e) {
                        Log.e(Constants.LOG_TAG, "Failed to get email data", e);
                    } finally {
                        if (cursor != null) {
                            cursor.close();
                        }
                    }
                }
                break;
        }
    }

    private static final String[] CONTACTS_SUMMARY_PROJECTION = new String[] {
        ContactsContract.CommonDataKinds.Phone.LOOKUP_KEY,
        ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
        ContactsContract.CommonDataKinds.Identity.IDENTITY,
        ContactsContract.CommonDataKinds.Identity.IS_PRIMARY,
        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
        ContactsContract.CommonDataKinds.Email.ADDRESS
    };

    private byte[] getImageBytes() {
        if (photo != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
            return stream.toByteArray();
        } else {
            return null;
        }
    }
}
