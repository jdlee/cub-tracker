package com.steeplesoft.cubtracker.android;

import android.annotation.SuppressLint;

import com.steeplesoft.cubtracker.R;

public class Constants {
    public static String LOG_TAG = "***** CUBTRACKER";

    public static final int VERSION_1_0_0 = 1;
    public static final int VERSION_1_0_1 = 2;
    public static final int VERSION_1_0_2 = 3;
    public static final int VERSION_1_0_3 = 4;
    public static final int VERSION_1_0_4 = 5;
    public static final int VERSION_1_1_0 = 6;
    public static final int VERSION_2_0_0 = 7;
    public static final int VERSION_2_0_1 = 8;
    public static final int VERSION_2_0_2 = 9;
    public static final int VERSION_2_0_3 = 10;
    public static final int VERSION_2_0_3_1 = 11;
    public static final int VERSION_2_0_3_2 = 12;
    public static final int VERSION_2_0_3_3 = 13;
    public static final int VERSION_2_0_3_4 = 14;
    public static final int VERSION_2_0_3_5 = 15;
    public static final int VERSION_2_0_4 = 16;
    public static final int VERSION_2_1 = 17;
    public static final int VERSION_2_2 = 18;
    public static final int VERSION_2_3 = 19;
    public static final int VERSION_2_4 = 20;
    public static final int VERSION_2_4_1 = 21;
    public static final int VERSION_2_4_2 = 22;
    public static final int VERSION_2_5 = 23;
    public static final int VERSION_2_6 = 24;
    public static final int VERSION_2_6_1 = 25;
    public static final int VERSION_2_6_2 = 26;
    public static final int VERSION_2_7 = 27;
    public static final int VERSION_3_0 = 28;
    public static final int VERSION_3_0_1 = 29;
    public static final int VERSION_3_0_2 = 30;
    public static final int VERSION_3_0_3 = 31;
    public static final int VERSION_3_0_4 = 32;
    public static final int VERSION_4_0 = 33;
    public static final int VERSION_4_0_1 = 34;
    public static final int VERSION_4_0_2 = 35;
    public static final int VERSION_4_0_3 = 36;
    public static final int VERSION_4_0_5 = 37;
    public static final int VERSION_4_0_6 = 38;

    @SuppressLint("SdCardPath")
    public static final String DATABASE_PATH = "/data/data/com.steeplesoft.cubtracker/databases";
    public static final String DATABASE_NAME = "cubtracker2.db";
    public static final String LOCAL_PATH = Constants.DATABASE_PATH + "/" + Constants.DATABASE_NAME;

    // The database version is bumped ONLY if the new version needs DB updates
    public static final int DATABASE_VERSION = VERSION_4_0_1;

    public static final int ACTIVITY_ADD_SCOUT = 1;
    public static final int ACTIVITY_RECORD_COMPLETION = 2;
    public static final int ACTIVITY_EDIT_SCOUT = 3;
    public static final int ACTIVITY_GENERATE_REPORT = 4;
    public static final int ACTIVITY_ADD_MEETING = 5;
    public static final int ACTIVITY_BACKUP_PREFS = 6;
    public static final int ACTIVITY_VIEW_DETAILS = 7;
    public static final int ACTIVITY_ADD_DEN = 8;
    public static final int ACTIVITY_UPGRADE = 9;

    public static final int CONTEXTMENU_EDIT_SCOUT = 1001;
    public static final int CONTEXTMENU_DELETE_SCOUT = 1002;
    public static final int CONTEXTMENU_DELETE_COMPLETION = 1003;
    public static final int CONTEXTMENU_EDIT_DEN = 1004;
    public static final int CONTEXTMENU_DELETE_DEN = 1005;

    public static final int COLOR_SELECTED = CubTracker.getAppContext().getResources().getColor(R.color.Selected);
    public static final int COLOR_DEFAULT = CubTracker.getAppContext().getResources().getColor(R.color.Default);

    public static final String PREF_LAST_VERSION_CHECKED = "LAST_VERSION_CHECKED";
    public static final String PREF_DONT_SHOW_VERSION_CHECK = "DONT_SHOW_VERSION_CHECK";

    public static final String BACKUP_DIR = "cubtracker";

    public static final int RANK_BOBCAT = 0;
    public static final int RANK_TIGER = 1;
    public static final int RANK_WOLF = 2;
    public static final int RANK_BEAR = 3;
    public static final int RANK_WEB1 = 4;
    public static final int RANK_WEB2 = 5;
    public static final int RANK_WEBELOS = 6;
    public static final int RANK_ARROW_OF_LIGHT = 7;
    public static final int RANK_BOY_SCOUTS = 8;

    public static final String TYPE_ACHIEVEMENT = "A";
    public static final String TYPE_ELECTIVE = "E";
    public static final String TYPE_BADGE = "B";
    public static final String TYPE_LOOP = "L";
    public static final String TYPE_PIN = "P";
    public static final String TYPE_ADVENTURE_REQ = "a";
    public static final String TYPE_ADVENTURE_ELECTIVE = "e";
}
