package com.steeplesoft.cubtracker.android.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.ExpandableListContextMenuInfo;
import android.widget.TextView;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.activities.MeetingFormActivity;
import com.steeplesoft.cubtracker.android.adapter.ExpandableListAdapter;
import com.steeplesoft.cubtracker.android.adapter.util.ExpandListChild;
import com.steeplesoft.cubtracker.android.adapter.util.ExpandListGroup;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.model.Meeting;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BrowseMeetingsFragment extends ExpandableListFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LayoutInflater li = (LayoutInflater) LayoutInflater.from(getActivity());
        return li.inflate(R.layout.list_expandable, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        mList = (ExpandableListView) getView().findViewById(android.R.id.list);
        mList.setTag("meetings");
        setListAdapter(new ExpandableListAdapter(getActivity(), getGroupList()));
        setEmptyText("-- No dens have been set up --");

        mList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View view, int groupPosition, int childPosition,
                    long id) {
                TextView tv = (TextView) view.findViewById(R.id.tvChild);
                Meeting meeting = (Meeting) tv.getTag();
                Intent intent = new Intent(view.getContext(), MeetingFormActivity.class);
                intent.putExtra(IntentConstants.MEETING_ID, meeting.getId());
                startActivity(intent);
                return false;
            }
        });
        mList.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
                if (ExpandableListView.getPackedPositionType(((ExpandableListContextMenuInfo)menuInfo).packedPosition) ==
                        ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                    menu.setHeaderTitle("Meeting Management");
                    menu.add(0, Constants.CONTEXTMENU_DELETE_SCOUT, 1, "Delete Meeting");
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.activity_meetings, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add_meeting:
                startActivityForResult(new Intent(getActivity(), MeetingFormActivity.class), Constants.ACTIVITY_ADD_MEETING);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.ACTIVITY_ADD_MEETING) {
            if (resultCode == Activity.RESULT_OK) {
                setListAdapter(new ExpandableListAdapter(getActivity(), getGroupList()));
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) item.getMenuInfo();
        int groupPos = 0, childPos = 0;
        int type = ExpandableListView.getPackedPositionType(info.packedPosition);
        if (type == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
            groupPos = ExpandableListView.getPackedPositionGroup(info.packedPosition);
            childPos = ExpandableListView.getPackedPositionChild(info.packedPosition);
        }

        final ExpandListGroupDenMeeting group = (ExpandListGroupDenMeeting)mAdapter.getGroup(groupPos);
        final ExpandListChildMeeting child = (ExpandListChildMeeting) mAdapter.getChild(groupPos, childPos);

        switch (item.getItemId()) {
            case Constants.CONTEXTMENU_DELETE_SCOUT:
                Util.showConfirmationDialog(getActivity(),
                        String.format("Are you sure you want to delete the %s meeting for %s?",
                                group.getName(), child.getName()),
                        new android.content.DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(android.content.DialogInterface dialog, int which) {
                                if (which == android.content.DialogInterface.BUTTON_POSITIVE) {
                                    DatabaseAccess.getInstance().deleteMeeting(child.getId());
                                    setListAdapter(new ExpandableListAdapter(getActivity(), getGroupList()));
                                }
                            }
                        });

                break;
        }
        return true;
    }

    @Override
    public void onResume() {
        ensureList();
        super.onResume();
    }

//    @Override
//    public void ensureList() {
//        List<ExpandListGroup> groupList = getGroupList();
//
//    }

    protected List<ExpandListGroup> getGroupList() {
        List<ExpandListGroup> list = new ArrayList<ExpandListGroup>();
        for (Den den : DatabaseAccess.getInstance().getDens()) {
            list.add(new ExpandListGroupDenMeeting(den, den.getName()));
        }
        /*
        Cursor cursor = DatabaseAccess.getInstance().getCursorForRegisteredRanks();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            long id = cursor.getLong(0);
            String name = cursor.getString(1);
            list.add(new ExpandListGroupDenMeeting(id, name));
            cursor.moveToNext();
        }
        cursor.close();
        */

        return list;
    }

    public static class ExpandListGroupDenMeeting implements ExpandListGroup {
        protected Den den;
        protected String name;
        protected List<ExpandListChild> items;

        public ExpandListGroupDenMeeting(Den den, String name) {
            super();
            this.den = den;
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public void setName(String name) {
        }

        @Override
        public List<ExpandListChild> getItems() {
            if (items == null) {
                try {
                    items = new ArrayList<ExpandListChild>();
                    for (Meeting meeting : DatabaseAccess.getInstance().getMeetingsForDen(den)) {
                        items.add(new ExpandListChildMeeting(meeting));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return items;
        }

        @Override
        public void setItems(List<ExpandListChild> items) {
            this.items = items;
        }
    }

    public static class ExpandListChildMeeting implements ExpandListChild {
        private Meeting meeting;

        public ExpandListChildMeeting(Meeting meeting) {
            super();
            this.meeting = meeting;
        }

        @Override
        public long getId() {
            return meeting.getId();
        }

//        @SuppressLint("SimpleDateFormat")
        @Override
        public String getName() {
//            DateFormat df = new SimpleDateFormat("E, MMMMM dd, yyyy");

            return formatDate(meeting.getMeetingDate(), true);
                    //android.text.format.DateFormat.format("EEEEE, MMMMM d, yyyy", meetingDate).toString();
        }

        public static String[] MONTHS = {"January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December" };
        public static String[] DAYS = {"Sunday", "Monday", "Tuesday", "Wednesday",
            "Thursday", "Friday", "Saturday"};
        public static String formatDate(Date date, boolean showDay) {
            StringBuilder sb = new StringBuilder();
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            if (showDay) {
                sb.append(DAYS[c.get(Calendar.DAY_OF_WEEK)-1])
                        .append(", ");
            }
            sb.append(MONTHS[c.get(Calendar.MONTH)])
                    .append(" ")
                    .append(c.get(Calendar.DAY_OF_MONTH))
                    .append(", ")
                    .append(c.get(Calendar.YEAR));

            return sb.toString();
        }

        @Override
        public void setName(String Name) {

        }

        @Override
        public Object getTag() {
            return meeting;
        }

        @Override
        public void setTag(Object Tag) {
        }

    }
}
