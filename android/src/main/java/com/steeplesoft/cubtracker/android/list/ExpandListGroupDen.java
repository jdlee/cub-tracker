package com.steeplesoft.cubtracker.android.list;

import com.steeplesoft.cubtracker.android.adapter.util.ExpandListChild;
import com.steeplesoft.cubtracker.android.adapter.util.ExpandListGroup;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.model.Scout;
import com.steeplesoft.cubtracker.android.utils.backup.RankCheck;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jdlee on 6/1/15.
 */
public class ExpandListGroupDen implements ExpandListGroup {
    private Den den;

    public ExpandListGroupDen(Den den) {
        this.den = den;
    }

    @Override
    public String getName() {
        return den.getName() + "(" + den.getRank().getName() + ")";
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public List<ExpandListChild> getItems() {
        List<ExpandListChild> children = new ArrayList<ExpandListChild>();

        for (Scout s : DatabaseAccess.getInstance().getScoutsForDen(den.getId())) {
            RankCheck rc = new RankCheck(s, DatabaseAccess.getInstance().getItemCompletionsForScout(s.getId()));
//                if (rc.getPercentComplete() > 0.0) {
            children.add(new ExpandListChildScout(s, rc.getPercentComplete()));
//                }
        }
        return children;
    }

    @Override
    public void setItems(List<ExpandListChild> items) {

    }
}
