package com.steeplesoft.cubtracker.android.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SimpleAdapter;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Item;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchResultsActivity extends ListActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(Constants.LOG_TAG, "Creating activity: " + getClass().getSimpleName());

        DatabaseAccess instance = DatabaseAccess.getInstance();
        Cursor searchCursor = instance.getSearchCursor(
                "%" + getIntent().getStringExtra(IntentConstants.SEARCH_TEXT) + "%",
                getIntent().getLongExtra(IntentConstants.RANK_ID, -1));
        List<Map<String, String>> data = new ArrayList<Map<String, String>>();
        int idIdx = searchCursor.getColumnIndex("_id");
        while (!searchCursor.isAfterLast()) {
            Map<String, String> row = new HashMap<String, String>();
            Item i = instance.getItem(searchCursor.getLong(idIdx));
            row.put("id", ""+i.getId());
            row.put("title", i.getTitle());
            row.put("description", i.getDescription());
            data.add(row);
            searchCursor.moveToNext();
        }
        searchCursor.close();

        setListAdapter(new SimpleAdapter(this, data, R.layout.search_result_item,
                new String[] { "title", "description" },
                new int[] { android.R.id.text1, android.R.id.text2 }));

        getListView().setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Map<String, String> map = (Map<String, String>)parent.getItemAtPosition(position);
                Intent intent = new Intent(view.getContext(), ViewItemActivity.class);
                intent.putExtra(IntentConstants.ITEM_ID, Long.parseLong(map.get("id")));
                startActivity(intent);
            }

        });

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

    }

}
