package com.steeplesoft.cubtracker.android.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.model.Group;
import com.steeplesoft.cubtracker.android.model.Item;
import com.steeplesoft.cubtracker.android.model.ItemCompletion;
import com.steeplesoft.cubtracker.android.model.Meeting;
import com.steeplesoft.cubtracker.android.model.Part;
import com.steeplesoft.cubtracker.android.model.Rank;
import com.steeplesoft.cubtracker.android.model.Scout;
import com.steeplesoft.cubtracker.android.utils.backup.AndroidCompletionsAnalyzer;
import com.steeplesoft.cubtracker.android.utils.backup.RankEnum;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class DatabaseAccess {
    private DatabaseAccess() {
    }

    private static class LazyHolder {
        private static final DatabaseAccess INSTANCE = new DatabaseAccess();
    }

    public static DatabaseAccess getInstance() {
        return LazyHolder.INSTANCE;
    }

    public void markDatabaseUpgraded() {
        execSql("UPDATE settings SET setting_value = ? WHERE setting_key = 'db_version'", Constants.DATABASE_VERSION);
//        Util.setIntPref("DB_VERSION", Constants.DATABASE_VERSION);
    }

    // TODO: make this protected
    public void execSql(String sql) {
    	getWritableDatabase().execSQL(sql);
    }

    protected void execSql(String sql, long id) {
        execSql(sql, new Object[] { id });
    }

    protected void execSql(String sql, Object[] args) {
        getWritableDatabase().execSQL(sql, args);
    }

    public SQLiteDatabase getWritableDatabase() {
        return Util.getHelper().getWritableDatabase();
    }

    // TODO: make this protected
    public Cursor executeSelectStatement(String query) {
        return executeSelectStatement(query , null);
    }

    public Cursor executeSelectStatement(String query, long id) {
        return executeSelectStatement(query, new String[] { Long.toString(id) } );
    }

    public Cursor executeSelectStatement(String query, String[] args) {
        // From the docs: A Cursor object, which is positioned before the first entry.
        Cursor cursor = Util.getHelper().getReadableDatabase().rawQuery(query , args);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public int getDatabaseVersion() {
        int version = 0;
        try {
            Cursor c = executeSelectStatement("SELECT setting_value FROM settings WHERE setting_key = 'db_version'");
            version = c.getInt(0);
            c.close();
        } catch (Exception e) {

        }
        return version;
    }
    
    public long getLastId() {
        Cursor cursor = executeSelectStatement("SELECT last_insert_rowid()");
        long id = cursor.getLong(0);
        cursor.close();
        return id;
    }

    public Cursor getSearchCursor(String searchText, long rank) {
        //String select = "SELECT p._id, p.description FROM parts p, groups g, items i ";
        String select = "SELECT distinct i._id, i.title FROM parts p, groups g, items i, ranks r "
                + " WHERE p.group_id = g._id AND g.item_id = i._id "
                + " AND (p.description like (?) OR g.name like (?) OR i.title like (?) OR "
                + "      p.notes like (?) OR i.notes like (?)) " + (rank > -1 ? " AND i.rank_id = ? " : "")
                + " AND i.rank_id = r._id " //not in (4,5) "
                + " AND type not in ('A','E','Ls','La','Ps','Pa')"
                + " ORDER BY i.type, rank_id, i._id, g._id, p._id";

        List<String> params = new ArrayList<String>();
        params.add(searchText);
        params.add(searchText);
        params.add(searchText);
        params.add(searchText);
        params.add(searchText);
        if (rank > -1) {
            params.add(Long.toString(rank));
        }

        return this.executeSelectStatement(select, params.toArray(new String[]{}));
    }


    // region Den Methods

    /******************************************************************************************************************/
    /* Den Methods                                                                                                  */
    /******************************************************************************************************************/
    public Den getDen(long id) {
        Cursor cursor = executeSelectStatement("SELECT _id, name, rank_id FROM dens WHERE _id = ?", id);
        Den den = null;
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            den = new Den(id,
                    getRank(cursor.getLong(cursor.getColumnIndex("rank_id"))),
                    cursor.getString(cursor.getColumnIndex("name")));
        }
        cursor.close();
        return den;
    }

    public List<Den> getDens() {
        List<Den> list = new ArrayList<Den>();
        Cursor cursor = executeSelectStatement("SELECT dens._id, dens.name, rank_id FROM dens, ranks WHERE rank_id = ranks._id and hidden = 0 ORDER BY rank_id, dens.name");
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                list.add(new Den(cursor.getLong(cursor.getColumnIndex("_id")),
                        getRank(cursor.getLong(cursor.getColumnIndex("rank_id"))),
                        cursor.getString(cursor.getColumnIndex("name"))));
                cursor.moveToNext();
            }
        }
        cursor.close();
        return list;
    }

    public List<Scout> getScoutsForDen(long denId) {
        List<Scout> list = new ArrayList<Scout>();
        Cursor cursor = executeSelectStatement("SELECT _id, name, den_id, email_address, phone_number, contacts FROM scouts where den_id = ? ORDER BY name", denId);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                list.add(buildScoutFromCursor(cursor));
                cursor.moveToNext();
            }
        }
        cursor.close();
        return list;
    }

    public void createDen(String name, long rank) {
        String sql = "INSERT INTO dens (name, rank_id) VALUES (?,?);";
        execSql(sql, new Object[] { name, rank });
    }

    public void updateDen(long id, String name, long rankId) {
        String sql = "UPDATE dens SET name = ?, rank_id = ? WHERE _id = ?";
        execSql(sql, new Object[] { name, rankId, id });
    }

    public void deleteDen(long id) {
        execSql("DELETE FROM dens WHERE _id = ?", new Object[] { id });
    }

    public boolean isDenEmpty(long id) {
        Cursor c = executeSelectStatement("SELECT count(*) FROM scouts where den_id = ?",
                new String[]{""+id});
        if (!c.isAfterLast()) {
            return c.getInt(0) == 0;
        }

        return false;
    }

    // endredion Den Methods

    /******************************************************************************************************************/
    /* Scout Methods                                                                                                  */
    /******************************************************************************************************************/
    public Scout getScout(long id) {
        Cursor cursor = executeSelectStatement("SELECT _id, name, den_id, email_address, phone_number, contacts FROM scouts WHERE _id = ?", id);
        Scout scout = null;
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            scout = buildScoutFromCursor(cursor);
        }
        cursor.close();
        return scout;
    }

    public List<Scout> getScouts() {
        Cursor cursor = executeSelectStatement("SELECT s._id, s.name, den_id, email_address, phone_number, contacts FROM scouts s INNER JOIN dens d on den_id = d._id WHERE d.rank_id < 8 ORDER BY s.name");
        List<Scout> scouts = new ArrayList<Scout>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            scouts.add(buildScoutFromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return scouts;
    }

    private Scout buildScoutFromCursor(Cursor cursor) {
        Scout scout;
        scout = new Scout(cursor.getLong(0), // id
                cursor.getString(1), // name
                cursor.getString(3), // email
                cursor.getString(4), // phone
                getDen(cursor.getLong(2)), // den id
                cursor.getString(5)); // contacts
        return scout;
    }

    public byte[] getScoutPhoto(long id) {
        byte[] photo = null;
        Cursor cursor = executeSelectStatement("SELECT photo FROM scouts WHERE _id = ?", id);
        if (cursor.getCount() > 0) {
            try {
                photo = cursor.getBlob(cursor.getColumnIndex("photo"));
            } catch (Exception e) {
                Util.displayMessage("Picture load failed. Please try adding a smaller photo.");
            }
        }
        cursor.close();
        return photo;
    }

    public void createScout(String name, Den den, String emailAddress, String phoneNumber, byte[] photo, String contacts) {
        String sql = "INSERT INTO scouts (name, den_id, email_address, phone_number, contacts, photo) VALUES (?,?,?,?,?,?);";
        execSql(sql, new Object[] { name, den.getId(), emailAddress, phoneNumber, contacts, photo });
    }

    public void updateScout(long id, String name, Den den, String emailAddress, String phoneNumber, byte[] photo, String contacts) {
        String sql = "UPDATE scouts SET name = ?, den_id = ?, email_address = ?, phone_number = ?, contacts = ?, photo = ? WHERE _id = ?";
        execSql(sql, new Object[] { name, den.getId(), emailAddress, phoneNumber, contacts, photo, id });
    }

    public void deleteScout(long id) throws SQLException {
        execSql("DELETE FROM scouts WHERE _id = ?", new Object[] { id });
    }
    
    public void promoteDens() throws SQLException {
    	execSql("UPDATE dens set rank_id = rank_id + 1 where rank_id < 6");
    }

    public List<Scout> getScoutsForRank(long rank, boolean mergeWebelos) {
        List<Scout> list = new ArrayList<Scout>();
        Cursor cursor = getCursorForAllScoutsInRank(rank, mergeWebelos);
        while (!cursor.isAfterLast()) {
            list.add(buildScoutFromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();

        return list;
    }

    public Cursor getCursorForAllScouts() {
        return getCursorForAllScoutsInRank(-1);
    }
    
    public Cursor getCursorForAllScoutsInRank(long rank) {
        return getCursorForAllScoutsInRank(rank, false);
    }

    public Cursor getCursorForAllScoutsInRank(long rank, boolean mergeWebelos) {
        String select = "SELECT s._id, s.name, s.den_id, s.email_address, s.phone_number, s.contacts FROM scouts s JOIN dens d on den_id = d._id ";
        String where = "WHERE d.rank_id = ?";
        String [] params = new String[] { };
        if (rank >= 0) {
            if (rank >= 4 && mergeWebelos) {
                where = " WHERE d.rank_id >= 4";
            } else {
                params = new String[] { Long.toString(rank) };
            }
        } else {
            where = "";
        }
        
        where += (where.length() > 0 ? " AND " : " WHERE ") + " d.rank_id < 8";

        String query = select + where +
                " UNION SELECT s._id, s.name, s.den_id, s.email_address, s.phone_number, s.contacts " +
                " FROM scouts s JOIN dens d on den_id = d._id WHERE d.rank_id = 0";
        return executeSelectStatement(query + " ORDER BY s.name", params);
    }

    /******************************************************************************************************************/
    /* Rank Methods                                                                                                   */
    /******************************************************************************************************************/
    public Rank getRank(long id) {
        Cursor cursor = executeSelectStatement("SELECT _id, name FROM ranks WHERE _id = ?" , id);
        Rank rank = null;
        if (cursor.getCount() > 0) {
            rank = new Rank(id, cursor.getString(cursor.getColumnIndex("name")));
        }
        cursor.close();
        return rank;
    }

    public List<Rank> getRanks() {
        return getRanks(-1, 999);
    }
    
    public List<Rank> getRanks(long beginning, long ending) {
        Cursor cursor = executeSelectStatement("SELECT _id, name FROM ranks WHERE _id BETWEEN ? and ? AND hidden = 0 ORDER BY _id",
                new String [] { Long.toString(beginning), Long.toString(ending) });
        List<Rank> ranks = new ArrayList<Rank>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Rank rank = new Rank();
            rank.setId(cursor.getLong(cursor.getColumnIndex("_id")));
            rank.setName(cursor.getString(cursor.getColumnIndex("name")));
            ranks.add(rank);
            cursor.moveToNext();
        }
        cursor.close();
        return ranks;
    }
    
    public Cursor getCursorForAllRanks() throws SQLException {
        return executeSelectStatement("SELECT _id, name FROM ranks WHERE _id > 0 AND _id <= 5 ORDER BY _id", new String[] {});
    }

    public Cursor getCursorForRegisteredRanks() {
        String query = "SELECT _id, name FROM ranks where _id in (SELECT DISTINCT rank_id FROM dens WHERE rank_id <= 8)";// AND _id > 0"; // Don't show Bobcat
        return executeSelectStatement(query , null);
    }

    public boolean hasScout(RankEnum rank) {
        Cursor cursor = executeSelectStatement("SELECT 1 FROM dens d WHERE d.rank_id = ?", rank.rank);
        boolean has = cursor.getCount() > 0;
        cursor.close();
        return has;
    }

    /******************************************************************************************************************/
    /* Item Methods                                                                                                   */
    /******************************************************************************************************************/
    public Item getItem(long id) {
        Cursor cursor = executeSelectStatement("SELECT _id, type, rank_id, number, title, notes, image FROM items WHERE _id = ?;", id);
        Item item = null;
        if (!cursor.isAfterLast()) {
            item = getItemFromCursor(cursor);
        }
        cursor.close();
        return item;
    }

    public Item getItemFromCursor(Cursor cursor) {
        return new Item(cursor.getLong(cursor.getColumnIndex("_id")),
                cursor.getString(cursor.getColumnIndex("type")),
                getRank(cursor.getLong(cursor.getColumnIndex("rank_id"))),
                cursor.getString(cursor.getColumnIndex("number")),
                cursor.getString(cursor.getColumnIndex("title")),
                cursor.getString(cursor.getColumnIndex("notes")),
                cursor.getString(cursor.getColumnIndex("image")));
    }

    public List<Item> getItemsForType(String type) throws SQLException {
        List<Item> items = new ArrayList<Item>();
        Cursor cursor = executeSelectStatement(
                "SELECT _id, type, rank_id, number, title, notes, image FROM items WHERE type like ? ORDER BY title;", 
                new String[] { type + "%" });
        while (!cursor.isAfterLast()) {
            items.add(getItemFromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        
        return items;
    }
    
    public List<Item> getItemsForRankAndType(long rankId, String type) throws SQLException {
        List<Item> items = new ArrayList<Item>();
        Cursor cursor = executeSelectStatement(
                "SELECT _id, type, rank_id, number, title, notes, image FROM items WHERE type = ? and rank_id = ?;", 
                new String[] { type, Long.toString(rankId) });
        while (!cursor.isAfterLast()) {
            items.add(getItemFromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        
        return items;
    }

    /******************************************************************************************************************/
    /* Group Methods                                                                                                  */
    /******************************************************************************************************************/
    public Group getGroup(long id) throws SQLException {
        Cursor cursor = executeSelectStatement("SELECT _id, item_id, name, number, qty_required, part_count, notes FROM groups WHERE _id = ?", id);
        Group group = null;
        if (!cursor.isAfterLast()) {
            group = new Group(id, 
                    getItem(cursor.getLong(cursor.getColumnIndex("item_id"))), 
                    cursor.getString(cursor.getColumnIndex("name")), 
                    cursor.getString(cursor.getColumnIndex("number")), 
                    cursor.getInt(cursor.getColumnIndex("qty_required")),
                    cursor.getInt(cursor.getColumnIndex("part_count")),
                    cursor.getString(cursor.getColumnIndex("notes")));
        }
        cursor.close();
        return group;
    }

    public List<Group> getAllGroups(long itemId) throws SQLException {
        List<Group> groups = new ArrayList<Group>();
        Cursor cursor = executeSelectStatement("SELECT _id, item_id, name, number, qty_required, part_count, notes FROM groups WHERE item_id = ?", itemId);
        while (!cursor.isAfterLast()) {
            groups.add(new Group(cursor.getLong(cursor.getColumnIndex("_id")), 
                    getItem(cursor.getLong(cursor.getColumnIndex("item_id"))), 
                    cursor.getString(cursor.getColumnIndex("name")), 
                    cursor.getString(cursor.getColumnIndex("number")), 
                    cursor.getInt(cursor.getColumnIndex("qty_required")),
                    cursor.getInt(cursor.getColumnIndex("part_count")),
                    cursor.getString(cursor.getColumnIndex("notes"))));
            cursor.moveToNext();
        }
        cursor.close();
        return groups;
    }

    /******************************************************************************************************************/
    /* Part Methods                                                                                                   */
    /******************************************************************************************************************/
    public Part getPart(long id) throws SQLException {
        Cursor cursor = executeSelectStatement("SELECT _id, group_id, number, description, notes FROM parts WHERE _id = ?", id);
        Part part = null;
        if (!cursor.isAfterLast()) {
            part = new Part(id, 
                    getGroup(cursor.getLong(cursor.getColumnIndex("group_id"))), 
                    cursor.getString(cursor.getColumnIndex("number")), 
                    cursor.getString(cursor.getColumnIndex("description")), 
                    cursor.getString(cursor.getColumnIndex("notes")));
        }
        cursor.close();

        return part;

    }

    public Cursor getPartDetailCursor(long partId, long rankId) {
        final boolean isTigerOrGreater = rankId > 0;

        String query = "SELECT c._id, s.name  FROM  ";
        String tables = " scouts s, dens d, items i, groups g, parts p, completions c";
        String where = " WHERE c.part_id = ? AND c.part_id = p._id AND p.group_id = g._id " +
                "   AND g.item_id = i._id AND c.scout_id = s._id ";
        String orderBy = " ORDER BY s.name";

        if (isTigerOrGreater) {
//            tables += ", ranks r";
            if (rankId == 4 || rankId == 5) {
                where += " AND d.rank_id in (4,5)";
            }
            where += " AND s.den_id = d._id";
        }
        return executeSelectStatement(query + tables + where + orderBy, new String[]{Long.toString(partId)});
    }

    public List<Part> getPartsForGroup(long groupId) throws SQLException {
        List<Part> parts = new ArrayList<Part>();
        Cursor cursor = executeSelectStatement("SELECT _id, group_id, number, description, notes FROM parts WHERE group_id = ?", groupId);
        while (!cursor.isAfterLast()) {
            parts.add(new Part(cursor.getLong(cursor.getColumnIndex("_id")), 
                    getGroup(cursor.getLong(cursor.getColumnIndex("group_id"))), 
                    cursor.getString(cursor.getColumnIndex("number")), 
                    cursor.getString(cursor.getColumnIndex("description")), 
                    cursor.getString(cursor.getColumnIndex("notes"))));
            cursor.moveToNext();
        }
        cursor.close();
        return parts;
    }
    
    /******************************************************************************************************************/
    /* Part Completion Methods                                                                                             */
    /******************************************************************************************************************/
    public void createPartCompletion(long partId, long scoutId, Date date) {
        Cursor c = executeSelectStatement("SELECT count(*) FROM completions WHERE scout_id = ? AND part_id = ?",
                new String[] {Long.toString(scoutId), Long.toString(partId)});
        int count = 0;
        if (!c.isAfterLast()) {
            count = c.getInt(0);
        }
        c.close();
        if (count == 0) {
            execSql("INSERT INTO completions (scout_id, part_id, timestamp) VALUES (?,?,?)",
                new Object[] { scoutId, partId, Meeting.getDateFormat().format(date) });
        }
    }

    public void deletePartCompletion(long id) throws SQLException {
        execSql("DELETE FROM completions WHERE _id = ?", id);
    }

    public long[] getScoutsWhoCompletedPart(long partId) {
        Cursor c = this.executeSelectStatement("SELECT scout_id FROM completions WHERE part_id = ?", partId);
        List<Long> ids = new ArrayList<Long>();
        int colIndex = c.getColumnIndex("scout_id");
        while (!c.isAfterLast()) {
            ids.add(c.getLong(colIndex));
            c.moveToNext();
        }
        c.close();

        long[] array = new long[ids.size()];
        for (int i = 0; i < ids.size(); i++) {
            array[i] = ids.get(i);
        }
        return array;
    }

    public List<String> getPartCompletionsForScout(long scoutId) {
        List<String> completions = new ArrayList<String>();
        String query = Util.readFileAsset(R.raw.parts_completed);
        query = query.replaceAll("--1.*1--", "AND scouts._id IN ("+scoutId+")");
        Cursor c = executeSelectStatement(query);
        while (!c.isAfterLast()) {
            Item i = getItemFromCursor(c);
            String entry = i.getDescription() + " part " +
                    c.getString(c.getColumnIndex("pnum")) +
                    " (" + Util.getDateString(c.getString(c.getColumnIndex("completed"))) + ")";
            completions.add(entry);
            c.moveToNext();
        }
        c.close();

        return completions;
    }

    public void analzePartCompletions(long[] scoutIds) {
        AndroidCompletionsAnalyzer aca = new AndroidCompletionsAnalyzer();
        aca.analyzePartCompletions(scoutIds);
    }

    /******************************************************************************************************************/
    /* Item Completion Methods                                                                                             */
    /******************************************************************************************************************/
    public void createItemCompletion(long itemId, long scoutId, Date date) {
        Cursor c = executeSelectStatement("SELECT count(*) FROM item_completions WHERE scout_id = ? AND item_id = ?",
                new String[] {Long.toString(scoutId), Long.toString(itemId)});
        int count = 0;
        if (!c.isAfterLast()) {
            count = c.getInt(0);
        }
        c.close();
        if (count == 0) {
            execSql("INSERT INTO item_completions (scout_id, item_id, completed) VALUES (?,?,?)",
                    new Object[] { scoutId, itemId, Meeting.getDateFormat().format(date) });
        }
    }

    public void deleteItemCompletion(long id) throws SQLException {
        execSql("DELETE FROM item_completions WHERE _id = ?", id);
    }

    public List<ItemCompletion> getCompletionsForItem(Item item) {
        List<ItemCompletion> completions = new ArrayList<ItemCompletion>();
        Rank rank = item.getRank();
        String rankClause = null;
        if (rank != null) {
            if (rank.getId() == 4 || rank.getId() == 5) {
                rankClause = "4,5";
            } else if (rank.getId() != 0) {
                rankClause = Long.toString(rank.getId());
            }
        }
        String query =
                " SELECT ic._id as compId, ic.scout_id, ic.completed " +
                " FROM item_completions ic, scouts s, items i, dens d " +
                " WHERE item_id = ? " +
                "     AND s._id = scout_id " +
                "     AND s.den_id = d._id " +
                "     AND item_id = i._id";
        if (rankClause != null) {
            query += " AND d.rank_id in (" + rankClause + ")";
        }
        if (rank.getId() == 7) {

        }
        query += " ORDER BY d.rank_id, s.name";
        Cursor c = executeSelectStatement(query, item.getId());
        int compIdx = c.getColumnIndex("compId");
        int scoutIdx = c.getColumnIndex("scout_id");
        int tsIdx = c.getColumnIndex("completed");
        while (!c.isAfterLast()) {
            ItemCompletion ic = new ItemCompletion();
            ic.setId(c.getLong(compIdx));
            ic.setScout(getScout(c.getLong(scoutIdx)));
            String ds = c.getString(tsIdx);
            // Thu Jul 31 00:00:00 CDT 2014
            try {
                Date date = Meeting.getDateFormat().parse(ds);
                        //new SimpleDateFormat("E MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH).parse(ds);
                ic.setTimestamp(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            completions.add(ic);
            c.moveToNext();
        }
        c.close();

        return completions;
    }

    public long[] getScoutsWhoCompletedItem(long itemId) {
        // Add join to return only rows where item and scout rank match? If so, fix up parts too
        Cursor c = this.executeSelectStatement("SELECT scout_id FROM item_completions WHERE item_id = ?", itemId);
        List<Long> ids = new ArrayList<Long>();
        int colIndex = c.getColumnIndex("scout_id");
        while (!c.isAfterLast()) {
            ids.add(c.getLong(colIndex));
            c.moveToNext();
        }
        c.close();

        long[] array = new long[ids.size()];
        for (int i = 0; i < ids.size(); i++) {
            array[i] = ids.get(i);
        }
        return array;
    }

    public List<ItemCompletion> getItemCompletionsForScout(long scoutId) {
        Scout scout = getScout(scoutId);
        scout.getDen();
        List<ItemCompletion> completions = new ArrayList<ItemCompletion>();
        String sql = " SELECT ic._id, ic.scout_id, ic.completed, " +
                "   i._id, i.type, i.rank_id, i.number, i.title, i.notes, i.group_count, i.image " +
                " FROM item_completions ic JOIN scouts s ON ic.scout_id = s._id " +
                "   JOIN items i ON ic.item_id = i._id " +
                "   JOIN dens d ON s.den_id = d._id AND (d.rank_id = i.rank_id ";
        if (scout.getDen().getRank().getId() == RankEnum.ARROW_OF_LIGHT.rank) {
            sql += "  or ((i.rank_id = 6) AND (i.type = 'e')) ";
        }
        sql += ")  WHERE ic.scout_id = ? ";
        Cursor c = executeSelectStatement(sql, scoutId);
        int scoutIdx = c.getColumnIndex("scout_id");
        int tsIdx = c.getColumnIndex("completed");
        while (!c.isAfterLast()) {
            ItemCompletion ic = new ItemCompletion();
            ic.setScout(getScout(c.getLong(scoutIdx)));
            String ds = c.getString(tsIdx);
            Log.d(Constants.LOG_TAG, ds);
            try {
                int index = ds.indexOf(" ");
                String format = "yyyy-MM-dd";
                if (index == 3) {
                    format = "EEE MMM dd HH:mm:ss zzz yyyy";
                } else if (index > 1) {
                    ds = ds.substring(0, index);
                }
                Date date =  new SimpleDateFormat(format, Locale.ENGLISH).parse(ds);
                ic.setTimestamp(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            ic.setItem(getItemFromCursor(c));
            completions.add(ic);
            c.moveToNext();
        }
        c.close();

        return completions;
    }

    /******************************************************************************************************************/
    /* Meeting Methods                                                                                                */
    /******************************************************************************************************************/
    public Meeting getMeeting(long id) {
        Cursor cursor = executeSelectStatement("SELECT _id, den_id, meeting_date, reported, notes FROM meetings WHERE _id = ?", id);
        Meeting meeting = null;
        if (!cursor.isAfterLast()) {
            meeting = new Meeting(id, 
                    getDen(cursor.getLong(cursor.getColumnIndex("den_id"))),
                    cursor.getString(cursor.getColumnIndex("meeting_date")), 
                    cursor.getString(cursor.getColumnIndex("reported")), 
                    cursor.getString(cursor.getColumnIndex("notes")));
        }
        cursor.close();
        return meeting;
    }

    public List<Meeting> getMeetingsForDen(Den den) throws SQLException {
        List<Meeting> meetings = new ArrayList<Meeting>();
        Cursor cursor = executeSelectStatement(
                "SELECT _id, den_id, meeting_date, reported, notes FROM meetings WHERE den_id = ? AND rank_id = ? ORDER BY meeting_date DESC",
                new String[] {
                        ""+den.getId(),
                        ""+den.getRank().getId()});
        if (cursor.getCount() > 0) {
            while (!cursor.isAfterLast()) {
                meetings.add(new Meeting(cursor.getLong(cursor.getColumnIndex("_id")),
                        getDen(cursor.getLong(cursor.getColumnIndex("den_id"))),
                        cursor.getString(cursor.getColumnIndex("meeting_date")),
                        cursor.getString(cursor.getColumnIndex("reported")),
                        cursor.getString(cursor.getColumnIndex("notes"))));
                cursor.moveToNext();
            }
        }
        cursor.close();
        
        return meetings;
    }

    public void createMeeting(Den den, Date date, String notes, long[] attendees) throws SQLException {
        execSql("INSERT INTO meetings (den_id, rank_id, meeting_date, notes) VALUES (?,?,?,?)",
                new Object[] {
                        den.getId(),
                        den.getRank().getId(),
                        Meeting.getDateFormat().format(date),
                        notes
                } );
        saveAttendees(getLastId(), attendees);
    }

    public void updateMeeting(long meetingId, Den den,  Date date, String notes, long[] attendees) throws SQLException {
        execSql("UPDATE meetings SET den_id = ?, rank_id = ?, meeting_date = ?, notes = ? WHERE _id = ?",
                new Object[] {
                        den.getId(),
                        den.getRank().getId(),
                        Meeting.getDateFormat().format(date),
                        notes,
                        meetingId
                });
        saveAttendees(meetingId, attendees);
    }
    

    public void deleteMeeting(long id) {
        final Object[] meetingId = new Object[] { id };
        execSql("DELETE FROM attendees WHERE meeting_id = ?", meetingId);
        execSql("DELETE FROM meetings WHERE _id = ?", meetingId);
    }

    public long[] getAttendees(long meetingId) {
        Cursor cursor = executeSelectStatement("SELECT scout_id FROM attendees WHERE meeting_id = ?",
                new String[]{Long.toString(meetingId)});
        Set<Long> set = new HashSet<Long>();
//        cursor.moveToFirst();
        final int columnIndex = cursor.getColumnIndex("scout_id");
        while (!cursor.isAfterLast()) {
            set.add(cursor.getLong(columnIndex));
            cursor.moveToNext();
        }

        long[] ids = new long[set.size()];
        int idx = 0;
        for (Long id : set) {
            ids[idx++] = id;
        }

        return ids;
    }

    protected void saveAttendees(long meetingId, long[] attendees) {
        execSql("DELETE FROM attendees WHERE meeting_id = ?", new String[] { "" + meetingId });
        for (long scoutId : attendees) {
            execSql("INSERT INTO attendees (meeting_id,  scout_id) VALUES (?,?)", 
                    new String[] { "" + meetingId, "" + scoutId });
        }
    }
}
