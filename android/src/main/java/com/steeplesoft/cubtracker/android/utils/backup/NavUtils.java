package com.steeplesoft.cubtracker.android.utils.backup;

import android.app.Activity;
import android.content.Intent;

/**
 * Created by jdlee on 6/22/15.
 */
public class NavUtils {
    public static void navigateUpTo(Activity activity, Intent upIntent) {
        upIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(upIntent);
        activity.finish();
    }
}
