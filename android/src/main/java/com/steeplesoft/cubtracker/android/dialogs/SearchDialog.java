package com.steeplesoft.cubtracker.android.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.CubTracker;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Rank;

import java.util.List;

public class SearchDialog extends Dialog {
    private long selectedRank;
    private OnClickListener callback;
    private String searchText;

    public SearchDialog(Context context, OnClickListener onClickListener) {
        super(context);
        this.callback = onClickListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_dialog);
        setTitle("Search...");

        Button dialogButton = (Button) findViewById(R.id.button_ok);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                okButtonClicked();
            }
        });

        Button cancelButton = (Button) findViewById(R.id.button_cancel);
        cancelButton.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
//                String string = (String) parent.getItemAtPosition(pos);
                selectedRank = pos - 1; //cursor.getLong(cursor.getColumnIndex("_id"));
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        List<Rank> list = DatabaseAccess.getInstance().getRanks();
        String[] ranks = new String[list.size()+1];
        int index = 1;
        ranks[0] = "--All Ranks--";
        for (Rank rank : list) {
            ranks[index++] = rank.getName();
        }

        spinner.setAdapter(new ArrayAdapter<String>(CubTracker.getAppContext(),
                R.layout.list_item, ranks));
    }

    public long getSelectedRank() {
        return selectedRank;
    }

    public void setSelectedRank(long selectedRank) {
        this.selectedRank = selectedRank;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    protected void okButtonClicked() {
        searchText = Util.getText(findViewById(R.id.searchText));
        dismiss();
        callback.onClick(this, 0);
    }
}
