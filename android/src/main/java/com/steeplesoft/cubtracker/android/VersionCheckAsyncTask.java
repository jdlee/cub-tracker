package com.steeplesoft.cubtracker.android;

import android.app.Activity;
import android.os.AsyncTask;

import com.steeplesoft.cubtracker.android.dialogs.NewVersionDialog;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class VersionCheckAsyncTask extends AsyncTask<String, Void, String> {
    private static String url = "http://cubtracker.com/latestversion";
    private Activity parent;

    public VersionCheckAsyncTask(Activity parent) {
        this.parent = parent;
    }

    @Override
    protected String doInBackground(String... params) {
        String latestVersion = null;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new URL(url).openStream()));
            latestVersion = reader.readLine();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (latestVersion == null) {
            latestVersion = "";
        }
        return latestVersion;
    }

    @Override
    protected void onPostExecute(String latestVersion) {
        if (latestVersion != null && !latestVersion.isEmpty()) {
            String runningVersion = Util.getVersionString();
            String lastChecked = Util.getStringPref(Constants.PREF_LAST_VERSION_CHECKED, "");
            Util.setStringPref(Constants.PREF_LAST_VERSION_CHECKED, lastChecked);
            boolean dontShow = Util.getBooleanPref(Constants.PREF_DONT_SHOW_VERSION_CHECK, false);

            if (runningVersion.equals(latestVersion)) {
                return;
            }

            if (lastChecked.equals(latestVersion) && dontShow) {
                return;
            }

            Util.setBooleanPref(Constants.PREF_DONT_SHOW_VERSION_CHECK, false);
            if (!parent.isFinishing()) {
                new NewVersionDialog(parent, latestVersion).show();
            }   
        }
    }

}
