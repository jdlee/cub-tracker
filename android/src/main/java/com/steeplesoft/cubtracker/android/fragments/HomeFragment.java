package com.steeplesoft.cubtracker.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.activities.ViewScoutActivity;
import com.steeplesoft.cubtracker.android.adapter.util.ExpandListGroup;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.list.ExpandListGroupDen;
import com.steeplesoft.cubtracker.android.list.ScoutStatusExpandableListAdapter;
import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.model.Scout;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jdlee on 7/31/14.
 */
public class HomeFragment extends ExpandableListFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!Util.getHelper().needsValidation()) {
            mAdapter = new ScoutStatusExpandableListAdapter(getActivity(), getGroupList());
            setListAdapter(mAdapter);

            final ExpandableListView listView = getListView();
            listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View view, int groupPosition, int childPosition, long id) {
                    Intent intent = new Intent(view.getContext(), ViewScoutActivity.class);
                    intent.putExtra(IntentConstants.SCOUT_ID, ((Scout) view.getTag()).getId());

                    startActivity(intent);
                    return false;
                }
            });
            listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    return parent.isGroupExpanded(groupPosition);
                }
            });
        }
    }

    @Override
    public void onResume () {
        super.onResume();
        if (!Util.getHelper().needsValidation()) {
            expandAllGroups(getListView());
        }
    }

    private void expandAllGroups(final ExpandableListView listView) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                if (listView != null && listView.getExpandableListAdapter() != null) {
                    int count = listView.getExpandableListAdapter().getGroupCount();
                    for (int group = 0; group < count; group++) {
                        listView.expandGroup(group);
                    }
                }
            }
        });
    }

    protected List<ExpandListGroup> getGroupList() {
        List<ExpandListGroup> list = new ArrayList<ExpandListGroup>();
        final DatabaseAccess db = DatabaseAccess.getInstance();
        for (final Den den : db.getDens()) {
            ExpandListGroup group = new ExpandListGroupDen(den);
            list.add(group);
        }

        return list;
    }

}
