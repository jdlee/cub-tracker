package com.steeplesoft.cubtracker.android.utils.backup;

import android.util.Log;

import com.steeplesoft.cubtracker.android.Constants;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by jdlee on 2/13/14.
 */
public class FtpBackup implements BackupMethod {
    FTPClient client = new FTPClient();
    private final BackupHandlerThread ht = new BackupHandlerThread();
    private String host;
    private String user;
    private String pass;

    public FtpBackup(String host, String user, String pass) {
        this.host = host;
        this.user = user;
        this.pass = pass;
    }

    @Override
    public List<String> getFileList() {
        final List<String> files = new ArrayList<String>();
        synchronized (ht) {
            ht.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        connect();
                        Log.i(Constants.LOG_TAG, "Listing files");
                        if (client.changeWorkingDirectory(Constants.BACKUP_DIR)) {
                            for (FTPFile file : client.listFiles()) {
                                if (file.getName().contains("cubtracker")) {
                                    files.add(file.getName());
                                }
                            }
                        }
                        client.disconnect();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    synchronized (ht) {
                        ht.notify();
                    }
                }
            }, true);
        }
        return files;
    }

    public void pruneBackups(int count) {
        List<String> files = getFileList();
        if (files.size() > count) {
            Collections.sort(files);
            final List<String> toDelete = files.subList(0, files.size() - count);
            synchronized (ht) {
                ht.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            connect();
                            Log.i(Constants.LOG_TAG, "Listing files");
                            if (client.changeWorkingDirectory(Constants.BACKUP_DIR)) {
                                for (String file : toDelete) {
                                    client.deleteFile(file);
                                }
                            }
                            client.disconnect();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        synchronized (ht) {
                            ht.notify();
                        }
                    }
                }, true);
            }
        }
    }

    @Override
    public boolean upload(String file) {
        final ResultHolder holder = new ResultHolder();
        holder.result = false;
        synchronized (ht) {
            ht.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd.HH.mm");
                        String remoteFile = "cubtracker-" + sdf.format(new Date()) +".db";

                        connect();

                        InputStream input  = new FileInputStream(Constants.LOCAL_PATH);
                        client.makeDirectory(Constants.BACKUP_DIR);
                        if (client.changeWorkingDirectory(Constants.BACKUP_DIR)) {
                            client.storeFile(remoteFile, input);
                            checkReplyCode();
                            holder.result = true;
                        }
                        input.close();
                        client.disconnect();
                    } catch (IOException e) {
                    }
                    synchronized (ht) {
                        ht.notify();
                    }
                }
            }, true);
        }
        return holder.result;
    }

    @Override
    public boolean download(final String file) {
        final ResultHolder holder = new ResultHolder();
        holder.result = false;
        synchronized (ht) {
            ht.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        connect();

                        if (client.changeWorkingDirectory(Constants.BACKUP_DIR)) {
                            FileOutputStream fos = new FileOutputStream(Constants.LOCAL_PATH);
                            client.retrieveFile(file, fos);
                            checkReplyCode();
                            holder.result = true;
                            fos.close();
                        }
                        client.disconnect();
                    } catch (IOException e) {
                    }
                    synchronized (ht) {
                        ht.notify();
                    }
                }
            }, true);
        }
        return holder.result;
    }

    private void connect() throws IOException {
        client.connect(host);
        checkReplyCode();
        client.login(user, pass);
        checkReplyCode();
        client.enterLocalPassiveMode();
        checkReplyCode();
        client.setFileType(FTP.BINARY_FILE_TYPE);
        checkReplyCode();
    }

    private void checkReplyCode() throws IOException {
        int replyCode = client.getReplyCode();
        if (replyCode >= 300) {
            throw new IOException(client.getReplyString());
        }
    }
}
