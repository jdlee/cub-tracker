package com.steeplesoft.cubtracker.android.activities;

import com.steeplesoft.cubtracker.android.fragments.BrowseMeetingsFragment;

/**
 * Created by jdlee on 8/1/14.
 */
public class BrowseMeetingsActivity extends ExpandableListActivity {
    public BrowseMeetingsActivity() {
        super(BrowseMeetingsFragment.class);
    }
}
