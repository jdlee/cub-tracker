package com.steeplesoft.cubtracker.android.adapter.util;

import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Item;
import com.steeplesoft.cubtracker.android.model.Rank;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ExpandListGroupRank implements ExpandListGroup {
    protected Rank rank;
    protected String type;
    protected List<ExpandListChild> items;

    public ExpandListGroupRank(Rank rank, String type) {
        this.rank = rank;
        this.type = type;
    }

    @Override
    public String getName() {
        return rank.getName();
    }

    @Override
    public void setName(String name) {
        //
    }

    @Override
    public List<ExpandListChild> getItems() {
        if (items == null) {
            try {
                items = new ArrayList<ExpandListChild>();
                final DatabaseAccess db = DatabaseAccess.getInstance();
                for (Item item : db.getItemsForRankAndType(rank.getId(), type)) {
                    items.add(new ExpandListChildItem(item));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return items;
    }

    @Override
    public void setItems(List<ExpandListChild> items) {
        this.items = items;
    }

}
