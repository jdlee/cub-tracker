package com.steeplesoft.cubtracker.android.activities;

import android.app.Activity;
import android.os.Bundle;

import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.db.DatabaseHelper;

/**
 * Created by jdlee on 6/1/15.
 */
public class BaseActivity extends Activity {
    protected DatabaseHelper databaseHelper;
    private final int contentView;

    public BaseActivity(int contentView) {
        this.contentView = contentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(contentView);
        databaseHelper = Util.initHelper(this);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }
}
