package com.steeplesoft.cubtracker.android.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.ListView;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.SelectableListActivity;
import com.steeplesoft.cubtracker.android.adapter.SelectableListViewAdapter;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Scout;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class GenerateReportActivity extends ListActivity implements SelectableListActivity {
    private static final int START_DATE_DIALOG_ID = 1;
    private static final int END_DATE_DIALOG_ID = 2;
    private static final int ACTIVITY_VIEW_REPORT = 1;

    private SelectableListViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(Constants.LOG_TAG, "Creating activity: " + getClass().getSimpleName());

        final ListView listView = getListView();
        listView.addHeaderView(getLayoutInflater().inflate(R.layout.generate_report_header, null));
        listView.addFooterView(getLayoutInflater().inflate(R.layout.generate_report_footer, null));
        List<SelectableListViewAdapter.ListItem> scouts = new ArrayList<SelectableListViewAdapter.ListItem>();
        for (Scout scout : DatabaseAccess.getInstance().getScouts()) {
            scouts.add(new SelectableListViewAdapter.ListItem(scout.getId(), scout.getName(), scout, false));
        }
        adapter = new SelectableListViewAdapter(scouts);
        setListAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        ((CheckBox) findViewById(R.id.showRemaining)).setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton button, boolean isChecked) {
                findViewById(R.id.showRemainingOptions).setVisibility(isChecked ? View.VISIBLE : View.GONE);
                findViewById(R.id.reportAll).setVisibility(isChecked ? View.GONE : View.VISIBLE);
                findViewById(R.id.reportAllHelp).setVisibility(isChecked ? View.GONE : View.VISIBLE);
                findViewById(R.id.useStartDate).setVisibility(isChecked ? View.GONE : View.VISIBLE);
                findViewById(R.id.startDate).setVisibility(isChecked ? View.GONE : View.VISIBLE);
                findViewById(R.id.useEndDate).setVisibility(isChecked ? View.GONE : View.VISIBLE);
                findViewById(R.id.endDate).setVisibility(isChecked ? View.GONE : View.VISIBLE);
            }

        });

        ((CheckBox) findViewById(R.id.useStartDate)).setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                final Button button = (Button) findViewById(R.id.startDate);
                button.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                button.setEnabled(isChecked);
            }

        });
        ((CheckBox) findViewById(R.id.useEndDate)).setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                final Button button = (Button) findViewById(R.id.endDate);
                button.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                button.setEnabled(isChecked);
            }

        });
        ((Button) findViewById(R.id.startDate)).setOnClickListener(new OnClickListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                showDialog(START_DATE_DIALOG_ID);
            }

        });
        ((Button) findViewById(R.id.endDate)).setOnClickListener(new OnClickListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                showDialog(END_DATE_DIALOG_ID);
            }

        });
        ((Button) findViewById(R.id.button_gen_report)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                generateReport();
            }

        });

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public SelectableListViewAdapter getSelectableAdapter() {
        return adapter;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        switch (id) {
            case START_DATE_DIALOG_ID:
                return new DatePickerDialog(this, getDatePickerListener(R.id.startDate), year, month, day);
            case END_DATE_DIALOG_ID:
                return new DatePickerDialog(this, getDatePickerListener(R.id.endDate), year, month, day);
        }
        return null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ACTIVITY_VIEW_REPORT:
                finish();
        }
    }

    private void generateReport() {
        boolean useStartDate = getCheckBoxState(R.id.useStartDate);
        boolean useEndDate = getCheckBoxState(R.id.useEndDate);
        Date startDate = (Date)((Button)findViewById(R.id.startDate)).getTag();
        Date endDate = (Date)((Button)findViewById(R.id.endDate)).getTag();

        Intent intent = new Intent(this, ViewReportActivity.class);
        intent.putExtra(IntentConstants.SELECTED, ((SelectableListViewAdapter)getListAdapter()).getSelected());
        intent.putExtra(IntentConstants.SHOW_REMAINING, getCheckBoxState(R.id.showRemaining));
        intent.putExtra(IntentConstants.REPORT_ALL, getCheckBoxState(R.id.reportAll));
        intent.putExtra(IntentConstants.SHOW_ADVENTURES_REQ, getCheckBoxState(R.id.showAdventuresReq));
        intent.putExtra(IntentConstants.SHOW_ADVENTURES_ELECT, getCheckBoxState(R.id.showAdventuresElect));
//        intent.putExtra(IntentConstants.SHOW_PINS_LOOPS, getCheckBoxState(R.id.showPinsLoops));
        intent.putExtra(IntentConstants.SHOW_WEBELOS_BADGES, getCheckBoxState(R.id.showWebelosBadges));
        if (useStartDate) {
            intent.putExtra(IntentConstants.START_DATE, startDate);
        }
        if (useEndDate) {
            intent.putExtra(IntentConstants.END_DATE, endDate);
        }
        startActivityForResult(intent, ACTIVITY_VIEW_REPORT);
    }

    private boolean getCheckBoxState(int id) {
        return ((CheckBox)findViewById(id)).isChecked();
    }

    private DatePickerDialog.OnDateSetListener getDatePickerListener(final int id) {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
                Button button = (Button) findViewById(id);
                Calendar c = new GregorianCalendar(selectedYear, selectedMonth, selectedDay);
                button.setTag(new Date(c.getTimeInMillis()));
                button.setText(new StringBuilder().append(selectedMonth + 1).append("-").append(selectedDay).append("-")
                        .append(selectedYear).append(" "));

                // set selected date into datepicker also
                view.init(selectedYear, selectedMonth, selectedDay, null);

            }
        };

        return listener;
    }
}
