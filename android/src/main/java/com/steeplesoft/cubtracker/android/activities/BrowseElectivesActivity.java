package com.steeplesoft.cubtracker.android.activities;

import com.steeplesoft.cubtracker.android.fragments.BrowseElectivesFragment;

public class BrowseElectivesActivity extends ExpandableListActivity {
    public BrowseElectivesActivity() {
        super(BrowseElectivesFragment.class);
    }
}
