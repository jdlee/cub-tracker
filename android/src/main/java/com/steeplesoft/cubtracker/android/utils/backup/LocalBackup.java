package com.steeplesoft.cubtracker.android.utils.backup;

import com.steeplesoft.cubtracker.android.Constants;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by jdlee on 8/22/14.
 */
public class LocalBackup implements BackupMethod {
    private String backupDir = Constants.DATABASE_PATH + "/backups";

    @Override
    public List<String> getFileList() {
        List<String> files = new ArrayList<String>();
        File dir = new File(backupDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        Collections.addAll(files, dir.list());
        return files;
    }

    @Override
    public void pruneBackups(int count) {

    }

    @Override
    public boolean upload(String file) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd.HH.mm");
        String remoteFile = "cubtracker-" + sdf.format(new Date()) +".db";

        return copyFile( Constants.LOCAL_PATH, backupDir + "/" + remoteFile);
    }

    @Override
    public boolean download(String file) {
        return copyFile(backupDir + "/" + file, Constants.LOCAL_PATH);
    }

    private boolean copyFile(String source, String dest) {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);

            byte[] buffer = new byte[16384];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return false;
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return true;
    }
}
