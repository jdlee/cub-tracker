package com.steeplesoft.cubtracker.android;

import com.steeplesoft.cubtracker.android.adapter.SelectableListViewAdapter;

/**
 * Created by jdlee on 1/6/14.
 */
public interface SelectableListActivity {
    SelectableListViewAdapter getSelectableAdapter();
}
