package com.steeplesoft.cubtracker.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.activities.ViewItemActivity;
import com.steeplesoft.cubtracker.android.adapter.ExpandableListAdapter;
import com.steeplesoft.cubtracker.android.adapter.util.ExpandListGroup;
import com.steeplesoft.cubtracker.android.adapter.util.ExpandListGroupRank;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Item;
import com.steeplesoft.cubtracker.android.model.Rank;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.util.ArrayList;
import java.util.List;

public class BrowseAchievementsFragment extends ExpandableListFragment {
    protected String itemType = "A";
    protected int beginningRank = 0;
    protected int endingRank = 5;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LayoutInflater li = (LayoutInflater) LayoutInflater.from(getActivity());
        return li.inflate(R.layout.list_expandable, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        mList = (ExpandableListView) getView().findViewById(android.R.id.list);
        mList.setTag("achievements");
        mAdapter = new ExpandableListAdapter(getActivity(), getGroupList());
//        mList.setAdapter(mAdapter);
        setListAdapter(mAdapter);

        mList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View view, int groupPosition, int childPosition,
                                        long id) {
                TextView tv = (TextView) view.findViewById(R.id.tvChild);
                Item item = (Item) tv.getTag();
                Intent intent = new Intent(view.getContext(), ViewItemActivity.class);
                intent.putExtra(IntentConstants.ITEM_ID, item.getId());

                startActivity(intent);
                return false;
            }
        });
    }

    protected List<ExpandListGroup> getGroupList() {
        List<ExpandListGroup> list = new ArrayList<ExpandListGroup>();

        final DatabaseAccess db = DatabaseAccess.getInstance();
        for (final Rank rank : db.getRanks(beginningRank, endingRank)) {
            ExpandListGroup group = new ExpandListGroupRank(rank, itemType);
            list.add(group);
        }

        return list;
    }
}
