package com.steeplesoft.cubtracker.android.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.jcraft.jsch.JSchException;
import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.utils.backup.BackupMethod;
import com.steeplesoft.cubtracker.android.utils.backup.FtpBackup;
import com.steeplesoft.cubtracker.android.utils.backup.LocalBackup;
import com.steeplesoft.cubtracker.android.utils.backup.ScpBackup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by jdlee on 2/12/14.
 */
public class BackupRestoreActivity extends ListActivity implements View.OnClickListener {
    private List<String> backupFiles = new ArrayList<String>();
    private String type;
    private String host;
    private String user;
    private String pass;
    private int count;
    private String selectedItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(Constants.LOG_TAG, "Creating activity: " + getClass().getSimpleName());
//        setContentView(R.layout.activity_backup);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        init();
    }

    protected void init() {
        try {
            if (loadPreferences()) {
                TextView header = new TextView(this);
                header.setText(getResources().getText(R.string.database_backups));
                header.setTextAppearance(this, android.R.style.TextAppearance_Large);
                header.setTypeface(Typeface.DEFAULT_BOLD, Typeface.BOLD);

                if (getListView().getHeaderViewsCount() == 0) {
                    getListView().addHeaderView(header);
                    getListView().addFooterView(getLayoutInflater().inflate(R.layout.backup_buttons, null));

                    ((Button) findViewById(R.id.button_backup)).setOnClickListener(this);
                    ((Button) findViewById(R.id.button_restore)).setOnClickListener(this);
                }

                populateUI();
            }
        } catch (JSchException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_backup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_backup_prefs:
                Intent intent = new Intent(this, BackupRestorePreferencesActivity.class);
                startActivityForResult(intent, Constants.ACTIVITY_BACKUP_PREFS);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.ACTIVITY_BACKUP_PREFS) {
            if (resultCode == RESULT_OK) {
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_backup:
                backup();
                break;
            case R.id.button_restore:
                restore();
                break;
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        ListAdapter listAdapter = getListAdapter();
        selectedItem = (String) listAdapter.getItem(position - 1);
//        Toast.makeText(this, item + " selected", Toast.LENGTH_LONG).show();
    }

    private void populateUI() throws JSchException {
        new FileListHandlerThread().execute();
        ListView listView = getListView();
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setSelector(R.color.Selected);
    }

    private void createListAdapter(List<String> fileList) {
        Collections.sort(fileList, Collections.reverseOrder());
        ArrayAdapter adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                fileList.toArray(new String[]{}));
        setListAdapter(adapter);
    }

    private boolean loadPreferences() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        type = prefs.getString("backupType", null);
        host = prefs.getString("backupHost", null);
        user = prefs.getString("backupUser", null);
        pass = prefs.getString("backupPass", null);
        count = Integer.parseInt(prefs.getString("backupCount", "1"));

        if (type == null || (!"Local".equals(type) && (host == null || user == null || pass == null))) {
            Util.displayMessage("You must first configure backups");
            startActivityForResult(new Intent(this, BackupRestorePreferencesActivity.class),
                    Constants.ACTIVITY_BACKUP_PREFS);
            return false;
        }

        return true;
    }

    private void backup() {
        BackupMethod backup = getBackupMethod();
        if (backup.upload(Constants.DATABASE_PATH + "/" + Constants.DATABASE_NAME)) {
            new FileListHandlerThread().execute();
        }
    }

    private void restore() {
        BackupMethod backup = getBackupMethod();
        if (backup.download(selectedItem)) {
            Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
    }

    private BackupMethod getBackupMethod() {
        if ("SCP".equals(type)) {
            return new ScpBackup(host, user, pass);
        }  else if ("FTP".equals(type)) {
            return new FtpBackup(host, user, pass);
        } else if ("Local".equals(type)) {
            return new LocalBackup();
        }

        return null;
    }

    private class FileListHandlerThread extends HandlerThread {
        protected Handler handler = null;

        public FileListHandlerThread() {
            super("FileListHandlerThread");
            start();
            handler = new Handler(getLooper());
        }

        public void execute() {
            Util.displayMessage("Getting list of backups.");
            handler.post(new Runnable() {
                @Override
                public void run() {
                    BackupMethod backup = getBackupMethod();
                    backup.pruneBackups(count);
                    final List<String> fileList = backup.getFileList();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            createListAdapter(fileList);
                        }
                    });
                }
            });
        }
    }
}
