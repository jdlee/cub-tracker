package com.steeplesoft.cubtracker.android.activities;

import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Item;
import com.steeplesoft.cubtracker.android.model.Part;
import com.steeplesoft.cubtracker.android.model.Rank;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.sql.SQLException;
import java.util.Date;

public class PartDetailActivity extends ListActivity {
    private Part part;
    private Item item;
    private SimpleCursorAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.part_details);

            final long partId = getIntent().getLongExtra(IntentConstants.PART_ID, -1);
            part = DatabaseAccess.getInstance().getPart(partId);
            item = (Item)getIntent().getSerializableExtra(IntentConstants.ITEM);

            if (part != null) {
                Util.setText(findViewById(R.id.itemTitle), item.getTitle());
                Util.setText(findViewById(R.id.partDetails), part.getDescription());
                Util.setText(findViewById(R.id.partInfo), Util.getPartInfo(part, item));

                Util.closeOldCursor((CursorAdapter) getListView().getAdapter());

                adapter = new SimpleCursorAdapter(this,
                        R.layout.list_item,
                        buildCursor(),
                        new String[]{"_id", "name"},
                        new int[]{android.R.id.text1, android.R.id.text1},
                        0);
//                listView.setAdapter(adapter);
                setListAdapter(adapter);

            }
        } catch (SQLException e) {

        }
        getListView().setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
                menu.setHeaderTitle("Completion Management");
                menu.add(0, Constants.CONTEXTMENU_DELETE_COMPLETION, 1, "Delete Completion");
            }
        });

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            updateList();
        } catch (SQLException e) {
            Log.e(Constants.LOG_TAG, e.getLocalizedMessage());
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.part_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.rec_complete:
                Intent intent = new Intent(this, SelectScoutActivity.class);
                intent.putExtra(IntentConstants.RANK_ID, item.getRank().getId());
                intent.putExtra(IntentConstants.PART_ID, part.getId());
                startActivityForResult(intent, Constants.ACTIVITY_RECORD_COMPLETION);
                break;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

//            case android.R.id.home:
//                Intent intent2 = new Intent(this, BrowsePartsActivity.class);
//                intent2.putExtra(IntentConstants.ITEM, item);
//                NavUtils.navigateUpTo(this,
//                        intent2);
////                        NavUtils.getParentActivityIntent(this));
        }

        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.ACTIVITY_RECORD_COMPLETION) {
            if (resultCode == RESULT_OK) {
                Date date = (Date)data.getSerializableExtra(IntentConstants.DATE);
                long[] scoutIds = data.getLongArrayExtra(IntentConstants.SELECTED);
                if (scoutIds != null) {
                    final DatabaseAccess db = DatabaseAccess.getInstance();
                    for (long scoutId : scoutIds) {
                        db.createPartCompletion(part.getId(), scoutId, date);
                    }
                    db.analzePartCompletions(scoutIds);
                    Util.closeOldCursor((CursorAdapter)getListAdapter());
                    adapter.changeCursor(buildCursor());
                }
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final Context context = this;
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        ListView list = (ListView)findViewById(android.R.id.list);
        Cursor c = (Cursor) list.getAdapter().getItem(info.position);
        final long id = c.getLong(c.getColumnIndex("_id"));
        switch (item.getItemId()) {
            case Constants.CONTEXTMENU_DELETE_COMPLETION:
                Util.showConfirmationDialog(this, "Are you sure you want to delete this completion?",
                        new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            try {
                                DatabaseAccess.getInstance().deletePartCompletion(id);
                                updateList();
                            } catch (SQLException e) {
                                Util.showAlertDialog(context, "Error!", "Unable to delete the record.");
                            }
                        }
                    }

                });
                break;
        }
        return true;
    }

    protected Cursor buildCursor() {
        final Rank rank = item.getRank();

        return DatabaseAccess.getInstance().getPartDetailCursor(part.getId(), (rank != null) ? rank.getId() : -1);
    }

    public void updateList() throws SQLException {
        Util.closeOldCursor((CursorAdapter)getListAdapter());
        adapter.changeCursor(buildCursor());
    }
}
