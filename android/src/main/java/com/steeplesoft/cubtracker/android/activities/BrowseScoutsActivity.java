package com.steeplesoft.cubtracker.android.activities;

import com.steeplesoft.cubtracker.android.fragments.BrowseScoutsFragment;

/**
 * Created by jdlee on 8/1/14.
 */
public class BrowseScoutsActivity extends ExpandableListActivity {
    public BrowseScoutsActivity() {
        super(BrowseScoutsFragment.class);
    }
}
