package com.steeplesoft.cubtracker.android;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.view.Gravity;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.db.DatabaseHelper;
import com.steeplesoft.cubtracker.android.model.Item;
import com.steeplesoft.cubtracker.android.model.Part;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA. User: jdlee Date: 7/14/12 Time: 7:46 PM To change this template use File | Settings |
 * File Templates.
 */
public class Util {
    public static DatabaseHelper databaseHelper;
    private static SimpleDateFormat source = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");
    private static SimpleDateFormat target = new SimpleDateFormat("yyyy-MM-dd");

    public static void viewFragment (FragmentManager fm, Fragment f) {
        fm.beginTransaction()
                .replace(R.id.content_frame, f)
                .addToBackStack(null)
                .commit();
    }

    public static void displayMessage(String message) {
        displayMessage(message, Toast.LENGTH_SHORT);
    }

    public static void displayMessage(String message, int length) {
        Toast toast = Toast.makeText(CubTracker.getAppContext(), message, length);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM, 0, 0);
        toast.show();
    }

    public static DatabaseHelper initHelper(Context context) {
        if (databaseHelper == null) {
            databaseHelper = new DatabaseHelper(context);
        }
        return databaseHelper;
    }

    public static DatabaseHelper getHelper() {
        return initHelper(CubTracker.getAppContext());
    }

    public static String getVersionString() {
        try {
            PackageManager manager = CubTracker.getAppContext().getPackageManager();
            PackageInfo info = manager.getPackageInfo(CubTracker.getAppContext().getPackageName(), 0);
            return info.versionName;
        } catch (NameNotFoundException e) {
            return "";
        }
    }

    public static String getText(View view) {
        if ((view != null) && (EditText.class.isAssignableFrom(view.getClass()))) {
            EditText et = (EditText) view;
            return et.getText().toString();
        }
        throw new RuntimeException("TextView not found");
    }

    public static void setText(View view, String text) {
        if (text == null) {
            text = "";
        }
        if ((view != null) && (TextView.class.isAssignableFrom(view.getClass()))) {
            ((TextView) view).setText(text.replace("\\n", "\n"));
        }
    }

    public static String getItemType(String code) {
        String type = null;
        if (Constants.TYPE_ACHIEVEMENT.equals(code)) {
            type = "Achievement";
        } else if (Constants.TYPE_ELECTIVE.equals(code)) {
            type = "Elective";
        } else if (Constants.TYPE_BADGE.equals(code)) {
            type = "Webelos Badge";
        } else if (Constants.TYPE_ADVENTURE_REQ.equals(code)) {
            type = "Required Adventure";
        } else if (Constants.TYPE_ADVENTURE_ELECTIVE.equals(code)) {
            type = "Elective Adventure";
        } else if (isBeltLoop(code)) {
            type = "Belt Loop";
        } else if (isPin(code)) {
            type = "Pin";
        }

        return type;
    }

    public static boolean isBeltLoop(String code) {
        return Constants.TYPE_LOOP.charAt(0) == code.charAt(0);
    }

    public static boolean isPin(String code) {
        return Constants.TYPE_PIN.charAt(0) == code.charAt(0);
    }

    public static String getPartInfo(Part part, Item item) throws SQLException {
        StringBuilder sb = new StringBuilder();
//        Part localPart = DatabaseAccess.getInstance().getPart(part.getId());
        if (item.getRank() != null) {
            sb.append(item.getRank().getName()).append(" ");
        }

        final String type = item.getType();
        final String number = item.getNumber();

        sb.append(Util.getItemType(type));
        if (number != null) {
            try {
                Integer.parseInt(number);
                sb.append(" #").append(number);
            } catch (NumberFormatException nfe) {
                // NaN
                sb.append(" ").append(number);
            }

        }
        if (("A".equals(type) || "E".equals(type)) && ((item.getRank().getId() > 0) && (item.getRank().getId() < 4))){
            sb.append(part.getNumber());
        }

        if (type.charAt(0) == 'L' || type.charAt(0) == 'P') {
            sb.append(" requirement #").append(part.getNumber());
        }

        return sb.toString();
    }

    /*
     * SQLite does terrible things to dates: it seems to store them as strings. To get the format we
     * want, then, we get to parse that string so that we can format it how we want. This ugly hack
     * does that. If there's an error converting the text, we'll just return the raw original.
     */
    public static String getDateString(String text) {
        // Thu Sep 19 00:00:00 CDT 2013
        if (text.charAt(4) == '-' && text.charAt(7) == '-') {
            return text.substring(0, 10);
        }

        try {

            Date date = source.parse(text);
            return target.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        // 2013-01-17 00:00:00.000000
        try {
            if ('-' == text.charAt(4)) {
                return text.substring(0,9);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }

    private static String[] months = { "January" };
    public static String formatDate(Date date) {
        return android.text.format.DateFormat.format("yyyy-MM-dd", date).toString();
    }

//    @SuppressLint("InlinedApi")
    public static Map<String, String> getContact(long id) {
        Map<String, String> data = new HashMap<String, String>();
        final String[] projection = new String[] {
                ContactsContract.CommonDataKinds.Identity._ID,
                ContactsContract.CommonDataKinds.Identity.DISPLAY_NAME
            };
        Cursor cursor =  CubTracker.getAppContext().getContentResolver()
                .query(Contacts.CONTENT_URI, projection,
                        Contacts._ID + " = ?",
                        new String[]{String.valueOf(id)},
                        ContactsContract.CommonDataKinds.Identity.DISPLAY_NAME);
        if (cursor.moveToFirst()) {
            for (String colName : cursor.getColumnNames()) {
                data.put(colName, cursor.getString(cursor.getColumnIndex(colName)));
            }
        } else {
            for (String colName : cursor.getColumnNames()) {
                data.put(colName, "UNKNOWN");
            }
        }
        cursor.close();
        return data;
    }

    public static String[] getContactNames(String[] ids) {
        long[] lids = new long[ids.length];
        for (int i = 0; i < ids.length; i++) {
            lids[i] = Long.parseLong(ids[i]);
        }

        return getContactNames(lids);
    }

    public static String[] getContactNames(String string) {
        if ((string == null) || string.isEmpty()) {
            return new String[]{};
        }
        return getContactNames(string.split(","));
    }

    @SuppressLint("InlinedApi")
    public static String[] getContactNames(long[] ids) {
        String[] names = new String[ids.length];
        for (int i = 0; i < ids.length; i++) {
            final Map<String, String> contact = Util.getContact(ids[i]);
            names[i] = contact.get(ContactsContract.CommonDataKinds.Identity.DISPLAY_NAME);
        }

        return names;
    }

    public static void showAlertDialog(final Context context, final String title, final String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new OnClickListener () {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }

        });
        builder.create().show();
    }

    public static void showConfirmationDialog(Context context, final String message, OnClickListener callback) {
        AlertDialog ad = new AlertDialog.Builder((context != null) ? context : CubTracker.getAppContext()).create();
        ad.setTitle("Confirmation");
        ad.setMessage(message);
        ad.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", callback);
        ad.setButton(DialogInterface.BUTTON_NEGATIVE, "No", callback);
//        ad.setIcon(android.R.drawable.alert_light_frame);
        ad.show();
    }

    public static SharedPreferences getSharedPrefs() {
        return PreferenceManager.getDefaultSharedPreferences(CubTracker.getAppContext());
    }

    @SuppressWarnings("deprecation")
    public static void scaleImage(Bitmap bitmap, ImageView imageView, float scale) {
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);

        int imageWidth = bitmap.getWidth();
        int imageHeight = bitmap.getHeight();

        // Create a new bitmap and convert it to a format understood by the ImageView
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, imageWidth, imageHeight, matrix, true);
        BitmapDrawable result = new BitmapDrawable(scaledBitmap);
        imageWidth = scaledBitmap.getWidth();
        imageHeight = scaledBitmap.getHeight();

        // Apply the scaled bitmap
        imageView.setImageDrawable(result);

        // Now change ImageView's dimensions to match the scaled image
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) imageView.getLayoutParams();
        params.width = imageWidth;
        params.height = imageHeight;
        imageView.setLayoutParams(params);

        imageView.setImageBitmap(scaledBitmap);
        imageView.setMaxWidth(scaledBitmap.getWidth());
    }

    /******************************************************************************************************************/
    // TODO: take the time to write one method to handle all types
    public static int getIntPref(String key, int defaultValue) {
        return getSharedPrefs().getInt(key, defaultValue);
    }

    public static void setIntPref(String key, int value) {
        SharedPreferences.Editor e = getSharedPrefs().edit();
        e.putInt(key, value);
        e.commit();
    }

    public static String getStringPref(String key, String defaultValue) {
        return getSharedPrefs().getString(key, defaultValue);
    }

    public static void setStringPref(String key, String value) {
        SharedPreferences.Editor e = getSharedPrefs().edit();
        e.putString(key, value);
        e.commit();
    }

    public static boolean getBooleanPref(String key, boolean defaultValue) {
        return getSharedPrefs().getBoolean(key, defaultValue);
    }

    public static void setBooleanPref(String key, boolean value) {
        SharedPreferences.Editor e = getSharedPrefs().edit();
        e.putBoolean(key, value);
        e.commit();
    }
    /******************************************************************************************************************/

    public static void closeOldCursor(CursorAdapter cursorAdapter) {
        if (cursorAdapter != null) {
            Cursor oldCursor = cursorAdapter.getCursor();
            if (oldCursor != null) {
                oldCursor.close();
            }
        }
    }

    public static void handleEmptyList(Activity activity, boolean isListEmpty) {
        if (isListEmpty) {
            activity.findViewById(android.R.id.empty).setVisibility(View.VISIBLE);
            activity.findViewById(android.R.id.list).setVisibility(View.GONE);
        }
    }

    public static String readFileAsset(int rid) {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(CubTracker.getAppContext().getResources().openRawResource(rid)));
            String line = reader.readLine();
            while (line != null) {
                sb.append(line).append("\n");
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException ioe) {

        }
        return sb.toString();
    }
}