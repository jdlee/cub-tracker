package com.steeplesoft.cubtracker.android.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.fragments.SelectContactFragment;

public class SelectContactsActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(Constants.LOG_TAG, "Creating activity: " + getClass().getSimpleName());

        setContentView(R.layout.fragment_layout);

        FragmentManager fmanager = getSupportFragmentManager();
        FragmentTransaction ftransaction = fmanager.beginTransaction();
        SelectContactFragment fragment = new SelectContactFragment();
        fragment.setSelectedContacts(getIntent().getStringExtra("selectedContacts"));
        ftransaction.add(R.id.frameLayout, fragment, "Select Contact");
        ftransaction.commit();

    }

}