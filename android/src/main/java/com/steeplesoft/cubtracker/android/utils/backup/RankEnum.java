package com.steeplesoft.cubtracker.android.utils.backup;

import com.steeplesoft.cubtracker.android.Constants;

/**
 * Created by jdlee on 8/21/14.
 */
public enum RankEnum {
    BOBCAT(Constants.RANK_BOBCAT, "Bobcat"),
    TIGER(Constants.RANK_TIGER, "Tiger"),
    WOLF(Constants.RANK_WOLF, "Wolf"),
    BEAR(Constants.RANK_BEAR, "Bear"),
    WEB1(Constants.RANK_WEB1, "Webelos 1"),
    WEB2(Constants.RANK_WEB2, "Webelos 2"),
    WEBELOS(Constants.RANK_WEBELOS, "Webelos"),
    ARROW_OF_LIGHT(Constants.RANK_ARROW_OF_LIGHT, "Arrow of Light");

    public final int rank;
    public final String name;

    RankEnum(int rank, String name) {
        this.rank = rank;
        this.name = name;
    }
}