package com.steeplesoft.cubtracker.android.adapter.util;

import com.steeplesoft.cubtracker.android.model.Part;

public class ExpandListChildPart implements ExpandListChild {
    protected Part part;

    public ExpandListChildPart(Part part) {
        super();
        this.part = part;
    }

    @Override
    public long getId() {
        return part.getId();
    }

    @Override
    public String getName() {
        return part.getNumber() + " - " + part.getDescription().replace("\\n", "\n");
    }

    @Override
    public void setName(String name) {
    }

    @Override
    public Object getTag() {
        return part;
    }

    @Override
    public void setTag(Object tag) {
    }
}
