package com.steeplesoft.cubtracker.android.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.CubTracker;
import com.steeplesoft.cubtracker.android.SelectableListActivity;
import com.steeplesoft.cubtracker.android.Util;
import com.steeplesoft.cubtracker.android.adapter.SelectableListViewAdapter;
import com.steeplesoft.cubtracker.android.adapter.SelectableListViewAdapter.ListItem;
import com.steeplesoft.cubtracker.android.adapter.util.ExpandListChild;
import com.steeplesoft.cubtracker.android.adapter.util.ExpandListGroup;
import com.steeplesoft.cubtracker.android.adapter.util.ExpandListGroupGroup;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Group;
import com.steeplesoft.cubtracker.android.model.Item;
import com.steeplesoft.cubtracker.android.model.Part;
import com.steeplesoft.cubtracker.android.utils.backup.IntentConstants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BrowsePartsActivity extends ListActivity implements SelectableListActivity {
//    private long itemId;
    private Item item;
    private List<ExpandListGroup> groupList;
    private SelectableListViewAdapter adapter;
    private Menu optionsMenu;

    @Override
    public SelectableListViewAdapter getSelectableAdapter() {
        return adapter;
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        if (bundle == null) {
            item = (Item) getIntent().getSerializableExtra(IntentConstants.ITEM);
        } else {
            item = (Item)bundle.getSerializable(IntentConstants.ITEM);
        }
        if (item != null) {
            setTitle("'" + item.getTitle() + "' Details");
            groupList = getGroupList();

            List<ListItem> items = new ArrayList<ListItem>();
            for (ExpandListGroup group : groupList) {
                items.add(new ListItem(-1, group.getName(), null, true));
                for (ExpandListChild i : group.getItems()) {
                    items.add(new ListItem(i.getId(), i.getName(), i.getTag(), false));
                }
            }
            adapter = new SelectableListViewAdapter(items, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    toggleMenuItemStatus();
                }
            });

            adapter.setLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(CubTracker.getAppContext(), PartDetailActivity.class);
                    intent.putExtra(IntentConstants.PART_ID, (Long) v.getTag());
                    intent.putExtra(IntentConstants.ITEM, item);
                    startActivity(intent);
                    return true;
                }
            });

            if (bundle != null) {
                adapter.setSelected(bundle.getLongArray(IntentConstants.SELECTED));
            }

            setListAdapter(adapter);
        } else {
            // Yuck
            try {
                finish();
            } catch (Exception e) {
            }
        }

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLongArray(IntentConstants.SELECTED, adapter.getSelected());
        outState.putSerializable(IntentConstants.ITEM, item);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            NavUtils.navigateUpTo(this, new Intent(this, MainActivity.class));
            return true;
        }

        switch (menuItem.getItemId()) {
            case R.id.rec_complete:
                Intent intent = new Intent(this, SelectScoutActivity.class);
//                intent.putExtra(IntentConstants.PARTS, adapter.getSelected());
                intent.putExtra(IntentConstants.ITEM_ID, item.getId());
                if (item.getRank() != null) {
                    intent.putExtra(IntentConstants.RANK_ID, item.getRank().getId());
                }
                startActivityForResult(intent, Constants.ACTIVITY_RECORD_COMPLETION);
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.part_detail, menu);
        optionsMenu = menu;
        optionsMenu.getItem(0).setEnabled(false);
        toggleMenuItemStatus();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        DatabaseAccess db = DatabaseAccess.getInstance();
        if (requestCode == Constants.ACTIVITY_RECORD_COMPLETION) {
            if (resultCode == RESULT_OK) {
                Date date = (Date)data.getSerializableExtra(IntentConstants.DATE);
                long[] scoutIds = data.getLongArrayExtra(IntentConstants.SELECTED);
                if (scoutIds != null) {
                    try {
                        for (long partId : adapter.getSelected()) {
                            Part part = db.getPart(partId);
                            if (part != null) {
                                for (long scoutId : scoutIds) {
                                    db.createPartCompletion(part.getId(), scoutId, date);
                                }
                            }
                        }
                        db.analzePartCompletions(scoutIds);
                        finish();
                        Util.displayMessage("Part completion has been recorded", Toast.LENGTH_LONG);
                    } catch (SQLException e) {
                        Log.e(Constants.LOG_TAG, e.getLocalizedMessage());
                    }
                }
            }
        }
    }

    private void toggleMenuItemStatus() {
        if (optionsMenu != null) {
            MenuItem mi = optionsMenu.getItem(0);
            if (mi != null) {
                mi.setEnabled(adapter.getSelected().length > 0);
            }
        }
    }

    protected List<ExpandListGroup> getGroupList() {
        List<ExpandListGroup> list = new ArrayList<ExpandListGroup>();

        try {
            for (final Group group : DatabaseAccess.getInstance().getAllGroups(item.getId())) {
                ExpandListGroup elg = new ExpandListGroupGroup(group);
                list.add(elg);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }
}
