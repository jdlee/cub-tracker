alter table items add group_count int;
alter table groups add part_count int;
alter table scouts add email_address text;
alter table scouts add phone_number text;
create table settings (setting_key char(50) primary key, setting_value text);

update items set group_count = 1;
update groups set part_count = 1;
insert into settings values ('db_version', '27');