SELECT DISTINCT
  scouts.name AS name,
  ranks.name AS rank,
  ranks._id,
  items.type as type,
  items.title as title,
  items.number AS inum,
  "timestamp" as completed,
  parts.number AS pnum,
  "p" as ctype
  --parts.description,
  --items._id, items.rank_id, items.number, items.notes, items.group_count, items.image
FROM scouts,
     items,
     groups,
     ranks,
     dens,
     completions,
     parts
WHERE scouts.den_id = dens._id
	AND (items.rank_id in (-1, 0) OR items.rank_id = dens.rank_id)
	AND dens.rank_id = ranks._id
	AND ranks._id < 6
	--1 AND scouts._id IN (17,21,27,38,40,41,43,44) 1--
	AND completions.scout_id = scouts._id
	AND completions.part_id = parts._id
	AND parts.group_id = groups._id
	AND groups.item_id = items._id
	AND items._id NOT IN
		(SELECT items._id
		 FROM items, item_completions
		 WHERE items._id = item_completions.item_id
		   AND item_completions.scout_id = scouts._id)
	--2 AND item_completions.completed >= '2014-10-16 00:00:00' 2--
	--3 AND item_completions.completed <= '2014-11-16 23:59:59' 3--
ORDER BY ctype,
        ranks._id,
	    scouts.name,
	    type,
	    inum,
	    pnum,
	    completed

