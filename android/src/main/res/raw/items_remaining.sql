SELECT DISTINCT
  scouts.name AS name,
  ranks.name AS rank,
  items.type,
  items.title,
  items.number AS rnum,
  items.number AS inum
FROM scouts,
     items,
     groups,
     ranks,
     dens
WHERE scouts.den_id = dens._id
AND (items.rank_id = -1 OR items.rank_id = dens.rank_id)
AND dens.rank_id = ranks._id
AND items.hidden = 0
AND ranks._id < 8
--1 AND scouts._id IN (1) 1--
AND items._id NOT IN (SELECT item_id
    FROM item_completions
    WHERE scout_id = scouts._id)
--4 AND items.type IN ('A', 'E') 4--
ORDER BY ranks._id, scouts.name, items.type, rnum
