SELECT DISTINCT
      scouts.name AS name,
      ranks.name AS rank,
      ranks._id,
      items.type as type,
      items.title as title,
      items.number AS inum,
      item_completions.completed as completed,
      null as pnum,
      "i" as ctype
FROM scouts,
     items,
     groups,
     ranks,
     dens,
     item_completions
WHERE scouts.den_id = dens._id
AND (items.rank_id in (-1, 0) OR items.rank_id = dens.rank_id)
AND dens.rank_id = ranks._id
AND items.hidden = 0
AND ranks._id < 8
--1 AND scouts._id IN (1) 1--
AND item_completions.scout_id = scouts._id
AND item_completions.item_id = items._id
--2 AND item_completions.completed >= '2014-10-16 00:00:00' 2--
--3 AND item_completions.completed <= '2014-11-16 23:59:59' 3--
AND item_completions.reported IS NULL
ORDER BY ctype,
    ranks._id,
    scouts.name, 
    ype,
    inum, 
    completed
