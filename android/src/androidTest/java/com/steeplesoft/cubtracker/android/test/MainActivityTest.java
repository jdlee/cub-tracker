package com.steeplesoft.cubtracker.android.test;

import android.database.Cursor;
import android.widget.EditText;

import com.robotium.solo.Solo;
import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.SelectableListActivity;
import com.steeplesoft.cubtracker.android.activities.BackupRestoreActivity;
import com.steeplesoft.cubtracker.android.activities.BackupRestorePreferencesActivity;
import com.steeplesoft.cubtracker.android.activities.BrowseAdventuresActivity;
import com.steeplesoft.cubtracker.android.activities.BrowseBadgesActivity;
import com.steeplesoft.cubtracker.android.activities.BrowseDensActivity;
import com.steeplesoft.cubtracker.android.activities.BrowseMeetingsActivity;
import com.steeplesoft.cubtracker.android.activities.BrowsePinsAndLoopsActivity;
import com.steeplesoft.cubtracker.android.activities.DenFormActivity;
import com.steeplesoft.cubtracker.android.activities.GenerateReportActivity;
import com.steeplesoft.cubtracker.android.activities.MainActivity;
import com.steeplesoft.cubtracker.android.activities.MeetingFormActivity;
import com.steeplesoft.cubtracker.android.activities.PartDetailActivity;
import com.steeplesoft.cubtracker.android.activities.ScoutFormActivity;
import com.steeplesoft.cubtracker.android.activities.ViewDenActivity;
import com.steeplesoft.cubtracker.android.activities.ViewItemActivity;
import com.steeplesoft.cubtracker.android.activities.ViewReportActivity;
import com.steeplesoft.cubtracker.android.activities.ViewScoutActivity;
import com.steeplesoft.cubtracker.android.adapter.SelectableListViewAdapter;
import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.model.Item;
import com.steeplesoft.cubtracker.android.model.Scout;
import com.steeplesoft.cubtracker.android.utils.backup.RankCheck;
import com.steeplesoft.cubtracker.android.utils.backup.RankEnum;

import junit.framework.Assert;

import org.junit.Ignore;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivityTest extends AbstractTestBase {

    public static final String PIC_BROWSE_MEETINGS = "browse_meetings";
    public static final String PIC_MEETING_FORM = "meeting_form";
    public static final String PIC_DELETE_MEETING = "meeting_menu";
    public static final String PIC_BROWSE_DENS = "browse_dens";
    public static final String PIC_DEN_MENU = "den_menu";
    public static final String PIC_BROWSE_MEETINGS_EXPANDED = "browse_meetings_expanded";
    public static final String PIC_DEN_CONFIRM_DELETE = "den_confirm_delete";
    public static final String PIC_MEETING_CONFIRM_DELETE = "meeting_confirm_delete";
    public static final String PIC_BACKUP_PREFERENCES = "backup_preferences";
    public static final String PIC_BROWSE_BACKUPS = "browse_backups";
    public static final String PIC_BROWSE_BACKUPS_WITH_ENTRY = "browse_backups_with_entry";
    public static final String PIC_VIEW_SCOUT = "view_scout";
    public static final String PIC_SCOUT_DELETE_CONFIRMATION = "scout_delete_confirmation";

    public void testHome() {
//        navigateToAdventure("Home", MainActivity.class);
//        solo.waitForText("Progress");
        takeScreenshot(PIC_MAIN_SCREEN);
    }

    public void testSearch() {
        solo.clickOnActionBarItem(R.id.menu_search);
        solo.waitForText("Search...");
        solo.enterText((EditText)solo.getView(R.id.searchText), "swim");
        solo.clickOnText("OK");
        Assert.assertTrue(solo.searchText("Salmon Run"));
    }

    public void testSearchCancel() {
        solo.clickOnActionBarItem(R.id.menu_search);
        solo.waitForText("Search...");
        solo.clickOnText("Cancel");
        Assert.assertTrue(solo.searchText("Progress"));
        Assert.assertFalse(solo.searchText("Search..."));
    }

    public void testHasWebelos() {
        addDen("Den 1", RankEnum.WOLF);
        assertFalse(db.hasScout(RankEnum.WEB2));
        addDen("Den 2", RankEnum.WEBELOS);
        assertFalse(db.hasScout(RankEnum.WEB2));
        addDen("Den 3", RankEnum.ARROW_OF_LIGHT);
        assertFalse(db.hasScout(RankEnum.WEB2));
        addDen("Den 4", RankEnum.WEB2);
        assertTrue(db.hasScout(RankEnum.WEB2));
    }

    public void testScoutSummaryScreenshot() {
        Den den = addDen("Den 1", RankEnum.WOLF);
        long scout1 = addScout(SCOUT_NAMES[0], den);
        long scout2 = addScout(SCOUT_NAMES[1], den);
        long scout3 = addScout(SCOUT_NAMES[2], den);
        db.createItemCompletion(6, scout1, new Date());
        db.createItemCompletion(7, scout1, new Date());
        db.createItemCompletion(8, scout1, new Date());
        db.createItemCompletion(6, scout2, new Date());
        db.createItemCompletion(7, scout2, new Date());
        db.createItemCompletion(7, scout3, new Date());

//        navigate("Meetings", BrowseMeetingsActivity.class);
        navigate("Home", MainActivity.class);

        takeScreenshot(PIC_MAIN_SCREEN_WITH_SUMMARY);
    }

    public void testGenerateReport() {
        Den den = addDen("Den 1", RankEnum.WOLF);
        long scout1 = addScout(SCOUT_NAMES[0], den);
        long scout2 = addScout(SCOUT_NAMES[1], den);
        long scout3 = addScout(SCOUT_NAMES[2], den);
        db.createItemCompletion(319, scout1, new Date());
        db.createItemCompletion(320, scout1, new Date());
        db.createItemCompletion(321, scout1, new Date());

        db.createItemCompletion(319, scout2, new Date());
        db.createItemCompletion(320, scout2, new Date());

        db.createItemCompletion(319, scout3, new Date());
        db.createPartCompletion(1660, scout3, new Date());
        db.createPartCompletion(1661, scout3, new Date());
        db.createPartCompletion(1662, scout3, new Date());
        db.createPartCompletion(1663, scout3, new Date());
        db.createPartCompletion(1664, scout3, new Date());

        navigate("Reports", GenerateReportActivity.class);

        solo.clickOnText(SCOUT_NAMES[0]);
        solo.clickOnText(SCOUT_NAMES[2]);

        solo.clickOnButton("Generate Report");
        solo.waitForActivity(ViewReportActivity.class);
        Assert.assertTrue(solo.searchText(SCOUT_NAMES[0]));
        Assert.assertTrue(solo.searchText(SCOUT_NAMES[2]));
        Assert.assertTrue(solo.searchText("Wolf Required Adventure a"));
        Assert.assertTrue(solo.searchText("Wolf Required Adventure b"));
        Assert.assertTrue(solo.searchText("Wolf Required Adventure c"));
        Assert.assertTrue(solo.searchText("Wolf Elective Adventure 1-1"));
        Assert.assertTrue(solo.searchText("Wolf Elective Adventure 1-2"));
        Assert.assertTrue(solo.searchText("Wolf Elective Adventure 1-3"));
        Assert.assertTrue(solo.searchText("Wolf Elective Adventure 1-4"));
        Assert.assertTrue(solo.searchText("Wolf Elective Adventure 1-5"));

        solo.clickOnActionBarItem(R.id.menu_send_report);
        solo.waitForText("Are you sure?");
        solo.clickOnText("Yes");
        solo.waitForText("No app");
        solo.goBack();

        navigate("Reports", GenerateReportActivity.class);
        solo.clickOnText("Show remaining items");
        solo.clickOnButton("Generate Report");
        solo.waitForActivity(ViewReportActivity.class);
        solo.goBack();
    }

    public void testMeetingLifecycle() {
        String scoutName1 = SCOUT_NAMES[0]; //"Meeting Scout 1";
        String scoutName2 = SCOUT_NAMES[1]; //"Meeting Scout 2";
        String scoutName3 = SCOUT_NAMES[2]; //"Meeting Scout 3";
        String scoutName4 = SCOUT_NAMES[3]; //"Meeting Scout 4";

        Den tigerDen = addDen("Den 1", RankEnum.TIGER);
        Den wolfDen = addDen("Den 2", RankEnum.WOLF);

        addScout(scoutName1, tigerDen);
        addScout(scoutName2, tigerDen);
        addScout(scoutName3, wolfDen);
        addScout(scoutName4, wolfDen);

        navigate("Meetings", BrowseMeetingsActivity.class);
        takeScreenshot(PIC_BROWSE_MEETINGS);

        solo.clickOnActionBarItem(R.id.menu_add_meeting); //"Add Meeting");
        solo.waitForActivity(MeetingFormActivity.class);
        takeScreenshot(PIC_MEETING_FORM);
        String meetingNotes = "Test Notes";
        solo.enterText(0, meetingNotes);
        solo.clickOnText(scoutName2);

        // Test rotation
        // TODO
        SelectableListViewAdapter adapter = ((SelectableListActivity) solo.getCurrentActivity()).getSelectableAdapter();
        long[] selected1 = adapter.getSelected();
        /*
        solo.setActivityOrientation(Solo.LANDSCAPE);
        adapter = ((SelectableListActivity) solo.getCurrentActivity()).getSelectableAdapter();
        long[] selected2 = adapter.getSelected();
        Assert.assertEquals(meetingNotes, solo.getEditText(0).getText().toString());
        Assert.assertTrue(compareLists(selected1, selected2));
        solo.setActivityOrientation(Solo.PORTRAIT);
        Assert.assertEquals(meetingNotes, solo.getEditText(0).getText().toString());
        */

        // Save meeting
        solo.clickOnText("OK");
        solo.waitForActivity(BrowseMeetingsActivity.class);

        String date = android.text.format.DateFormat.format("EEEEE, MMMMM d, yyyy", new Date()).toString();
        solo.clickOnText(tigerDen.getName());
        solo.waitForText(date);
        takeScreenshot(PIC_BROWSE_MEETINGS_EXPANDED);
        solo.clickOnText(date);
        solo.waitForText("Edit Meeting");
        Assert.assertEquals(meetingNotes, solo.getEditText(0).getText().toString());
        meetingNotes = "Updated Notes";
        solo.clearEditText(0);
        solo.enterText(0, meetingNotes);
        solo.clickOnText("OK");
        solo.waitForActivity(BrowseMeetingsActivity.class);

        solo.clickOnText(tigerDen.getName());
        solo.clickOnText(date);
        solo.waitForActivity(MeetingFormActivity.class);
        solo.waitForText(meetingNotes);
        adapter = ((SelectableListActivity) solo.getCurrentActivity()).getSelectableAdapter();
        Assert.assertEquals(meetingNotes, solo.getEditText(0).getText().toString());
        Assert.assertTrue(compareLists(selected1, adapter.getSelected()));
        solo.clickOnText("Cancel");

        // Delete meeting
        solo.clickOnText(tigerDen.getName());
        solo.clickLongOnText(date);
        solo.waitForText("Delete Meeting");
        takeScreenshot(PIC_DELETE_MEETING);
        solo.clickOnText("Delete Meeting");
        solo.waitForText("Confirmation");
        takeScreenshot(PIC_MEETING_CONFIRM_DELETE);
        solo.clickOnText("Yes");
        solo.waitForActivity(BrowseMeetingsActivity.class);

        Assert.assertFalse(solo.searchText(date));
        solo.clickOnText(tigerDen.getName());
        Assert.assertFalse(solo.searchText(date));
    }

    public void testAchievementRecording() throws SQLException {
        recordCompletionTest(NAV_SECTION_ADV,
                RankEnum.WOLF,
                "Duty to God Footsteps",
                BrowseAdventuresActivity.class);
    }

//    public void testWebelosBadgesRecording() throws SQLException {
//        recordCompletionTest("Webelos Badges",
//                RankEnum.WEB2,
//                "Aquanaut",
//                BrowseBadgesActivity.class);
//    }

    public void testElectivesRecording() throws SQLException {
        recordCompletionTest(NAV_SECTION_ADV_ELEC,
                RankEnum.WOLF,
                "Hometown Heroes",
                BrowseAdventuresActivity.class);
    }

    /*
    private void testPinsAndLoopsRecording() {
        recordCompletionTest("Pins and Loops",
                RankEnum.WOLF,
                "Pins",
                "Archery",
                new String[]{"1 - ", "2 - ", "3 - ", "4 - ", "5 - "},
                BrowsePinsAndLoopsActivity.class);
    }
    */

    public void testDenLifecycle() {
        String denName = "Den 1";

        navigate("Dens", BrowseDensActivity.class);

        addDenGui(denName, RankEnum.WOLF);
        takeScreenshot(PIC_BROWSE_DENS);

        // Edit
        solo.clickLongOnText(denName);
        solo.waitForText("Edit Den");
        takeScreenshot(PIC_DEN_MENU);
        solo.clickOnText("Edit Den");
        solo.waitForActivity(DenFormActivity.class);
        solo.clearEditText(0);
        solo.enterText(0, denName + " - edited");
        solo.pressSpinnerItem(0, 1);
        solo.clickOnText("Save");
        solo.waitForActivity(BrowseDensActivity.class);
        Assert.assertTrue(solo.searchText(denName + " - edited"));
        solo.clickOnText(denName + " - edited");
        solo.waitForActivity(ViewDenActivity.class);
        Assert.assertTrue(solo.searchText(denName + " - edited"));
        Assert.assertTrue(solo.searchText(RankEnum.BEAR.name));
        solo.goBack();
        solo.waitForActivity(BrowseDensActivity.class);

        // Delete
        solo.clickLongOnText(denName);
        solo.waitForText("Delete Den");
        solo.clickOnText("Delete Den");
        solo.waitForText("Are you sure");
        takeScreenshot(PIC_DEN_CONFIRM_DELETE);
        solo.clickOnText("Yes");
        solo.waitForActivity(BrowseDensActivity.class);
        Assert.assertFalse(solo.searchText(denName + " - edited"));
    }

    public void testTigerPercentComplete() {
        percentCompletionCheck(RankEnum.TIGER,
                new int[]{300, 301, 302, 303, 304, 305, 306},
                new int[]{300, 301, 302, 303, 306},
                5.0 / 7.0);
    }

    public void testWolfPercentComplete() {
        percentCompletionCheck(RankEnum.WOLF,
                new int[]{319, 320, 321, 322, 323, 324, 325},
                new int[]{319, 320, 321, 322, 325},
                5.0 / 7.0);
    }

    public void testBearPercentComplete() {
        percentCompletionCheck(RankEnum.BEAR,
                new int[]{338, 339, 340, 341, 342, 343, 344},
                new int[]{338, 339, 340, 341, 342},
                5.0 / 7.0);
    }

    public void testWebelosPercentComplete() {
        percentCompletionCheck(RankEnum.WEBELOS,
                new int[]{357, 358, 359, 360, 361, 366, 367},
                new int[]{357, 358, 359, 360, 366, 367, 368},
                6.0 / 7.0);
    }

    public void testArrowOfLightPercentComplete() {
        percentCompletionCheck(RankEnum.ARROW_OF_LIGHT,
                new int[]{362, 363, 364, 365, 366, 367, 368},
                new int[]{362, 363, 364, 365, 366},
                5.0 / 7.0);
    }

    public void testBackupAndRestore() {
        final String denName = "Backup Den";

        navigate("Backups", BackupRestorePreferencesActivity.class);
        takeScreenshot(PIC_BACKUP_PREFERENCES);
        solo.clickOnText("Backup type");
        solo.waitForText("Local");
        solo.clickOnText("Local");
        solo.waitForText("User name");
        solo.goBack();
        solo.waitForActivity(BackupRestoreActivity.class);
        takeScreenshot(PIC_BROWSE_BACKUPS);
        solo.clickOnButton("Backup");

        // This is different from the full backup name to make sure we don't have an issue with the minutes
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String backupName = "cubtracker-" + sdf.format(new Date());
        solo.waitForText(backupName);
        takeScreenshot(PIC_BROWSE_BACKUPS_WITH_ENTRY);
        solo.goBack();
        solo.waitForActivity(MainActivity.class);
        addDen(denName, RankEnum.WOLF);

        navigate("Dens", BrowseDensActivity.class);
        Assert.assertTrue(solo.searchText(denName));
        solo.goBack();

        navigate("Backups", BackupRestoreActivity.class);
        solo.waitForText(backupName);
        solo.clickOnText(backupName);
        solo.clickOnButton("Restore");
//        solo.goBack();
        solo.waitForActivity(MainActivity.class);
        Assert.assertTrue(solo.searchText("No scouts have been added to the system"));
    }

    public void testCompletionDeletion() {
        Den den = addDen("Wolf Den", RankEnum.WOLF);
        long scoutId1 = addScout(SCOUT_NAMES[0], den);

        db.createItemCompletion(319, scoutId1, new Date());
        db.createPartCompletion(1627, scoutId1, new Date());

        navigateToAdventure(RankEnum.WOLF.name, true);
        solo.waitForText("Call of the Wild");
        solo.clickOnText("Call of the Wild");
        solo.waitForActivity(ViewItemActivity.class);

        Assert.assertTrue(solo.searchText(SCOUT_NAMES[0]));
        solo.clickLongOnText(SCOUT_NAMES[0]);
        solo.waitForText("Delete Completion");
        takeScreenshot("delete_item_completion");
        solo.clickOnText("Delete Completion");
        solo.waitForText("Confirmation");
        solo.clickOnText("Yes");
        Assert.assertFalse(solo.searchText(SCOUT_NAMES[0]));

        solo.clickOnActionBarItem(R.id.view_details);
        solo.waitForText("1 -");
        solo.clickLongOnText("1 - ");
        solo.waitForActivity(PartDetailActivity.class);
        Assert.assertTrue(solo.searchText(SCOUT_NAMES[0]));
        solo.clickLongOnText(SCOUT_NAMES[0]);
        solo.waitForText("Delete Completion");
        takeScreenshot("delete_part_completion");
        solo.clickOnText("Delete Completion");
        solo.waitForText("Confirmation");
        solo.clickOnText("Yes");
        Assert.assertFalse(solo.searchText(SCOUT_NAMES[0]));
    }

    public void percentCompletionCheck(RankEnum rank, int[] completeList, int[] incompleteList, double incompletePct) {
        clearDatabase();
        String scout1 = SCOUT_NAMES[0]; //rank.name + " Scout 1";
        String scout2 = SCOUT_NAMES[1]; //rank.name + " Scout 2";
        Den den = addDen(rank.name + " Den", rank);
        addScout(scout1, den);
        addScout(scout2, den);

        recordItemCompletion(scout1, completeList);
        recordItemCompletion(scout2, incompleteList);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Scout complete = getScout(scout1);
        Scout incomplete = getScout(scout2);
        RankCheck check1 = new RankCheck(complete, db.getItemCompletionsForScout(complete.getId()));
        RankCheck check2 = new RankCheck(incomplete, db.getItemCompletionsForScout(incomplete.getId()));

        Assert.assertEquals(1.0, check1.getPercentComplete());
        Assert.assertEquals(incompletePct, check2.getPercentComplete());
    }

    public void testTigerScoutLifeCycle() {
        performScoutLifecycle(RankEnum.TIGER);
    }

    public void testWolfScoutLifeCycle() {
        performScoutLifecycle(RankEnum.WOLF);
    }

    public void testBearScoutLifeCycle() {
        performScoutLifecycle(RankEnum.BEAR);
    }

    public void testWebelosScoutLifeCycle() {
        performScoutLifecycle(RankEnum.WEBELOS);
    }

    public void testArrowOfLightScoutLifeCycle() {
        performScoutLifecycle(RankEnum.ARROW_OF_LIGHT);
    }

    protected void performScoutLifecycle(RankEnum rank) {
        String scoutName = SCOUT_NAMES[0]; //"Lifecycle Scout";
        String denName = "Den 1";

        navigate("Dens", BrowseDensActivity.class);

        addDenGui(denName, rank);
        Den den = getDen(denName);

        addScoutGui(scoutName, den);

        solo.clickOnText(scoutName);
        solo.waitForActivity(ViewScoutActivity.class);
//        solo.waitForText("View Scout");
        Assert.assertTrue(solo.searchText(scoutName));
        Assert.assertTrue(solo.searchText(TEST_EMAIL1));
        Assert.assertTrue(solo.searchText(TEST_PHONE1));
        takeScreenshot(PIC_VIEW_SCOUT);

        scoutName = "Edited " + scoutName;
        solo.clickOnActionBarItem(R.id.menu_edit_scout);
//        solo.clickOnText("Edit Scout");
        solo.waitForActivity(ScoutFormActivity.class);
        setText(0, scoutName);
        setText(1, TEST_EMAIL2);
        setText(2, TEST_PHONE2);
        solo.clickOnText("Save");
        solo.waitForActivity(ViewScoutActivity.class);
//        solo.waitForText("View Scout");
        Assert.assertTrue(solo.searchText(scoutName));
        Assert.assertTrue(solo.searchText(TEST_EMAIL2));
        Assert.assertTrue(solo.searchText(TEST_PHONE2));

        solo.clickOnActionBarItem(R.id.menu_delete_scout);
//        solo.clickOnText("Delete Scout");
        solo.waitForText("Confirmation");
        takeScreenshot(PIC_SCOUT_DELETE_CONFIRMATION);
        solo.clickOnText("Yes");
        solo.waitForActivity(ViewDenActivity.class);

        Assert.assertFalse("The scout '" + scoutName + "' should have been deleted but was still found.",
                solo.searchText(scoutName));
    }

    public void /*test*/ScoutFormRotation() {
        String scoutName = "Rotation Scout";
        solo.clickOnText("Add Scout");
        solo.waitForText("Linked Contacts");
        solo.enterText(0, scoutName);
        solo.pressSpinnerItem(0, 2);
        solo.setActivityOrientation(Solo.LANDSCAPE);
        Assert.assertEquals(scoutName, solo.getEditText(0).getText().toString());
        Assert.assertTrue(solo.isSpinnerTextSelected(0, "Wolf"));

    }

}