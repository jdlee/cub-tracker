package com.steeplesoft.cubtracker.android.test;

import android.annotation.TargetApi;
import android.app.Activity;
import android.database.Cursor;
import android.graphics.Point;
import android.os.Build;
import android.support.v4.widget.DrawerLayout;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Spinner;

import com.robotium.solo.Solo;
import com.steeplesoft.cubtracker.R;
import com.steeplesoft.cubtracker.android.Constants;
import com.steeplesoft.cubtracker.android.SelectableListActivity;
import com.steeplesoft.cubtracker.android.activities.BrowseAdventuresActivity;
import com.steeplesoft.cubtracker.android.activities.BrowseDensActivity;
import com.steeplesoft.cubtracker.android.activities.BrowsePartsActivity;
import com.steeplesoft.cubtracker.android.activities.DenFormActivity;
import com.steeplesoft.cubtracker.android.activities.MainActivity;
import com.steeplesoft.cubtracker.android.activities.PartDetailActivity;
import com.steeplesoft.cubtracker.android.activities.ScoutFormActivity;
import com.steeplesoft.cubtracker.android.activities.SelectScoutActivity;
import com.steeplesoft.cubtracker.android.activities.ViewDenActivity;
import com.steeplesoft.cubtracker.android.activities.ViewItemActivity;
import com.steeplesoft.cubtracker.android.adapter.ExpandableListAdapter;
import com.steeplesoft.cubtracker.android.adapter.SelectableListViewAdapter;
import com.steeplesoft.cubtracker.android.adapter.util.ExpandListGroup;
import com.steeplesoft.cubtracker.android.db.DatabaseAccess;
import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.model.Group;
import com.steeplesoft.cubtracker.android.model.Item;
import com.steeplesoft.cubtracker.android.model.Part;
import com.steeplesoft.cubtracker.android.model.Scout;
import com.steeplesoft.cubtracker.android.utils.backup.RankEnum;

import junit.framework.Assert;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by jdlee on 9/4/14.
 */
public abstract class AbstractTestBase extends ActivityInstrumentationTestCase2<MainActivity> {
    public static final String PIC_NAV_DRAWER = "nav_drawer";
    public static final String PIC_MAIN_SCREEN = "main_screen";
    public static final String PIC_MAIN_SCREEN_WITH_SUMMARY = "main_screen_with_summary";
    public static final String PIC_DEN_FORM = "den_form";
    public static final String PIC_BROWSE_BASE = "browse_";
    public static final String PIC_VIEW_ITEM = "view_item";
    public static final String PIC_SELECT_SCOUT = "select_scout";
    public static final String PIC_SELECT_SCOUT_WITH_SELECTION = "select_scout_with_selection";
    public static final String PIC_BROWSE_PARTS = "browse_parts";
    public static final String PIC_BROWSE_PARTS_WITH_SELECTION = "browse_parts_with_selection";
    public static final String PIC_VIEW_ITEM_WITH_COMPLETION = "view_item_with_completion";
    public static final String PIC_DATE_SELECT = "date_select";
    public static final String PIC_ADD_SCOUT_FORM = "add_scout_form";
    public static final String PIC_VIEW_DEN = "view_den";
    public static final String PIC_VIEW_DEN_WITH_SCOUT = "view_den_with_scout";
    public static final String[] SCOUT_NAMES = new String[] {
            "Joe Hardy",
            "Frank Hardy",
            "Tony Prito",
            "Chet Morton",
            "Biff Hooper"
    };

    protected Solo solo;
    protected DatabaseAccess db;
    protected static final String TEST_EMAIL1 = "test1@email.com";
    protected static final String TEST_EMAIL2 = "test2@email.com";
    protected static final String TEST_PHONE1 = "555-1234";
    protected static final String TEST_PHONE2 = "555-1235";
    protected static final String NAV_SECTION_ADV = "Adventures";
    protected static final String NAV_SECTION_ADV_ELEC = "Electives";



    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public AbstractTestBase() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Solo.Config config = new Solo.Config();
        config.screenshotFileType = Solo.Config.ScreenshotFileType.PNG;
//        config.screenshotSavePath = "/";
                //Environment.getExternalStorageDirectory() + "/Robotium/";
        this.solo = new Solo(getInstrumentation(), config, getActivity());

        while (solo.searchText("Welcome to Cub Tracker")) {
            Thread.sleep(100);
        }
        clearDatabase();

        if (solo.waitForText("New Version", 1, 7500)) {
            solo.clickOnCheckBox(0);
            solo.clickOnText("Cancel");
        }

        if (solo.waitForText("Change log", 1, 2500)) {
            solo.clickOnText("Close");
        }
    }

    protected void clearDatabase() {
        db = DatabaseAccess.getInstance();

        db.execSql("delete from completions");
        db.execSql("delete from item_completions");
        db.execSql("delete from attendees");
        db.execSql("delete from meetings");
        db.execSql("delete from scouts");
        db.execSql("delete from dens");
    }

    @Override
    protected void tearDown() throws Exception {
        db.execSql("delete from completions");
        db.execSql("delete from attendees");
        db.execSql("delete from meetings");
        db.execSql("delete from scouts");
        db.execSql("delete from dens");
        solo.finishOpenedActivities();
    }

    protected void addDenGui(final String denName, final RankEnum rank) {
        Log.d(Constants.LOG_TAG, "Adding den for rank " + rank.name);
        Class<? extends Activity> activity = solo.getCurrentActivity().getClass();
        solo.clickOnActionBarItem(R.id.menu_add_den); //"Add Den");
        solo.waitForActivity(DenFormActivity.class);
        solo.waitForText("Den Rank");
        takeScreenshot(PIC_DEN_FORM);
        solo.enterText(0, denName);
//        solo.pressSpinnerItem(0, rank.rank-1);
        final CountDownLatch done = new CountDownLatch(1);
        final Spinner spinner = (Spinner) solo.getView(R.id.spinner);
        solo.getCurrentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArrayAdapter adapter  = (ArrayAdapter) spinner.getAdapter();
                for (int i = 0; i< adapter.getCount(); i++) {
                    SelectableListViewAdapter.ListItem item = (SelectableListViewAdapter.ListItem) adapter.getItem(i);
                    System.out.println("item.name = " + item.name);
                    if (rank.name.equals(item.name)) {
                        spinner.setSelection(i);
                        break;
                    }
                }
                done.countDown();
            }
        });
        try {
            Log.d(Constants.LOG_TAG, "Waiting for latch...");
            done.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d(Constants.LOG_TAG, "Waiting for text...");
        solo.waitForText(rank.name);
        solo.clickOnText("Save");
        solo.waitForActivity(activity); //BrowseDensActivity.class);
        Assert.assertTrue(solo.searchText(denName));
    }

    protected void addScoutGui(String scoutName, Den desiredDen) {
        solo.clickOnText(desiredDen.getName());
        solo.waitForActivity(ViewDenActivity.class);
        takeScreenshot(PIC_VIEW_DEN);
        solo.clickOnActionBarItem(R.id.menu_add_scout); //"Add Scout");
        solo.waitForActivity(ScoutFormActivity.class);
        takeScreenshot(PIC_ADD_SCOUT_FORM);
        solo.enterText(0, scoutName);
        solo.enterText(1, TEST_EMAIL1);
        solo.enterText(2, TEST_PHONE1);
        /*
        Spinner spinner = solo.getView(Spinner.class, 0);
        for (int i = 0; i < spinner.getAdapter().getCount(); i++) {
            Den den = (Den)spinner.getItemAtPosition(i);
            if (den.equals(desiredDen)) {
                solo.pressSpinnerItem(0, i);
            }
        }
        */
        solo.clickOnText("Save");
        solo.waitForActivity(ViewDenActivity.class);
        Assert.assertTrue(solo.searchText(scoutName));
        takeScreenshot(PIC_VIEW_DEN_WITH_SCOUT);
    }

    protected Den addDen(String denName, RankEnum rank) {
        db.createDen(denName, rank.rank);
        return db.getDen(db.getLastId());
    }

    protected long addScout(String scoutName, Den den) {
        db.createScout(scoutName, den, TEST_EMAIL1, TEST_PHONE1, null, "");
        return db.getLastId();
    }
    
    protected void takeScreenshot(String name) {
        Thread.yield();
        solo.sleep(1000);
        solo.takeScreenshot(name, 50);
    }

    protected void recordCompletionTest(String type, RankEnum rank, String itemTitle, Class<? extends Activity> clazz) throws SQLException {
        solo.waitForText("Cub Tracker");
        String scout1 = SCOUT_NAMES[0]; //type + " Scout 1";
        String scout2 = SCOUT_NAMES[1]; //type + " Scout 2";
        String scout3 = SCOUT_NAMES[2]; //type + " Scout 3";
        String scout4 = SCOUT_NAMES[3]; //type + " Scout 4";
        Den goodDen = addDen("Den 1", rank);
        Den badDen = addDen("Den 2", RankEnum.TIGER);

        addScout(scout1, goodDen);
        addScout(scout2, goodDen);
        addScout(scout3, goodDen);
        addScout(scout4, badDen);

//        swipeLeft();
        if (type.equals(NAV_SECTION_ADV) || type.equals(NAV_SECTION_ADV_ELEC)) {
            navigateToAdventure(rank.name, type.equals(NAV_SECTION_ADV));
        } else {
            navigate(type, clazz);
        }
        Item item = getItemByTitle(itemTitle);
        takeScreenshot(PIC_BROWSE_BASE + type.toLowerCase().replace(" ", ""));
        solo.waitForText(item.getTitle());
        solo.clickOnText(item.getTitle());
        solo.waitForActivity(ViewItemActivity.class);
        takeScreenshot(PIC_VIEW_ITEM);
        solo.clickOnActionBarItem(R.id.rec_complete);
        solo.waitForActivity(SelectScoutActivity.class);
        takeScreenshot(PIC_SELECT_SCOUT);
        solo.clickOnText(scout2);
        takeScreenshot(PIC_SELECT_SCOUT_WITH_SELECTION);
        solo.clickOnButton("OK");
        solo.waitForText("Done");
        solo.clickOnButton("Done");

        solo.waitForActivity(ViewItemActivity.class);
        Assert.assertTrue(solo.searchText(scout2));
        takeScreenshot(PIC_VIEW_ITEM_WITH_COMPLETION);

        solo.clickOnActionBarItem(R.id.view_details); //"View Details");
        solo.waitForActivity(BrowsePartsActivity.class);
        takeScreenshot(PIC_BROWSE_PARTS);

//        Part part = item.getGroups().get(0).getParts().get(0);
        List<Group> groups = db.getAllGroups(item.getId());
        for (Group g : groups) {
            List<Part> parts = db.getPartsForGroup(g.getId());
            for (Part p : parts) {
                solo.clickOnText(p.getNumber() + " - ");
            }
        }
        takeScreenshot(PIC_BROWSE_PARTS_WITH_SELECTION);
        SelectableListViewAdapter adapter = ((SelectableListActivity) solo.getCurrentActivity()).getSelectableAdapter();
        long[] selected1 = adapter.getSelected();

        // Test rotation
        solo.setActivityOrientation(Solo.LANDSCAPE);
        adapter = ((SelectableListActivity) solo.getCurrentActivity()).getSelectableAdapter();
        Assert.assertTrue(compareLists(selected1, adapter.getSelected()));
        solo.setActivityOrientation(Solo.PORTRAIT);

        solo.waitForText(getActivity().getResources().getString(R.string.rec_complete));
        solo.clickOnActionBarItem(R.id.rec_complete); //"Record Completion");
        solo.waitForActivity(SelectScoutActivity.class);

        Assert.assertFalse(solo.searchText(scout4)); // "badDen" scout shouldn't be shown

        solo.clickOnText(scout1);
        solo.clickOnText(scout3);
        solo.clickOnButton("OK");
        solo.waitForText("Done");
        takeScreenshot(PIC_DATE_SELECT);
        solo.clickOnButton("Done");
//        Assert.assertTrue(solo.searchText("has been recorded"));
        solo.waitForActivity(ViewItemActivity.class);
        Assert.assertTrue(solo.searchText(scout1));
        Assert.assertTrue(solo.searchText(scout2));
        Assert.assertTrue(solo.searchText(scout3));

        solo.clickOnActionBarItem(R.id.view_details);
        solo.waitForActivity(BrowsePartsActivity.class);
        solo.clickLongOnText(db.getPartsForGroup(groups.get(0).getId()).get(0).getNumber() + " - ");
        solo.waitForActivity(PartDetailActivity.class);
        Assert.assertTrue(solo.searchText(scout1));
        Assert.assertTrue(solo.searchText(scout3));
    }

    protected void recordCompletionTest(String type,
                                        RankEnum rank,
                                        String group,
                                        String itemText,
                                        String[] partText,
                                        Class<? extends Activity> clazz) {
        String scout1 = SCOUT_NAMES[0]; //type + " Scout 1";
        String scout2 = SCOUT_NAMES[1]; //type + " Scout 2";
        String scout3 = SCOUT_NAMES[2]; //type + " Scout 3";
        String scout4 = SCOUT_NAMES[3]; //type + " Scout 4";
        Den goodDen = addDen("Den 1", rank);
        Den badDen = addDen("Den 2", RankEnum.TIGER);

        addScout(scout1, goodDen);
        addScout(scout2, goodDen);
        addScout(scout3, goodDen);
        addScout(scout4, badDen);

//        swipeLeft();
        if (type.equals(NAV_SECTION_ADV) || type.equals(NAV_SECTION_ADV_ELEC)) {
            navigateToAdventure(rank.name, type.equals(NAV_SECTION_ADV));
        } else {
            navigate(type, clazz);
        }
        takeScreenshot(PIC_BROWSE_BASE + type.toLowerCase().replace(" ", ""));

        if (group != null) {
            expandGroup(group);
            takeScreenshot(PIC_BROWSE_BASE + type.toLowerCase() + "_expanded");
        }

        solo.waitForText(itemText);
        solo.clickOnText(itemText);
        solo.waitForActivity(ViewItemActivity.class);
        takeScreenshot(PIC_VIEW_ITEM);
        solo.clickOnActionBarItem(R.id.rec_complete);
        solo.waitForActivity(SelectScoutActivity.class);
        takeScreenshot(PIC_SELECT_SCOUT);
        solo.clickOnText(scout2);
        takeScreenshot(PIC_SELECT_SCOUT_WITH_SELECTION);
        solo.clickOnButton("OK");
        solo.waitForText("Done");
        solo.clickOnButton("Done");
//        Assert.assertTrue(solo.searchText("has been recorded"));
        solo.waitForActivity(ViewItemActivity.class);
        Assert.assertTrue(solo.searchText(scout2));
        takeScreenshot(PIC_VIEW_ITEM_WITH_COMPLETION);

        solo.clickOnActionBarItem(R.id.view_details); //"View Details");
        solo.waitForActivity(BrowsePartsActivity.class);
        takeScreenshot(PIC_BROWSE_PARTS);
        for (String t : partText) {
            solo.clickOnText(t);
        }
        takeScreenshot(PIC_BROWSE_PARTS_WITH_SELECTION);
        SelectableListViewAdapter adapter = ((SelectableListActivity) solo.getCurrentActivity()).getSelectableAdapter();
        long[] selected1 = adapter.getSelected();

        // Test rotation
        solo.setActivityOrientation(Solo.LANDSCAPE);
        adapter = ((SelectableListActivity) solo.getCurrentActivity()).getSelectableAdapter();
        Assert.assertTrue(compareLists(selected1, adapter.getSelected()));
        solo.setActivityOrientation(Solo.PORTRAIT);

        solo.waitForText(getActivity().getResources().getString(R.string.rec_complete));
        solo.clickOnActionBarItem(R.id.rec_complete); //"Record Completion");
        solo.waitForActivity(SelectScoutActivity.class);
        if (!"Pins".equals(group)) {
            Assert.assertFalse(solo.searchText(scout4)); // "badDen" scout shouldn't be shown
        }
        solo.clickOnText(scout1);
        solo.clickOnText(scout3);
        /*
        adapter = ((SelectableListActivity) solo.getCurrentActivity()).getSelectableAdapter();
        long[] selectedScouts1 = adapter.getSelected();
        solo.setActivityOrientation(Solo.LANDSCAPE);
        adapter = ((SelectableListActivity) solo.getCurrentActivity()).getSelectableAdapter();
        Assert.assertTrue(compareLists(selectedScouts1, adapter.getSelected()));
        solo.setActivityOrientation(Solo.PORTRAIT);
        */
        solo.clickOnButton("OK");
        solo.waitForText("Done");
        takeScreenshot(PIC_DATE_SELECT);
        solo.clickOnButton("Done");
//        Assert.assertTrue(solo.searchText("has been recorded"));
        solo.waitForActivity(ViewItemActivity.class);
        Assert.assertTrue(solo.searchText(scout1));
        Assert.assertTrue(solo.searchText(scout2));
        Assert.assertTrue(solo.searchText(scout3));

        solo.clickOnActionBarItem(R.id.view_details);
        solo.waitForActivity(BrowsePartsActivity.class);
        solo.clickLongOnText(partText[0]);
        solo.waitForActivity(PartDetailActivity.class);
        Assert.assertTrue(solo.searchText(scout1));
        Assert.assertTrue(solo.searchText(scout3));


//        testGenerateReport();
    }

    protected void expandGroup(String group) {
        final ExpandableListView elv = solo.getView(ExpandableListView.class, 0);
        ExpandableListAdapter ela = (ExpandableListAdapter) elv.getExpandableListAdapter();
        for (int i = 0; i < ela.getGroupCount(); i++) {
            ExpandListGroup elg = (ExpandListGroup) ela.getGroup(i);
            if (elg.getName().equals(group)) {
                final int pos = i;
                solo.getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        elv.expandGroup(pos);
                    }
                });
            }
        }
    }

    protected void openNavDrawer() {
        solo.getCurrentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DrawerLayout view = (DrawerLayout) solo.getView(R.id.drawer_layout);
                view.openDrawer(Gravity.LEFT);
            }
        });
        /*
        sleep(0.5d);
        Point deviceSize = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(deviceSize);

        int screenWidth = deviceSize.x;
        int screenHeight = deviceSize.y;
        int fromX = 0;
        int toX = screenWidth / 2;
        int fromY = screenHeight / 2;
        int toY = fromY;

        solo.drag(fromX, toX, fromY, toY, 1);
        sleep(0.5d);
        */
    }

    private void sleep(double time) {
        try {
            Thread.sleep((long)time *1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void navigateToAdventure(String text, boolean required) {
        openNavDrawer();
        solo.waitForText(text);
        takeScreenshot(PIC_NAV_DRAWER);

        final String tag = "adv_" + text.toLowerCase() + "_" + required;
        List<View> views = getViewsByTag((ViewGroup)solo.getCurrentActivity().findViewById(R.id.navigation_drawer), tag);
        View view = views.get(0);
        solo.clickOnView(view);
        solo.waitForActivity(BrowseAdventuresActivity.class);
    }

    protected void navigate(String text, Class<? extends Activity> activity) {
        openNavDrawer();
        solo.waitForText(text);
        takeScreenshot(PIC_NAV_DRAWER);
        solo.clickOnText(text);
//        solo.sleep(10);
        solo.waitForActivity(activity);
    }

    protected void clickOnMenuItem1(int id) {
        solo.clickOnMenuItem(getActivity().getResources().getString(id));
//        ActionMenuItemView item = (ActionMenuItemView) solo.getCurrentActivity().findViewById(id);
//        if (item != null) {
//            item.callOnClick();
//        }
//        solo.clickOnActionBarItem(id);
//        try {
//            solo.clickOnActionBarItem(text);
//        } catch (junit.framework.AssertionFailedError e) {
//            solo.clickOnText(text);
//        }
    }

    protected void swipeRight() {
        solo.drag(100, 700, 600, 600, 1);
    }

    protected void swipeLeft() {
        solo.drag(700, 100, 600, 600, 1);
    }

    protected void getListViewCount() {
        log("************* Number of ListViews: " + solo.getCurrentViews(ListView.class).size());
    }

    protected void log(String message) {
        Log.d("CubTrackerTest", message);
    }

    protected boolean compareLists(long[] expectedArray, long[] actualArray) {
        Arrays.sort(expectedArray);
        Arrays.sort(actualArray);
        return Arrays.equals(expectedArray, actualArray);
    }

    protected void setText(int field, String text) {
        solo.clearEditText(field);
        solo.enterText(field, text);
    }

    protected Scout getScout(String name) {
        Scout scout = null;
        Cursor c = db.executeSelectStatement("SELECT _id FROM scouts WHERE name = ?", new String[] {name});
        c.moveToFirst();
        if (!c.isAfterLast()) {
            long scoutId = c.getLong(0);
            scout = db.getScout(scoutId);
        }
        c.close();

        return scout;
    }

    protected void recordItemCompletion (String scoutName, int... items) {
        Scout scout = getScout(scoutName);
        for (int itemId : items) {
            db.createItemCompletion(itemId, scout.getId(), new Date());
        }
    }

    private ArrayList<View> getViewsByTag(ViewGroup root, String tag){
        ArrayList<View> views = new ArrayList<View>();
        final int childCount = root.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = root.getChildAt(i);
            if (child instanceof ViewGroup) {
                views.addAll(getViewsByTag((ViewGroup) child, tag));
            }

            final Object tagObj = child.getTag();
            if (tagObj != null && tagObj.equals(tag)) {
                views.add(child);
            }

        }
        return views;
    }

    protected Item getItemByTitle(String title) {
        Cursor cursor = db.executeSelectStatement("SELECT _id, type, rank_id, number, title, notes, image FROM items WHERE title = ?",
                new String[] {title});
        Item item = null;
        if (!cursor.isAfterLast()) {
            item = db.getItemFromCursor(cursor);
        }
        cursor.close();
        return item;

    }

    public Den getDen(String name) {
        Cursor cursor = db.executeSelectStatement("SELECT _id, name, rank_id FROM dens WHERE name = ?",
                new String[] {name});
        Den den = null;
        if (!cursor.isAfterLast()) {
            den = new Den(cursor.getLong(cursor.getColumnIndex("_id")),
                    db.getRank(cursor.getLong(cursor.getColumnIndex("rank_id"))),
                    cursor.getString(cursor.getColumnIndex("name")));
        }
        cursor.close();
        return den;
    }

}
