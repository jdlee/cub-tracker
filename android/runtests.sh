#!/bin/bash

export ANDROID_HOME=`cat local.properties | grep sdk | cut -f 2 -d =`
#export PATH=$PATH:"$ANDROID_HOME/platform-tools":"$ANDROID_HOME/tools"

function status() {
	STATUS=`$ANDROID_HOME/platform-tools/adb -e shell getprop dev.bootcomplete 2>/dev/null`
	echo ${STATUS:0:1}
}

function pause() {
    read -p "Press enter:"
}

function capture_log() {
    $ANDROID_HOME/platform-tools/adb logcat > tests-$1.log
}

function testsuite() {
    AVD=$1
    echo "hw.gpu.enabled=true" >> $HOME/.android/avd/$AVD.avd/config.ini 
    $ANDROID_HOME/platform-tools/adb emu kill
    $ANDROID_HOME/tools/emulator64-x86 -gpu off -no-boot-anim -avd $AVD -qemu -m 512 & # -enable-kvm 2>&1 >> emulator-$AVD.log &
    sleep 5
    echo Waiting for emulator to be ready...
    $ANDROID_HOME/platform-tools/adb wait-for-device

    capture_log $AVD &
    ./gradlew clean connectedAndroidTest

    RESULT=$?
    
    rm -rf build/screenshots/$AVD
    mkdir -p build/screenshots/$AVD 2>/dev/null
    for FILE in `$ANDROID_HOME/platform-tools/adb shell ls /sdcard/Robotium-Screenshots/` ; do 
        FILE=`echo $FILE | tr -d '$\r '`
        PNG=/sdcard/Robotium-Screenshots/$FILE 
        echo Copying screenshot $FILE
        $ANDROID_HOME/platform-tools/adb pull $PNG build/screenshots/$AVD &> /dev/null
    done
    $ANDROID_HOME/platform-tools/adb emu kill
    $ANDROID_HOME/tools/android delete avd -n $AVD

    if [ $RESULT != 0 ] ; then
        exit $RESULT
    fi
}

$ANDROID_HOME/tools/android delete avd -n TestAVD4 &>/dev/null
$ANDROID_HOME/tools/android delete avd -n TestAVD7 &>/dev/null
$ANDROID_HOME/tools/android -s create avd -f -t "android-16" -n TestAVD4 -d "Nexus 4" -c 200M -s 768x1280 -b "default/x86" &> emulator-TestAVD4.log
$ANDROID_HOME/tools/android -s create avd -f -t "android-16" -n TestAVD7 -d "Nexus 7" -c 200M -s 800x1280 -b "default/x86" &> emulator-TestAVD7.log

if [ "$1" == "-4" ]; then
    testsuite TestAVD4
else
    testsuite TestAVD7
fi
