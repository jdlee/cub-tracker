/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.cubtracker.web;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.testng.annotations.BeforeSuite;

/**
 *
 * @author jdlee
 */
public class DatabaseSetupTest  {
    
    @BeforeSuite
    public void blah() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("cubtracker_test");
        EntityManager em = emf.createEntityManager();
        
        em.close();
        emf.close();
    }
}
