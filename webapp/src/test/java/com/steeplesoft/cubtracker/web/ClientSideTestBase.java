package com.steeplesoft.cubtracker.web;

import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.model.Rank;
import com.steeplesoft.cubtracker.android.model.Scout;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jettison.json.JSONException;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

/**
 *
 * @author jdlee
 */
@RunAsClient
public class ClientSideTestBase extends TestBase {

    @ArquillianResource
    protected java.net.URL url;
    protected final Client client = ClientBuilder.newClient();

    @AfterMethod
    protected void cleanup() {
        try {
            for (Scout scout : getScouts()) {
                try {
                    delete(basePath().path("completions").path("parts").path(""+scout.getId()));
                } catch (URISyntaxException ex) {
                }
                try {
                    delete(basePath().path("completions").path("items").path(""+scout.getId()));
                } catch (URISyntaxException ex) {
                }
                try {
                    delete(basePath().path("scouts").path(""+scout.getId()));
                } catch (Exception ex) {
                }
            }
            for (Den den : getDens()) {
                try {
                    delete(basePath().path("dens").path(""+den.getId()));
                } catch (Exception ex) {
                }
            }
        } catch (Exception ex) {
        }
    }

    protected WebTarget basePath() throws URISyntaxException {
        return client.target(url.toURI()).path("api");
    }

    protected <T> Response create(WebTarget target, T entity) {
        Response r = target.request(MediaType.APPLICATION_JSON).post(Entity.entity(entity, MediaType.APPLICATION_JSON), Response.class);
        Assert.assertEquals(r.getStatus(), 201);

        return r;
    }

    protected Response getResponse(WebTarget target) {
        return target.request().accept(MediaType.APPLICATION_JSON).get(Response.class);
    }
    
    protected <T> T get(WebTarget target, Class<T> clazz) {
        Response r = target.request().accept(MediaType.APPLICATION_JSON).get(Response.class);
        Assert.assertEquals(r.getStatus(), 200);
        return r.readEntity(clazz);
    }

    protected <T> Response update(WebTarget target, T entity) {
        Response r = target.request(MediaType.APPLICATION_JSON).post(Entity.entity(entity, MediaType.APPLICATION_JSON), Response.class);
        Assert.assertEquals(r.getStatus(), 200);

        return r;
    }

    protected void delete(WebTarget target) {
        Response r = target.request().accept(MediaType.APPLICATION_JSON).delete(Response.class);
        Assert.assertEquals(r.getStatus(), 200);
    }

    protected List<Rank> getRanks() throws URISyntaxException, JSONException {
        WebTarget target = basePath().path("ranks");
        Response r = target.request().accept(MediaType.APPLICATION_JSON).get(Response.class);
        Assert.assertEquals(r.getStatus(), 200);
        return r.readEntity(new GenericType<List<Rank>>() {
        });
    }

    protected List<Den> getDens() throws URISyntaxException {
        WebTarget target = basePath().path("dens");
        Response r = target.request().accept(MediaType.APPLICATION_JSON).get(Response.class);
        Assert.assertEquals(r.getStatus(), 200);
        return r.readEntity(new GenericType<List<Den>>() {
        });
    }

    protected Den createDen(String name, int rankId) throws JSONException, URISyntaxException {
        Den den = new Den();
        den.setName(name);
        den.setRank(getRanks().get(rankId));
        den = get(client.target(create(basePath().path("dens"), den).getHeaderString("Location")),
                Den.class);
        return den;
    }

    protected List<Scout> getScouts() throws URISyntaxException {
        WebTarget target = basePath().path("scouts");

        Response response = target.request().accept(MediaType.APPLICATION_JSON).get();
        Assert.assertNotNull(response);
        return response.readEntity(new GenericType<List<Scout>>() {});
    }

    protected Scout createScout(Scout scout) throws URISyntaxException {
        Response r = create(basePath().path("scouts"), scout);
        Assert.assertEquals(r.getStatus(), 201);
        String location = r.getHeaderString("Location");
        Assert.assertNotNull(location);
        return get(client.target(location), Scout.class);
    }
}
