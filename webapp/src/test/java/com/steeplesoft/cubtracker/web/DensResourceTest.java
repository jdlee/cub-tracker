/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.cubtracker.web;

import com.steeplesoft.cubtracker.android.model.Den;
import java.net.URISyntaxException;
import javax.ws.rs.core.Response;
import org.codehaus.jettison.json.JSONException;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author jdlee
 */
public class DensResourceTest extends ClientSideTestBase {
    @Test
    public void testGetDens() throws URISyntaxException, JSONException {
        getDens();
    }
    
    @Test
    public void createDen() throws URISyntaxException, JSONException {
        Den den = new Den();
        den.setName("Test Den");
        den.setRank(getRanks().get(1));
        
        Response r = create(client.target(url.toURI()).path("api").path("dens"), den);
        String location = r.getHeaderString("Location");
        Assert.assertNotNull(location);
        Den check = get(client.target(location), Den.class);
        Assert.assertEquals(check.getName(), den.getName());
        delete(client.target(location));
    }
    
    @Test
    public void updateDen() throws URISyntaxException, JSONException {
        Den den = new Den();
        den.setName("Test Den");
        den.setRank(getRanks().get(1));
        
        Response r = create(client.target(url.toURI()).path("api").path("dens"), den);
        String location = r.getHeaderString("Location");
        Assert.assertNotNull(location);
        Den update = get(client.target(location), Den.class);
        update.setName("Updated Den");
        update(client.target(location), update);
        
        Den check = get(client.target(location), Den.class);
        Assert.assertNotEquals(check.getName(), den.getName());
        Assert.assertEquals(check.getName(), update.getName());
        
        delete(client.target(location));
    }
}
