/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.cubtracker.web;

import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.model.Meeting;
import com.steeplesoft.cubtracker.android.model.Scout;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author jdlee
 */
public class MeetingsResourceTest extends ClientSideTestBase {
    @Test
    public void getMeetings() throws URISyntaxException {
        WebTarget target = basePath().path("dens");
        Response r = target.request().accept(MediaType.APPLICATION_JSON).get(Response.class);
        Assert.assertEquals(r.getStatus(), 200);
        List<Meeting> meetings = r.readEntity(new GenericType<List<Meeting>>(){});
        Assert.assertNotNull(meetings);
    }

    @Test
    public void createMeeting() throws URISyntaxException, JSONException {
        Meeting meeting = new Meeting();
        meeting.setRank(getRanks().get(1));
        meeting.setNotes("Test notes");
        meeting.setAttendees(buildAttendeeList());
        
        Response r = create(basePath().path("meetings"), meeting);
        String location = r.getHeaderString("Location");
        Assert.assertNotNull(location);
        Meeting check = get(client.target(location), Meeting.class);
        Assert.assertEquals(check.getMeetingDate(), meeting.getMeetingDate());
        Assert.assertEquals(check.getNotes(), meeting.getNotes());
        delete(client.target(location));
        for (Scout attendee : check.getAttendees()) {
            delete (basePath().path("scouts").path(""+attendee.getId()));
        }
    }

    @Test
    public void updateMeeting() throws URISyntaxException, JSONException {
        Meeting meeting = new Meeting();
        meeting.setRank(getRanks().get(1));
        meeting.setNotes("Test notes");
        
        Response r = create(basePath().path("meetings"), meeting);
        String location = r.getHeaderString("Location");
        Assert.assertNotNull(location);
        Meeting update = get(client.target(location), Meeting.class);
        update.setNotes("Updated notes");
        update(client.target(location), update);
        
        Meeting check = get(client.target(location), Meeting.class);
        Assert.assertNotEquals(update.getNotes(), meeting.getNotes());
        Assert.assertEquals(check.getNotes(), update.getNotes());
        delete(client.target(location));
    }
    
    protected List<Scout> buildAttendeeList() throws URISyntaxException, JSONException {
        List<Scout> attendees = new ArrayList<>();
        Den den = createDen("Meeting Den", 2);
        for (int i = 1; i <= 5; i++) {
            Scout scout = new Scout();
            scout.setName("Attendee #1");
            scout.setDen(den); // Should create one
            scout.setEmailAddress("test@example.com");
            scout.setPhoneNumber("555-1234");
            scout.setRank(den.getRank());
            attendees.add(createScout(scout));
        }
        
        return attendees;
    }
}
