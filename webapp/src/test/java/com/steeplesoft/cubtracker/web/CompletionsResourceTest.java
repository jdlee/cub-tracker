/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.cubtracker.web;

import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.model.Item;
import com.steeplesoft.cubtracker.android.model.Scout;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author jdlee
 */
public class CompletionsResourceTest extends ClientSideTestBase {

    @Test(enabled=false)
    public void testRedirect() throws URISyntaxException, IOException {
        final URL url = basePath().path("completions").getUri().toURL();
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setInstanceFollowRedirects(false);
        conn.setReadTimeout(5000);
        int status = conn.getResponseCode();
        Assert.assertTrue((status != HttpURLConnection.HTTP_OK)
                && ((status == HttpURLConnection.HTTP_MOVED_TEMP
                || status == HttpURLConnection.HTTP_MOVED_PERM
                || status == HttpURLConnection.HTTP_SEE_OTHER)));

    }

    @Test(enabled=false)
    public void testItemCompletion() throws URISyntaxException, JSONException {
        Den den = createDen("Item Completion Den", 2);

        final Scout scout1 = createTestScout("Item Completion Scout 1", den);
        Form form = new Form()
                .param("scout", Long.toString(scout1.getId()))
                .param("item", "6")
                .param("item", "8")
                .param("item", "10");

        Response r = basePath().path("completions").path("items")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.form(form), Response.class);
        Assert.assertEquals(r.getStatus(), 200);

        r = getResponse(basePath().path("completions").path("items").path("" + scout1.getId()));
        List<Item> items = r.readEntity(new GenericType<List<Item>>() {
        });
        Assert.assertEquals(items.size(), 3);
    }

    @Test(enabled=false)
    public void testPartCompletion() throws URISyntaxException, JSONException {
        Den den = createDen("Part Completion Den", 2);
        final Scout scout1 = createTestScout("Part Completion Scout 1", den);
        recordPartCompletion(
                Arrays.asList(new Long[]{17L, 18L, 19L, 36L, 37L, 38L}),
                Arrays.asList(new Scout[]{scout1}));
    }

    @Test
    public void completionCheck() throws URISyntaxException, JSONException {
        Den den = createDen("Completion Den", 2);
        Scout scout1 = createTestScout("Completion Scout 1", den);
        Scout scout2 = createTestScout("Completion Scout 2", den);
        
        // Record part completion for ALL of items 6 and 8, and part of 7
        recordPartCompletion(
                Arrays.asList(new Long[]{17L, 18L, 19L, 20L, 21L, 22L, 34L, 35L, 36L, 37L, 38L}),
                Arrays.asList(new Scout[]{scout1,scout2}));
        
        
        Response r = getResponse(basePath().path("completions").path("items").path("" + scout1.getId()));
        List<Item> items = r.readEntity(new GenericType<List<Item>>() {
        });
        Assert.assertEquals(items.size(), 2);
        
        r = getResponse(basePath().path("completions").path("items").path("" + scout2.getId()));
        items = r.readEntity(new GenericType<List<Item>>() {
        });
        Assert.assertEquals(items.size(), 2);

        /*
        Response r = getResponse(basePath().path("completions").path("parts").path("check2")
            .queryParam("scout", scout1.getId())
            .queryParam("scout", scout2.getId())
        );
        Assert.assertEquals(r.getStatus(), 200);
        List<CompletionCheck> list = r.readEntity(new GenericType<List<CompletionCheck>>() {});
        System.out.println(list.toString());
        */
    }

    protected Scout createTestScout(String name, Den den) throws URISyntaxException, JSONException {
        Scout scout = new Scout();
        scout.setName(name);
        scout.setDen(den);
        scout.setRank(den.getRank());
        scout.setPhoneNumber("555-1234");
        scout.setEmailAddress("test@example.com");

        return createScout(scout);
    }

    protected void recordPartCompletion(List<Long> parts, List<Scout> scouts) throws URISyntaxException {
        Form form = new Form();
        for (Scout scout : scouts) {
            form = form.param("scout", Long.toString(scout.getId()));
        }
        for (Long part : parts) {
            form = form.param("part", Long.toString(part));
        }

        Response r = basePath()
                .path("completions")
                .path("parts")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.form(form), Response.class);
        Assert.assertEquals(r.getStatus(), 200);
    }
}
