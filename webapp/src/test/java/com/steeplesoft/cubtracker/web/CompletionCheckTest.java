/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.cubtracker.web;

import com.steeplesoft.cubtracker.android.model.CompletionCheck;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author jdlee
 */
public class CompletionCheckTest {
    @Test
    public void testCompletedWithSubsetRequired() {
        //CompletionCheck(Integer id, Integer scoutId, Integer itemId, Integer groupId, Long partsCompleted, Integer qtyRequired, Integer partCount) {
        CompletionCheck cc1 = new CompletionCheck(1, 1, 1, 1, 6L, 5, 7);
        Assert.assertTrue(cc1.isGroupComplete());
 
        CompletionCheck cc2 = new CompletionCheck(1, 1, 1, 1, 5L, 5, 7);
        Assert.assertTrue(cc2.isGroupComplete());
    }
    
    @Test
    public void testCompletedWithAllRequired() {
        CompletionCheck cc1 = new CompletionCheck(1, 1, 1, 1, 4L, -1, 4);
        Assert.assertTrue(cc1.isGroupComplete());
    }
    
    @Test
    public void testNotCompletedWithSubsetRequired() {
        CompletionCheck cc = new CompletionCheck(1, 1, 1, 1, 3L, 5, 7);
        Assert.assertFalse(cc.isGroupComplete());
        
    }
    
    @Test
    public void testNotCompletedWithAllRequired() {
        CompletionCheck cc = new CompletionCheck(1, 1, 1, 1, 3L, -1, 4);
        Assert.assertFalse(cc.isGroupComplete());
        
    }
}
