/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.cubtracker.web;

import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.model.Scout;
import java.net.URISyntaxException;
import java.util.List;
import javax.ws.rs.core.Response;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author jdlee
 */
public class ScoutsResourceTest extends ClientSideTestBase {
    @Test
    public void getScoutList() throws URISyntaxException {
        final List<Scout> scouts = getScouts();
        Assert.assertNotNull(scouts, "scouts should not be null");
    }

    @Test
    public void createScout() throws URISyntaxException, JSONException {
        Scout scout = new Scout();
        Den den = createDen("Create Scout Den", 2);
        scout.setName("Integration Test Scout");
        scout.setDen(den);
        scout.setRank(den.getRank());
        scout.setPhoneNumber("555-1234");
        scout.setEmailAddress("test@example.com");

        Response r = create(client.target(url.toURI()).path("api").path("scouts"), scout);
        String location = r.getHeaderString("Location");
        Assert.assertNotNull(location);
        
        Scout check = get(client.target(location), Scout.class);
        
        // We can't use .equals() as the ID is not set for scout
        Assert.assertEquals(check.getName(), scout.getName());
        Assert.assertEquals(check.getEmailAddress(), scout.getEmailAddress());
        
        delete(client.target(location));
    }
    
    @Test
    public void updateScout() throws URISyntaxException, JSONException {
        Scout scout = new Scout();
        Den den = createDen("Update Scout Den", 2);
        scout.setName("Integration Test Scout");
        scout.setDen(den);
        scout.setRank(den.getRank());
        scout.setPhoneNumber("555-1234");
        scout.setEmailAddress("test@example.com");
        
        Response r = create(client.target(url.toURI()).path("api").path("scouts"), scout);
        String location = r.getHeaderString("Location");
        
        Scout update = get(client.target(location), Scout.class);
        scout.setName("Updated Scout");
        update(client.target(location), update);
        Scout check = get(client.target(location), Scout.class);
        Assert.assertNotEquals(check.getName(), scout.getName());
        Assert.assertEquals(check.getName(), update.getName());
        
        delete(client.target(location));
    }
}
