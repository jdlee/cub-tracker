'use strict';

/* Services */

var services = angular.module('services', ['ngResource']);

services.factory('Dens', ['$resource',
    function($resource) {
        return $resource('api/dens', {}, {
            query: {method: 'GET', isArray: true},
            create: {method: 'POST'}
        });
    }]);

services.factory('Den', ['$resource',
    function($resource) {
        return $resource('api/dens/:id', {}, {
            show: { method: 'GET' },
            update: { method: 'POST', params: {id: '@id'} },
            delete: { method: 'DELETE', params: {id: '@id'} }
        });
    }]);

services.factory('Rank', ['$resource',
    function($resource) {
        return $resource('api/ranks', {}, {
            query: {method: 'GET', isArray: true}
        });
    }]);

services.factory('Scouts', ['$resource',
    function($resource) {
        return $resource('api/scouts', {}, {
            query: {method: 'GET', isArray: true},
            create: {method: 'POST'}
        });
    }]);

services.factory('Scout', ['$resource',
    function($resource) {
        return $resource('api/scouts/:id', {}, {
            show: { method: 'GET' },
            update: { method: 'POST', params: {id: '@id'} },
            delete: { method: 'DELETE', params: {id: '@id'} }
        });
    }]);

services.factory('Item', ['$resource',
    function($resource) {
        return $resource('api/items?rank=:rank&type=:type', {}, {
            query: {method: 'GET', isArray: true}
        });
    }]);