'use strict';

/* App Module */

var app = angular.module('app', [
    'ngRoute',
    'ui.bootstrap',
//  'cubtrackerAnimations',
    'controllers',
//  'cubtrackerFilters',
    'services'
]);

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider
                .when('/scouts', {
                    templateUrl: 'partials/scouts/scout-list.html',
                    controller: 'ScoutListCtrl'
                })
                .when('/scouts/new', {
                    templateUrl: 'partials/scouts/scout-form.html',
                    controller: 'ScoutCreateCtrl'
                })
                .when('/scouts/edit/:id', {
                    templateUrl: 'partials/scouts/scout-form.html',
                    controller: 'ScoutEditCtrl'
                })
                .when('/scouts/:id', {
                    templateUrl: 'partials/scouts/scout-detail.html',
                    controller: 'ScoutDetailCtrl'
                })
                .when('/dens', {
                    templateUrl: 'partials/dens/den-list.html',
                    controller: 'DenListCtrl'
                })
                .when('/dens/new', {
                    templateUrl: 'partials/dens/den-add.html',
                    controller: 'DenCreateCtrl'
                })
                .when('/dens/edit/:id', {
                    templateUrl: 'partials/dens/den-edit.html',
                    controller: 'DenEditCtrl'
                })
                .when('/completion/items', {
                    templateUrl: 'partials/completions-item.html',
                    controller: 'CompletionCtrl'
                })
                .when('/achievements/', {
                    templateUrl: 'partials/achievements-list.html',
                    controller: 'ItemsListCtrl',
                    type: ['A']
                })
                .when('/achievements/:rank', {
                    templateUrl: 'partials/achievements-list1.html',
                    controller: 'AchListCtrl1'
                })
                .when('/electives/', {
                    templateUrl: 'partials/achievements-list.html',
                    controller: 'ItemsListCtrl',
                    type: ['E']
                })
                .when('/loopsandpins/', {
                    templateUrl: 'partials/achievements-list.html',
                    controller: 'ItemsListCtrl',
                    type: ['Ls', 'La', 'Ps', 'Pa']
                })
                .when('/webelosbadges/', {
                    templateUrl: 'partials/achievements-list.html',
                    controller: 'ItemsListCtrl',
                    type: ['B']
                })
                .otherwise({
                    redirectTo: '/scouts'
                });
    }]);
