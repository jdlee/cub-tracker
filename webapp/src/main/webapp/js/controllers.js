'use strict';

/* Controllers */

var controllers = angular.module('controllers', []);

controllers.controller('ScoutListCtrl',
        ['$scope', 'Scout', 'Den',
            function($scope, Scout, Den) {
                $scope.scouts = Scout.query();
                $scope.dens = Den.query();
                $scope.orderProp = 'name';
            }]);

controllers.controller('ScoutCreateCtrl',
        ['$scope', 'Scouts', 'Den', '$location',
            function($scope, Scouts, Den, $location) {
                $scope.scout = {};
                $scope.operation = 'new';
                $scope.dens = Den.query();

                $scope.addScout = function() {
                    Scouts.create($scope.scout);
                    $location.path('/scouts');
                };
            }]);

controllers.controller('ScoutEditCtrl',
        ['$scope', '$routeParams', 'Scout', 'Den', '$location',
            function($scope, $routeParams, Scout, Den, $location) {
                $scope.scout = Scout.show({id: $routeParams.id});
                $scope.operation = 'edit';

                $scope.dens = Den.query();
                $scope.updateScout = function() {
                    Scout.update($scope.scout);
                    $location.path('/scouts');
                };
            }]);

controllers.controller('ScoutDetailCtrl',
        ['$scope', '$routeParams', 'Scout',
            function($scope, $routeParams, Scout) {
                $scope.scout = Scout.get({id: $routeParams.id});

                $scope.setImage = function(imageUrl) {
                    $scope.mainImageUrl = imageUrl;
                };
            }]);

controllers.controller('DenListCtrl',
        ['$scope', 'Den',
            function($scope, Den) {
                $scope.dens = Den.query();
                $scope.orderProp = 'name';
            }]);

controllers.controller('DenCreateCtrl',
        ['$scope', 'Dens', 'Rank', '$location',
            function($scope, Dens, Rank, $location) {
                $scope.den = {};
                $scope.operation = 'new';
                $scope.ranks = Rank.query();

                $scope.addDen = function() {
                    Dens.create($scope.den);
                    $location.path('/dens');
                };
            }]);

controllers.controller('DenEditCtrl',
        ['$scope', '$routeParams', 'Den', 'Rank', '$location',
            function($scope, $routeParams, Den, Rank, $location) {
                $scope.den = Den.show({id:$routeParams.id})
                $scope.ranks = Rank.query();

                $scope.updateDen = function() {
                    Den.update($scope.den);
                    $location.path('/dens');
                };
            }]);

controllers.controller('CompletionCtrl',
        ['$scope', 'Den', 'Scout', 'Item',
            function($scope, Den, Scout, Item) {
                $scope.dens = Den.query();
                $scope.scouts = Scout.query();
                $scope.selectedScouts = [];
                $scope.items = [];
                $scope.rank = 1;
                $scope.types = [
                    {'key': 'A', 'value': 'Achievements'},
                    {'key': 'E', 'value': 'Electives'},
                    {'key': 'La,Ls,Pa,Ps', 'value': 'Belt Loops/Pins'}
                ];
                
                $scope.typeChange = function() {
                    $scope.denChange();
                };
                
                $scope.denChange = function() {
                    $scope.items = Item.query({
                        type: $scope.typequery.type, 
                        rank: $scope.denquery.den
                    });
                };
                
                $scope.toggleSelection = function (scoutName) {
                    var idx = $scope.selectedScouts.indexOf(scoutName);

                    // is currently selected
                    if (idx > -1) {
                        $scope.selectedScouts.splice(idx, 1);
                    }

                    // is newly selected
                    else {
                        $scope.selectedScouts.push(scoutName);
                    }
                };
                
                $scope.listSelected = function() {
                    alert($scope.selectedScouts);
                };
            }
        ]);

controllers.controller('ItemsListCtrl',
        ['$scope', '$route', 'Item', 'Rank',
            function($scope, $route, Item, Rank) {
                $scope.achievements = Item.query({type: $route.current.type});
                $scope.ranks = Rank.query();
                $scope.orderProp = 'number';
            }]);

controllers.controller('RankListCtrl',
        ['$scope', 'Rank',
            function($scope, Rank) {
                $scope.ranks = Rank.query();
            }]);