DROP TABLE IF EXISTS item_completions;
DROP TABLE IF EXISTS attendees;
DROP TABLE IF EXISTS meetings;
DROP TABLE IF EXISTS completions;
DROP TABLE IF EXISTS parts;
DROP TABLE IF EXISTS groups;
DROP TABLE IF EXISTS items;
DROP TABLE IF EXISTS scouts;
DROP TABLE IF EXISTS dens;
DROP TABLE IF EXISTS ranks;
DROP SEQUENCE IF EXISTS dens_id_seq;
DROP SEQUENCE IF EXISTS scouts_id_seq;
DROP SEQUENCE IF EXISTS completions_id_seq;
DROP SEQUENCE IF EXISTS meetings_id_seq;
DROP SEQUENCE IF EXISTS attendees_id_seq;
DROP SEQUENCE IF EXISTS ic_id_seq;

CREATE SEQUENCE dens_id_seq START WITH 1000;
CREATE SEQUENCE scouts_id_seq START WITH 1000;
CREATE SEQUENCE completions_id_seq START WITH 1000;
CREATE SEQUENCE meetings_id_seq START WITH 1000;
CREATE SEQUENCE attendees_id_seq START WITH 1000;
CREATE SEQUENCE ic_id_seq START WITH 1000;

CREATE TABLE ranks (
    _id INTEGER PRIMARY KEY, 
    name varchar(255) NOT NULL);
CREATE TABLE dens (
    _id INTEGER PRIMARY KEY DEFAULT nextval('dens_id_seq'),
    name varchar(255) NOT NULL, 
    rank_id INTEGER NOT NULL);
CREATE TABLE scouts (
    _id INTEGER PRIMARY KEY DEFAULT nextval('scouts_id_seq'), 
    name varchar(255), 
    den_id INTEGER NOT NULL,
    rank_id INTEGER, 
    contacts TEXT, 
    photo bytea, 
    email_address text, 
    phone_number text);
CREATE TABLE items (
    _id INTEGER PRIMARY KEY, 
    type varchar(255), 
    rank_id INTEGER, 
    number varchar(255), 
    title varchar(255), 
    notes TEXT, 
    image bytea, 
    group_count int);
CREATE TABLE groups (
    _id INTEGER PRIMARY KEY, 
    item_id INTEGER NOT NULL, 
    name varchar(255), 
    number varchar(255), 
    qty_required INTEGER , 
    notes TEXT, 
    part_count int, 
        FOREIGN KEY (item_id) REFERENCES items(_id));
CREATE TABLE parts (
    _id INTEGER PRIMARY KEY, 
    group_id INTEGER NOT NULL, 
    number varchar(255), 
    description TEXT, 
    notes TEXT, 
        FOREIGN KEY (group_id) REFERENCES groups(_id));
CREATE TABLE completions (
    _id INTEGER PRIMARY KEY DEFAULT nextval('completions_id_seq'), 
    scout_id INTEGER NOT NULL, 
    part_id INTEGER NOT NULL, 
    timestamp timestamp without time zone, 
    reported timestamp without time zone, 
        FOREIGN KEY (scout_id) REFERENCES scouts(_id), 
        FOREIGN KEY (part_id) REFERENCES parts(_id));
CREATE TABLE meetings (
    _id INTEGER PRIMARY KEY DEFAULT nextval('meetings_id_seq'), 
    rank_id INTEGER, 
    meeting_date timestamp without time zone, 
    notes TEXT, 
    reported timestamp without time zone, 
        FOREIGN KEY (rank_id) REFERENCES ranks(_id));
CREATE TABLE attendees (
    meeting_id INTEGER DEFAULT nextval('attendees_id_seq'), 
    scout_id INTEGER, 
        FOREIGN KEY(meeting_id) REFERENCES meetings(_id), 
        FOREIGN KEY (scout_id) REFERENCES scouts(_id));
CREATE TABLE item_completions (
    _id INTEGER PRIMARY KEY DEFAULT nextval('ic_id_seq'), 
    scout_id INTEGER NOT NULL, 
    item_id INTEGER NOT NULL, 
    timestamp timestamp without time zone, 
    reported timestamp without time zone);
