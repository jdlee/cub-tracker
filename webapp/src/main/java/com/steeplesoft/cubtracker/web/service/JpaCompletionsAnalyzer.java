/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.cubtracker.web.service;

import com.steeplesoft.cubtracker.CompletionsAnalyzer;
import com.steeplesoft.cubtracker.android.model.CompletionCheck;
import com.steeplesoft.cubtracker.android.model.Item;
import com.steeplesoft.cubtracker.android.model.ItemCompletion;
import com.steeplesoft.cubtracker.android.model.Scout;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author jdlee
 */
public class JpaCompletionsAnalyzer extends CompletionsAnalyzer{
    private EntityManager em;

    public JpaCompletionsAnalyzer(EntityManager em) {
        this.em = em;
    }

    @Override
    protected List<CompletionCheck> getCompletionCheck(long[] scoutIds) {
        StringBuilder scouts = arrayToString(scoutIds);

        String sql = getItemCompletionQuery(scouts.toString());
        final Query query = em.createNativeQuery(sql, "CompletionCheckResult");
        List<CompletionCheck> list = new ArrayList<>();
        try {
            for (CompletionCheck o : (List<CompletionCheck>)query.getResultList()) {
                list.add(o);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    protected void persistItemCompletion(Long scoutId, Long itemId) {
        ItemCompletion ic = getItemCompletion(scoutId, itemId);
        em.persist(ic);
    }
    
    private ItemCompletion getItemCompletion(Long scoutId, Long itemId) {
        ItemCompletion ic = new ItemCompletion();
        ic.setScout(em.find(Scout.class, scoutId));
        ic.setItem(em.find(Item.class, itemId));
        ic.setTimestamp(new Date());
        return ic;
    }
}
