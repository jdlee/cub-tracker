/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.cubtracker.web.resources;

import com.steeplesoft.cubtracker.android.model.Item;
import com.steeplesoft.cubtracker.android.model.ItemCompletion;
import com.steeplesoft.cubtracker.android.model.Scout;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author jdlee
 */
@RestResource
public class ItemsCompletionResource {
    @PersistenceContext
    protected EntityManager em;

    @GET
    public List<ItemCompletion> items() {
        return em.createQuery("SELECT ic FROM ItemCompletion ic ORDER BY ic.timestamp", ItemCompletion.class)
                .getResultList();
    }

    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Transactional
    public Response recordItemCompletion(
            @FormParam("scout") List<Long> scoutIds,
            @FormParam("item") List<Long> itemIds) {
        for (Long scoutId : scoutIds) {
            for (Long itemId : itemIds) {
                ItemCompletion ic = new ItemCompletion(
                        em.find(Scout.class, scoutId),
                        em.find(Item.class, itemId));
                em.persist(ic);
                System.out.println("Recording a completion of item #" + itemId + " for scout " + scoutId);
            }
        }
        return Response.ok().build();
    }

    @GET
    @Path("{scoutId}")
    public List<ItemCompletion> getItemCompletions(@PathParam("scoutId") @NotNull Long scoutId) {
        return (List<ItemCompletion>) em.createQuery("SELECT ic from ItemCompletion ic WHERE ic.scout.id = :scoutId")
                .setParameter("scoutId", scoutId)
                .getResultList();
    }

    @DELETE
    @Path("{scoutId}")
    @Transactional
    public Response deleteItemCompletionsForScout(Long scoutId) {
        em.createQuery("DELETE FROM ItemCompletion i WHERE i.scout.id = :scout")
                .setParameter("scout", scoutId)
                .executeUpdate();
        return Response.ok().build();
    }


}
