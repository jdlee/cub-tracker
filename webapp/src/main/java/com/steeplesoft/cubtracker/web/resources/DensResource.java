/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.cubtracker.web.resources;

import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.model.Rank;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author jdlee
 */
@Path("/dens")
@RestResource
public class DensResource {

    @PersistenceContext
    protected EntityManager em;
    @Context
    UriInfo uriInfo;

    @GET
    public List<Den> getDens() {
        return em.createQuery("SELECT d FROM Den d ORDER BY d.name", Den.class)
                .getResultList();
    }

    @GET
    @Path("{id}")
    public Response getScout(@PathParam("id") long id) {
        final Den den = em.find(Den.class, id);
        if (den == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("Not found").build();
        }

        return Response.ok(den).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Transactional
    public Response createDen(@FormParam("name") String name,
            @FormParam("rank") long rankId) {
        Den den = new Den();
        den.setName(name);
        den.setRank(em.find(Rank.class, rankId));
        return createDen(den);
    }

    @POST
    @Transactional
    public Response createDen(Den den) {
        try {
            em.persist(den);
            em.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.created(uriInfo.getRequestUriBuilder().path("" + den.getId()).build()).build();
    }

    @POST
    @Transactional
    @Path("{denId}")
    public Response updateDen(Den den) {
        em.merge(den);
        return Response.ok().build();
    }

    @DELETE
    @Transactional
    @Path("{denId}")
    public Response deleteDen(@PathParam("denId") @NotNull Long denId) {
        Den den = em.find(Den.class, denId);
        if (den != null) {
            em.remove(den);
        }

        return Response.ok().build();
    }
}
