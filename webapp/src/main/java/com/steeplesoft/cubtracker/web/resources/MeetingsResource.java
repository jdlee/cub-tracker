/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.cubtracker.web.resources;

import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.model.Meeting;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author jdlee
 */
@Path("meetings")
@RestResource
public class MeetingsResource {
    @PersistenceContext
    protected EntityManager em;
    @Context
    UriInfo uriInfo;

    @GET
    public List<Meeting> getMeetings() {
        return em.createQuery("SELECT m FROM Meeting m", Meeting.class).getResultList();
    }
    
    @GET
    @Path("{meetingId}")
    public Meeting getMeeting(@PathParam("meetingId") Long meetingId) {
        return em.find(Meeting.class, meetingId);
    }
    
    @POST
    @Transactional
    public Response createMeeting(Meeting meeting) {
        em.persist(meeting);
        em.flush();
        return Response.created(uriInfo.getRequestUriBuilder().path("" + meeting.getId()).build()).build();
    }
    
    @POST
    @Path("{meetingId}")
    @Transactional
    public Response updateMeeting(Meeting meeting) {
        em.merge(meeting);
        return Response.ok().build();
    }
    
    @DELETE
    @Path("{meetingId}")
    @Transactional
    public Response deleteMeeting(@PathParam("meetingId") @NotNull Long meetingId) {
        Meeting meeting = em.find(Meeting.class, meetingId);
        if (meeting != null) {
            em.remove(meeting);
        }

        return Response.ok().build();
    }
}
