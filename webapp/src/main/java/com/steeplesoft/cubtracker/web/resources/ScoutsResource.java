package com.steeplesoft.cubtracker.web.resources;

import com.steeplesoft.cubtracker.android.model.Den;
import com.steeplesoft.cubtracker.android.model.Scout;
import com.steeplesoft.cubtracker.web.MultipartRequestMap;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author jdlee
 */
@Path("/scouts")
@RestResource
public class ScoutsResource {

    @PersistenceContext
    protected EntityManager em;
    @Context
    UriInfo uriInfo;

    @GET
    @Transactional
    public List<Scout> getScouts() {
        final List<Scout> scouts = em.createQuery("SELECT s FROM Scout s", Scout.class).getResultList();
        for (Scout scout : scouts) {
            // TODO : for testing only. Remove before publishing
            if (scout.getPhoto() == null) {
                try {
                    File file = new File("/home/jdlee/cover.jpg");
                    byte[] fileData = new byte[(int) file.length()];
                    DataInputStream dis = new DataInputStream(new FileInputStream(file));
                    dis.readFully(fileData);
                    dis.close();
                    scout.setPhoto(fileData);
                    em.persist(scout);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return scouts;
    }

    @GET
    @Path("{id}")
    @Transactional
    public Response getScout(@PathParam("id") long id,
            @QueryParam("skipPhoto") @DefaultValue("false") boolean skipPhoto) {
        final Scout scout = em.find(Scout.class, id);
        if (scout == null) {
            return Response.status(Status.NOT_FOUND).entity("Not found").build();
        }

        // TODO : for testing only. Remove before publishing
        if (scout.getPhoto() == null) {
            try {
                File file = new File("/home/jdlee/cover.jpg");
                byte[] fileData = new byte[(int) file.length()];
                DataInputStream dis = new DataInputStream(new FileInputStream(file));
                dis.readFully(fileData);
                dis.close();
                scout.setPhoto(fileData);
                em.persist(scout);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (skipPhoto) {
            scout.setPhoto(null);
        }
        return Response.ok(scout).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Transactional
    public Response createScoutForm(
            @FormParam("name") String name,
            @FormParam("phoneNumber") String phoneNumber,
            @FormParam("emailAddress") String emailAddress,
            @FormParam("denId") Long denId) {
        Scout scout = new Scout();
        scout.setName(name);
        scout.setPhoneNumber(phoneNumber);
        scout.setEmailAddress(emailAddress);
        scout.setDen(em.find(Den.class, denId));
        return Response.created(uriInfo.getRequestUriBuilder().path("" + scout.getId()).build()).build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Transactional
    public Response createScoutUpload(@Context HttpServletRequest request) throws ServletException {
        MultipartRequestMap map = new MultipartRequestMap(request);
        Scout scout = new Scout();
        scout.setName(map.getStringParameter("name"));
        scout.setPhoneNumber(map.getStringParameter("phone_number"));
        scout.setEmailAddress(map.getStringParameter("email_address"));
        scout.setDen(em.find(Den.class, Long.parseLong(map.getStringParameter("den_id"))));
        scout.setPhoto(readFile(map.getFileParameter("photo")));
        
        em.persist(scout);
        em.flush();
        
        return Response.created(uriInfo.getRequestUriBuilder().path("" + scout.getId()).build()).build();
    }

    @POST
    @Transactional
    public Response createScout(Scout scout) {
        em.persist(scout);
        em.flush();
        return Response.created(uriInfo.getRequestUriBuilder().path("" + scout.getId()).build()).build();
    }

    @POST
    @Path("{id}")
    @Transactional
    public Response updateScout(Scout scout) {
        em.merge(scout);
        return Response.ok().build();
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public Response delete(@PathParam("id") Long id) {
        Scout scout = em.find(Scout.class, id);
        if (scout != null) {
            em.remove(scout);
        }

        return Response.ok().build();
    }

    @GET
    @Path("{id}/photo")
    public Response getScoutPhoto(@PathParam("id") long id) {
        final Scout scout = em.find(Scout.class, id);
        return Response.ok(scout.getPhoto()).type("image/jpg").build();
    }

    private byte[] readFile(File file) {
        try (BufferedInputStream input = new BufferedInputStream(new FileInputStream(file), 8192);
                ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[8192];
            int bytesRead;
            while ((bytesRead = input.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }
            return output.toByteArray();
        } catch (IOException ex) {
            Logger.getLogger(ScoutsResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
}
