package com.steeplesoft.cubtracker.web.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.glassfish.jersey.server.model.Resource;

/**
 *
 * @author jdlee
 */
@Path("/completions")
@RestResource
public class CompletionsResource {
    @Context
    private UriInfo uriInfo;

    @GET
    public Response get() {
        final Response response = 
            Response.seeOther(uriInfo.getRequestUriBuilder().path("items").build()).build();
        return response;
    }
    
    @Path("items") 
    public Resource getItemsCompletionResource() {
        return Resource.from(ItemsCompletionResource.class);
    }
    
    @Path("parts") 
    public Resource getPartsCompletionResource() {
        return Resource.from(PartsCompletionResource.class);
    }
}
