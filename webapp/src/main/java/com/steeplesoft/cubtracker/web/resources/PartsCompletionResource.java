/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.cubtracker.web.resources;

import com.steeplesoft.cubtracker.android.model.Completion;
import com.steeplesoft.cubtracker.android.model.Part;
import com.steeplesoft.cubtracker.android.model.Scout;
import com.steeplesoft.cubtracker.web.service.JpaCompletionsAnalyzer;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 *
 * @author jdlee
 */
@RestResource
@RequestScoped
public class PartsCompletionResource {
    @PersistenceContext
    protected EntityManager em;

    @GET
    public List<Completion> parts() {
        return em.createQuery("SELECT c FROM Completion c ORDER BY c.timestamp", Completion.class)
                .getResultList();
    }

    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Transactional
    public Response recordPartCompletion(
            @FormParam("scout") List<Long> scoutIds,
            @FormParam("part") List<Long> partIds) {
        for (Long scoutId : scoutIds) {
            for (Long partid : partIds) {
                Completion completion = new Completion(
                        em.find(Scout.class, scoutId),
                        em.find(Part.class, partid));
                try {
                    em.persist(completion);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println("Recording a completion of part #" + partid + " for scout " + scoutId);
            }
        }

        try {
            JpaCompletionsAnalyzer jca = new JpaCompletionsAnalyzer(em);
            jca.analyzePartCompletions(scoutIds);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok().build();
    }

    @DELETE
    @Path("{scoutId}")
    @Transactional
    public Response deletePartCompletionsForScout(@PathParam("scoutId") @NotNull Long scoutId) {
        em.createQuery("DELETE FROM Completion c WHERE c.scout.id = :scout")
                .setParameter("scout", scoutId)
                .executeUpdate();
        return Response.ok().build();
    }

    @GET
    @Path("{scoutId}")
    public List<Completion> getPartCompletions(@PathParam("scoutId") @NotNull Long scoutId) {
        return (List<Completion>) em.createQuery("SELECT c from Completion c WHERE c.scout.id = :scoutId")
                .setParameter("scoutId", scoutId)
                .getResultList();
    }
/*
    @GET
    @Path("check")
    public List<CompletionCheck> check() {
        String jpaql = 
                "SELECT new com.steeplesoft.cubtracker.web.CompletionCheck(i.id, g.id, p.id, g.quantityRequired, g.partCount, s.id) " +
                "FROM Item i, Group g, Part p, Scout s, Completion c " +
                "WHERE i.id = g.itemId " +
                "    and g.id = p.groupId " +
                "    and p.id = c.partId " +
                "    and c.scoutId = s.id " +
                "    and i.id not in (select ic.itemId from ItemCompletion ic where ic.scoutId = s.id) " +
                "order by itemId, groupId, partId";
        final List<CompletionCheck> list = (List<CompletionCheck>)em.createQuery(jpaql).getResultList();
        return list;
    }

    @GET
    @Path("check2") // Temp
    public List<CompletionCheck> check2(@QueryParam("scout") List<Long> scoutIds,
            @QueryParam("part") List<Long> partIds) {
        StringBuilder scouts = listToArray(scoutIds);
        StringBuilder parts = listToArray(partIds);

        String sql = 
                "select distinct 1 as id, i._id as itemId, g._id as groupId, p._id as partId, qty_required as qtyRequired, part_count as partCount, s._id as scoutId " +
                "from items i, " +
                "    groups g, " +
                "    parts p, " +
                "    completions c, " +
                "    scouts s " +
                "where i._id = g.item_id" +
                "    and g._id = p.group_id" +
                "    and p._id = c.part_id" +
                "    and c.scout_id = s._id" +
                "    and s._id in (" + scouts.toString() +") " +
//                "    and p._id in (" + parts.toString() +") " +
                "    and i._id not in (select item_id from item_completions where scout_id = s._id) " +
                "order by itemId, groupId, partId";
        System.out.println(sql);
        final Query query = em.createNativeQuery(sql, "CompletionCheckResult");
        List<CompletionCheck> list = null;
        try {
            list = (List<CompletionCheck>)query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
*/
}
