/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.cubtracker.web.filters;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author jdlee
 */
@Provider
public class PersistenceExceptionFilter implements ExceptionMapper<PersistenceException> {

    @Override
    public Response toResponse(PersistenceException exception) {
        if (exception instanceof NoResultException) {
            return Response.status(Status.NOT_FOUND).entity("Not found").build();
        } else {
            return Response.status(500).entity(exception.getLocalizedMessage()).build();
        }
    }

}
