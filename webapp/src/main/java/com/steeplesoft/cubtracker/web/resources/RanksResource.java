/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.cubtracker.web.resources;

import com.steeplesoft.cubtracker.android.model.Rank;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 *
 * @author jdlee
 */
@Path("/ranks")
@RestResource
public class RanksResource {
    @PersistenceContext
    protected EntityManager em;

    @GET
    public List<Rank> getRanks() {
        return em.createQuery("SELECT r FROM Rank r WHERE r.id < 6 ORDER BY r.id", Rank.class).getResultList();
    }
}
