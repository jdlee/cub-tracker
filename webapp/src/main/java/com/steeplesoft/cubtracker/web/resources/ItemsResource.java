package com.steeplesoft.cubtracker.web.resources;

import com.steeplesoft.cubtracker.android.model.Group;
import com.steeplesoft.cubtracker.android.model.Item;
import com.steeplesoft.cubtracker.android.model.Part;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

/**
 *
 * @author jdlee
 */
@Path("/items")
@RestResource
public class ItemsResource {

    @PersistenceContext
    protected EntityManager em;

    @GET
    public List<Item> getItems(
            @QueryParam("type") final String type, 
            @QueryParam("rank") final Long rank) {
        String jpaql = "SELECT i from Item i";
        boolean useType = false;
        boolean useRank = false;

        List<String> types = new ArrayList<>();
        if (type != null) {
            useType = true;
            jpaql += " WHERE i.type in :type";

            if (type.indexOf(",") > 0) {
                for (String t : type.split(",")) {
                    types.add(t.trim());
                }
            } else {
                types.add(type.trim());
            }
        }
        
        if (type != null && !type.isEmpty()) {
        }
        if (rank != null && (types.size() <= 1)) {
            useRank = true;
            jpaql += (useType ? " AND " : " WHERE ") + " i.rank.id = :rank";
        }
        // TODO: validate rank?
        Query query = em.createQuery(jpaql); // AND i.rank.id = :rank")
        if (useType) {
            query = query.setParameter("type", types);
        }
        if (useRank) {
            query = query.setParameter("rank", rank);
        }
        
        return (List<Item>)query.getResultList();
    }
    
    @GET
    @Path("{itemId}")
    public Item getItem(@PathParam("itemId") Long itemId) {
        return em.find(Item.class, itemId);
    }

    @GET
    @Path("{itemId}/groups")
    public List<Group> getGroups(@PathParam("itemId") @NotNull long itemId) {
        final List<Group> groups = (List<Group>) em.createQuery("SELECT g from Group g WHERE g.itemId = :itemId")
                .setParameter("itemId", itemId)
                .getResultList();
        return groups;
    }

    @GET
    @Path("{itemId}/groups/{groupId}")
    public Group getGroup(@PathParam("itemId") @NotNull long itemId,
            @PathParam("groupId") @NotNull long groupId) {
        return em.createQuery("SELECT g FROM Group g WHERE g.id = :groupId AND g.itemId = :itemId", Group.class)
                .setParameter("groupId", groupId)
                .setParameter("itemId", itemId)
                .getSingleResult();
    }

    @GET
    @Path("{itemId}/groups/{groupId}/parts")
    public List<Part> getGroupParts(@PathParam("groupId") @NotNull long groupId) {
        final List<Part> groups = (List<Part>) em.createQuery("SELECT p from Part p WHERE p.groupId = :groupId")
                .setParameter("groupId", groupId)
                .getResultList();
        return groups;
    }
    
    @GET
    @Path("{itemId}/groups/{groupId}/parts/{partId}")
    public Part getPart(@PathParam("partId") Long partId) {
        return em.find(Part.class, partId);
    }
}
