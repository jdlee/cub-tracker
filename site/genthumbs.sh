#!/bin/bash
DIR=target/cubtracker-site/images/users-guide
#mkdir -p target/generated-docs/images &>/dev/null
if [ -e $DIR ]; then
    cd $DIR

    for FILE in `ls *png | grep -v thumb 2>/dev/null` ; do
        convert $FILE -resize 14% thumb_$FILE
    done
fi
