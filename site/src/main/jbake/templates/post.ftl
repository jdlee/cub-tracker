<#include "header.ftl">
                            <div class="post">
                                <div class="entry_header">
                                    <p class="entry-date">
                                        ${content.date?date?string('MMM yy')}
                                        <br>
                                        <span class="date">${content.date?date?string('dd')}</span>
                                    </p>
                                    <h2 class="home">
                                        <a href="${content.uri}">${content.title}</a>
                                    </h2>
                                    <div class="recover"></div>
                                </div>
                                <div class="entry_content">
                                    <div class="paragraph">
                                        ${content.body?keep_before(splitter)}
                                    </div>
                                    <#if content.tags??>
                                    <div class="postedinfo">
                                        Posted in 
                                        <span class="categories">
                                            <#list content.tags as tag>
                                            <a rel="category tag" title="View all posts in ${tag}" href="${content.rootpath}/posts/tags/${tag}">${tag}</a>
                                            </#list>
                                        </span>
                                    </div>
                                    </#if>
                                    <div id="disqus_thread"></div>
                                    <script type="text/javascript">
                                        var disqus_shortname = 'steeplesoft';

                                        (function() {
                                            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                                            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                                            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                                        })();
                                    </script>
                                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                                </div>
                            </div>
<#include "footer.ftl">
