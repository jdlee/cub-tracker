                        </div>
                        <div id="sidebar">
                            <ul>
                                <li class="widget">
                                    <h2>
                                        <a class="sidebartitle" href="#" rel="nofollow">Links</a>
                                    </h2>
                                    <ul class="xoxo blogroll">
                                        <li><a href="https://play.google.com/store/apps/details?id=com.steeplesoft.cubtracker">Play Store</a></li>
                                        <li><a href="http://issues.cubtracker.com">Issue Tracker</a></li>
                                        <li><a href="https://www.facebook.com/cubtracker">Facebook</a></li>
                                        <li><a href="http://twitter.com/cubtracker">Twitter</a></li>
                                    </ul>
                                </li>
                                <li class="widget widget_text">
                                    <div class="textwidget">
                                        <iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2FCub-Tracker%2F154833441245731&amp;width=255&amp;colorscheme=light&amp;show_faces=true&amp;stream=false&amp;header=true&amp;height=200" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:255px; height:215px;" allowTransparency="true"></iframe>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="recover"></div>
                    </div>
                </div>
                <div id="globalnav">
                    <div id="navpocket">
                        <ul id="nav" class="sf-js-enabled sh-shadow">
                            <li class="page_item"><a href="/" rel="nofollow">Home</a></li>
                            <li class="page_item"><a href="/users-guide/">Users Guide</a></li>
                        </ul>
                    </div>
                </div>
                <ul id="socialize-icons">
                    <li id="icon-facebook"><a href="http://www.facebook.com/cubtracker" rel="nofollow" target="_blank">facebook</a></li>
                    <li id="icon-twitter"><a href="http://twitter.com/cubtracker" rel="nofollow" target="_blank">twitter</a></li>
                </ul>
                <div id="footer">
                    <div class="footer-content">
                        <div class="footer-widget"></div>
                    </div>
                    <span class="copyright">
                        <span id="alignleft">Copyright &copy; 2011-2015 Cub Tracker</span>
                    </span>
                    <span class="footer-tag">
                        <a href="http://www.topblogformula.com/wordpress-business-themes/intrepidity" target="_blank">
                            intrepidity Theme by 
                            <a href="http://www.topblogformula.com/">Top Blog Formula</a>
                        </a>
                    </span>
                </div>
            </div>
        </div>
        <script>
            $(".image").each(function () {
                $(this).attr("rel", "lightbox[group]");
            });
        </script>
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-1091049-5']);
            _gaq.push(['_trackPageview']);
            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();
        </script>
    </body>
</html>