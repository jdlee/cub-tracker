<#include "header.ftl">
    <#include "pagination.ftl">
    <#list posts as post>
    <#if (post.status == "published")>
        <div class="post">
            <div class="entry_header">
                <p class="entry-date">
                    ${post.date?date?string('MMM yy')}
                    <br>
                    <span class="date">${post.date?date?string('dd')}</span>
                </p>
                <h2 class="home">
                    <a href="${post.uri}">${post.title}</a>
                </h2>
                <div class="recover"></div>
            </div>
            <div class="entry_content">
                <div class="paragraph">
                    ${post.body?keep_before(splitter)}
                </div>
                <#if post.tags??>
                <div class="postedinfo">
                    Posted in 
                    <span class="categories">
                        <#list post.tags as tag>
                        <a rel="category tag" title="View all posts in ${tag}" href="${content.rootpath}/posts/tags/${tag}">${tag}</a>
                        </#list>
                    </span>
                </div>
                </#if>
            </div>
        </div>
    </#if>
    </#list>
    <#include "pagination.ftl">
<#include "footer.ftl">