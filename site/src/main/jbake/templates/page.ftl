<#include "header.ftl">
        <div class="page">
                  <h1 class="title">
                    ${content.title}
                  </h1>
                  ${content.body}
        </div>
<#include "footer.ftl">