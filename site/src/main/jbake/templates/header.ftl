<!DOCTYPE html>
<html>
    <head>
        <title>Cub Scout progress tracking on the go</title>
        <meta name="author" content="Jason Lee">
        <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
        <link rel="alternate" type="application/rss+xml" title="Cub Tracker RSS Feed" href="/feed.atom">

        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Cabin+Sketch:bold">
        <link type="text/css" rel="stylesheet" href="/styles/colorbox.css" media="screen">
        <link type="text/css" rel="stylesheet" href="/styles/style.css">
        <link type="text/css" rel="stylesheet" href="/styles/skin-silver.css">
        <link type="text/css" rel="stylesheet" href="/styles/lightbox/lightbox.css">
        <link type="text/css" rel="stylesheet" href="/styles/custom.css">

        <script src="/scripts/lightbox/jquery-1.7.2.min.js"></script>
        <script src="/scripts/lightbox/lightbox.js"></script>
        <script src="/scripts/jquery-1.9.1.min.js"></script>
        <script src="/scripts/superfish.js"></script>
        <script src="/scripts/functions.js"></script>
    </head>
    <body class="home blog" style="font-size:14px;">
        <#include "misc.ftl">
        <div id="bg">
            <div id="shadow">
                <div id="header">
                    <h1 id="logo" style="margin-bottom: 50px">
                        <a id="blogname" href="${content.rootpath}" style="background:none;text-indent:0;width:auto;">
                            <img src="/images/Cub-Tracker-Logo_72.png" style="float:left;border:none;" alt="Cub Tracker Logo">
                            <span class="bold" style="margin: 0px 0px 20px 10px; font-size: 48pt; font-family: 'Cabin Sketch', arial, serif;">
                                Cub Tracker
                            </span>
                            <span style="font-style: italic; font-size: 14pt; font-weight: normal;">
                                Cub Scout progress tracking on the go
                            </span>
                        </a>
                    </h1>
                </div>
                <div id="container">
                    <div id="container-shoulder">
                        <div id="left-col">
