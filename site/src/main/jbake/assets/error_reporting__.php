<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $message .= "PHONE_MODEL = " . $_POST["PHONE_MODEL"] . "\n";
    $message .= "ANDROID_VERSION = " . $_POST["ANDROID_VERSION"] . "\n";
    $message .= "PRODUCT = " . $_POST["PRODUCT"] . "\n";
    $message .= "APP_VERSION_CODE = " . $_POST["APP_VERSION_CODE"] . "\n";
    $message .= "APP_VERSION_NAME = " . $_POST["APP_VERSION_NAME"] . "\n";
    $message .= "APPLICATION_LOG = " . $_POST["APPLICATION_LOG"] . "\n";
    $message .= "USER_COMMENT = " . $_POST["USER_COMMENT"] . "\n";
    $message .= "STACK_TRACE:\n" . $_POST["STACK_TRACE"] . "\n";
    $message .= "LOGCAT:\n" . $_POST["LOGCAT"] . "\n";

    // Send
    mail('jason@steeplesoft.com', 'Cub Tracker Error Report', $message);
} else {
    header("HTTP/1.0 404 Not Found");
    exit();
}
?>
