package com.steeplesoft.cubtracker.android.model;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "scouts")
public class Scout implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String name;

    @Column(name="phone_number", nullable = true)
    private String phoneNumber;

    @Column(name="email_address", nullable = true)
    private String emailAddress;

//    @ManyToOne
//    @JoinColumn(name = "rank_id")
//    private Rank rank;
//
    @ManyToOne
    @JoinColumn(name = "den_id")
    private Den den;

    @Column(name = "contacts")
    private String contactIds;

    @Lob
    @Basic
    @Column
    private byte[] photo;

    public Scout() {

    }

    public Scout(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Scout(long id, String name, String emailAddress, String phoneNumber, Den den, String contactIds) {
        this.id = id;
        this.name = name;
        this.emailAddress = emailAddress;
        this.phoneNumber = phoneNumber;
        this.contactIds = contactIds;
        this.den = den;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Den getDen() {
        return den;
    }

    public void setDen(Den den) {
        this.den = den;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getContactIds() {
        return contactIds;
    }

    public void setContactIds(String contactIds) {
        this.contactIds = contactIds;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "Scout{" + "id=" + id + ", name='" + name + '\'' + ", den=" + den + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 59 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Scout other = (Scout) obj;
        if (this.id != other.id) {
            return false;
        }
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if ((this.phoneNumber == null) ? (other.phoneNumber != null) : !this.phoneNumber.equals(other.phoneNumber)) {
            return false;
        }
        if ((this.emailAddress == null) ? (other.emailAddress != null) : !this.emailAddress.equals(other.emailAddress)) {
            return false;
        }
        if (this.den != other.den && (this.den == null || !this.den.equals(other.den))) {
            return false;
        }
        if ((this.contactIds == null) ? (other.contactIds != null) : !this.contactIds.equals(other.contactIds)) {
            return false;
        }
        if (!Arrays.equals(this.photo, other.photo)) {
            return false;
        }
        return true;
    }
    
}