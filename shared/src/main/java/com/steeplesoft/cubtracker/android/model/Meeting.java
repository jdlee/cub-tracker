package com.steeplesoft.cubtracker.android.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "meetings")
public class Meeting implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "rank_id", referencedColumnName = "_id")
    private Den den;

    @Column(name = "meeting_date")
    @Temporal(value = TemporalType.DATE)
    private Date meetingDate = new Date();
    
    @OneToMany
    @JoinTable(name="attendees",
            joinColumns=@JoinColumn(name="meeting_id"),
            inverseJoinColumns=@JoinColumn(name="scout_id"))
    private List<Scout> attendees;

    @Column(name = "reported")
    @Temporal(value = TemporalType.DATE)
    private Date reported;

    @Column
    private String notes;
    
    private static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");

    public Meeting() {
        
    }
    
    public Meeting(long id, Den den, String meetingDate, String reported, String notes) {
        this.id = id;
        this.den = den;
        try {
            this.meetingDate = (meetingDate != null) ? df.parse(meetingDate) : null;
        } catch (ParseException e) {
        }
        try {
            this.reported = (reported != null) ? df.parse(reported) : null;
        } catch (ParseException e) {
        }
        this.notes = notes;
    }
    
    public Meeting(long id, Den den, Date meetingDate, Date reported, String notes) {
        super();
        this.id = id;
        this.den = den;
        this.meetingDate = meetingDate;
        this.reported = reported;
        this.notes = notes;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Den getDen() {
        return den;
    }

    public void setDen(Den den) {
        this.den = den;
    }

    public Date getMeetingDate() {
        return meetingDate;
    }

    public void setMeetingDate(Date meetingDate) {
        this.meetingDate = meetingDate;
    }
    
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public static DateFormat getDateFormat() {
        return df;
    }

    public List<Scout> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<Scout> attendees) {
        this.attendees = attendees;
    }
}
