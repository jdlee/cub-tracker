package com.steeplesoft.cubtracker.android.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "completions")
public class Completion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "scout_id", referencedColumnName = "_id")
    private Scout scout;

    @ManyToOne
    @JoinColumn(name = "part_id", referencedColumnName = "_id")
    private Part part;

    @Column
    @Temporal(value = TemporalType.DATE)
    private Date timestamp = new Date();

    @Column
    @Temporal(value = TemporalType.DATE)
    private Date reported;

    public Completion() {
    }

    public Completion(Scout scout, Part part) {
        this.scout = scout;
        this.part = part;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Scout getScout() {
        return scout;
    }

    public void setScout(Scout scout) {
        this.scout = scout;
    }

    public Part getPart() {
        return part;
    }

    public void setPart(Part part) {
        this.part = part;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Date getReported() {
        return reported;
    }

    public void setReported(Date reported) {
        this.reported = reported;
    }

}
