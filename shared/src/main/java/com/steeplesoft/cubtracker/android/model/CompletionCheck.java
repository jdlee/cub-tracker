/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.cubtracker.android.model;

import java.io.Serializable;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

/**
 *
 * @author jdlee
 */
@SqlResultSetMapping(
        name = "CompletionCheckResult",
//        entities = {
//            @EntityResult(entityClass = CompletionCheck.class)
//        }
//        /*
        classes = {
            @ConstructorResult(
                    targetClass = CompletionCheck.class,
                    columns = {
                        @ColumnResult(name = "id"),
                        @ColumnResult(name = "scoutId"),
                        @ColumnResult(name = "itemId"),
                        @ColumnResult(name = "groupId"),
                        @ColumnResult(name = "partsCompleted"),
                        @ColumnResult(name = "qtyRequired", type = Integer.class),
                        @ColumnResult(name = "partCount", type = Integer.class)
                    }
            )
        }
//*/
)
@Entity
public class CompletionCheck implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int scoutId;
    private int itemId;
    private int groupId;
    private long partsCompleted;
    private int qtyRequired;
    private int partCount;

    public CompletionCheck() {
        System.out.println("CompletionCheck");
    }
    
//    (Integer, Integer, Integer, Integer, Long, Integer, Integer)]
    public CompletionCheck(Integer id, Integer scoutId, Integer itemId, Integer groupId, Long partsCompleted, Integer qtyRequired, Integer partCount) {
        this.id = id;
        this.itemId = itemId;
        this.groupId = groupId;
        this.partsCompleted = partsCompleted;
        this.qtyRequired = qtyRequired;
        this.partCount = partCount;
        this.scoutId = scoutId;
    }
    
    public CompletionCheck(Object[] o) {
        this.id = (Integer) o[0];
        this.scoutId = (Integer) o[1];
        this.itemId = (Integer) o[2];
        this.groupId = (Integer) o[3];
        this.partsCompleted = (Long) o[4];
        this.qtyRequired = (Integer) o[5];
        this.partCount = (Integer) o[6];
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public long getPartsCompleted() {
        return partsCompleted;
    }

    public void setPartsCompleted(long partsCompleted) {
        this.partsCompleted = partsCompleted;
    }

    public int getQtyRequired() {
        return qtyRequired;
    }

    public void setQtyRequired(int qtyRequired) {
        this.qtyRequired = qtyRequired;
    }

    public int getPartCount() {
        return partCount;
    }

    public void setPartCount(int partCount) {
        this.partCount = partCount;
    }

    public int getScoutId() {
        return scoutId;
    }

    public void setScoutId(int scoutId) {
        this.scoutId = scoutId;
    }

    public boolean isGroupComplete() {
        return ((qtyRequired == -1) && (partsCompleted == partCount)) ||
               ((qtyRequired > 0) && (partsCompleted >= qtyRequired));
    }

    @Override
    public String toString() {
        return "CompletionCheck{" + "id=" + id + ", scoutId=" + scoutId + ", itemId=" + itemId + ", groupId=" + groupId + ", partsCompleted=" + partsCompleted + ", qtyRequired=" + qtyRequired + ", partCount=" + partCount + '}';
    }
}
