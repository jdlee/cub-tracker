package com.steeplesoft.cubtracker.android.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "items")
public class Item implements Serializable {
    
    public static enum TYPE {
        ACHIEVEMENT("A", "Achievement"),
        ELECTIVE("E", "Elective"),
        WEBELOS_BADGE("B", "Webelos Badge"),
        ACADEMIC_LOOP("La", "Academic Belt Loop"),
        ACADEMIC_PIN("Pa", "Academic Pin"),
        SPORTS_LOOP("Ls", "Sports Belt Loop"),
        SPORTS_PIN("Ps", "Sports Pin"),
        ADVENTURE("a", "Adventure"),
        ELECTIVE_ADVENTURE("e", "Elective Adventure");
        
        private final String code;
        private final String name;
        TYPE(String code, String name) {
            this.code = code;
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
        
        public static TYPE lookup(String code) {
            if ("A".equals(code)) { return ACHIEVEMENT; }
            else if ("E".equals(code)) { return ELECTIVE; }
            else if ("B".equals(code)) { return WEBELOS_BADGE; }
            else if ("La".equals(code)) { return ACADEMIC_LOOP; }
            else if ("Pa".equals(code)) { return ACADEMIC_PIN; }
            else if ("Ls".equals(code)) { return SPORTS_LOOP; }
            else if ("Ps".equals(code)) { return SPORTS_PIN; }
            else if ("a".equals(code)) { return ADVENTURE; }
            else if ("e".equals(code)) { return ELECTIVE_ADVENTURE; }
            else {
                throw new RuntimeException("Invalid type code: " + code);
            }
        }
        
    }
   
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column
    private String type;
    @ManyToOne
    @JoinColumn(name = "rank_id", referencedColumnName = "_id")
    private Rank rank;
    @Column
    private String number;
    @Column
    private String title;
    @Column
    private String notes;
    @Column(name = "group_count")
    private int groupCount;
    @OneToMany
    @JoinColumn(name="item_id")
    private List<Group> groups;
    @Basic
    @Column
    private String image;
    
    public Item() {
        
    }
    
    public Item(long id, String type, Rank rank, String number, String title, String notes, String image) {
        super();
        this.id = id;
        this.type = type;
        this.rank = rank;
        this.number = number;
        this.title = title;
        this.notes = notes;
        this.image = image;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getGroupCount() {
        return groupCount;
    }

    public void setGroupCount(int groupCount) {
        this.groupCount = groupCount;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }
    
    public String getDescription() {
        String rankName = rank != null ? rank.getName() : "";
        TYPE t = TYPE.lookup(type);
        if (t == TYPE.ACHIEVEMENT || t == TYPE.ELECTIVE || t == TYPE.ADVENTURE || t == TYPE.ELECTIVE_ADVENTURE) {
            return rankName + " " + t.getName() + " #" + number.toUpperCase();
        }
        return t.getName();
    }

    @Override
    public String toString() {
        return title;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Item other = (Item) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

}
