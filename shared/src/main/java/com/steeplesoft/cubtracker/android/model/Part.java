package com.steeplesoft.cubtracker.android.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "parts")
public class Part implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
//    @ManyToOne
//    @JoinColumn(name = "group_id", referencedColumnName = "_id")
//    private Group group;
    @Column(name="group_id")
    private long groupId;
    @Column
    private String number;
    @Column
    private String description;
    @Column
    private String notes;
    
    public Part() {
        
    }
    
    public Part(long id, Group group, String number, String description, String notes) {
        super();
        this.id = id;
//        this.group = group;
        this.number = number;
        this.description = description;
        this.notes = notes;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Part other = (Part) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

}
