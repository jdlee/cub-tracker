/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.cubtracker;

import com.steeplesoft.cubtracker.android.model.CompletionCheck;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author jdlee
 */
public abstract class CompletionsAnalyzer {
    public void analyzePartCompletions(List<Long> scoutIds) {
        long[] array = new long[scoutIds.size()];
        for(int i = 0; i < scoutIds.size(); i++) {
            array[i] = scoutIds.get(i);
        }
        analyzePartCompletions(array);
    }
    
    public void analyzePartCompletions(long[] scoutIds) {
        long lastScout = -1;
        long lastItem = -1;
        List<Boolean> results = new ArrayList<Boolean>();
        final List<CompletionCheck> completionCheck = getCompletionCheck(scoutIds);
        for (CompletionCheck cc : completionCheck) {
            long scout = cc.getScoutId();
            long item = cc.getItemId();
                    
            if (scout != lastScout) {
                if (lastScout != -1) {
                    checkItem(results, lastItem, lastScout);
                }
                lastScout = scout;
                lastItem = -1;
                results.clear();
            }
            
            if (item != lastItem) {
                if (lastItem != -1) {
                    checkItem(results, lastItem, lastScout);
                }
                lastItem = item;
                results.clear();
            }
            results.add(cc.isGroupComplete());
        }
        checkItem(results, lastItem, lastScout);
    }
    
    protected abstract List<CompletionCheck> getCompletionCheck(long[] scoutIds);
    protected abstract void persistItemCompletion(Long scoutId, Long itemId);
    
    protected StringBuilder arrayToString(long[] scoutIds) {
        String sep = "";
        StringBuilder scouts = new StringBuilder();
        for (long id : scoutIds) {
            scouts.append(sep).append(id);
            sep = ",";
        }
        return scouts;
    }

    protected void checkItem(List<Boolean> results, long itemId, long scoutId) {
        System.out.println(results.toString());
        if (Collections.frequency(results, true) == results.size()) {
            persistItemCompletion(scoutId, itemId);
        } else {
            System.out.println("NOT recording completion of " + itemId + " for " + scoutId);
        }
    }
    
    protected String getItemCompletionQuery(String scouts) {
        String sql =
                "select distinct 1 as id, " +
                        "   s._id as scoutId, " +
                        "   i._id as itemId, " +
                        "   g._id as groupId, " +
                        "   count(p._id) as partsCompleted, " +
                        "   g.qty_required as qtyRequired, " +
                        "   part_count as partCount " +
                        "from items i, " +
                        "   groups g, " +
                        "   parts p, " +
                        "   completions c, " +
                        "   scouts s " +
                        "where i._id = g.item_id " +
                        "    and g._id = p.group_id " +
                        "    and p._id = c.part_id " +
                        "    and c.scout_id = s._id " +
                        "    and s._id in (" + scouts +") " +
                        "    and i._id not in (select item_id from item_completions where scout_id = s._id) " +
                        "group by scoutId, itemId, groupId, qtyRequired, part_count " +
                        "order by scoutId, itemId, groupId";

        return sql;
    }
}
